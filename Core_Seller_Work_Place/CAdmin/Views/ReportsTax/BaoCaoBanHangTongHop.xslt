﻿<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <div class="report-used-vat" style =" width:1000px;margin 0px auto;font-family:Arial;font-size:12px;margin-left:15px">
      <ul style="list-style: none;width:1276px;text-align: center;">
        <li style="float:left;line-height: 20px;margin-left:434px;">
          <center>
            <b>BẢNG KÊ HOÁ ĐƠN TỔNG HỢP</b>
            <!--(Kèm theo tờ khai thuế GTGT theo mẫu số 01/GTGT)-->
            <br/>
            <!--(Kèm theo tờ khai thuế GTGT theo mẫu số 01/GTGT)-->
            <br/>
            <b>[1]</b>Kỳ tính thuế: Từ ngày <xsl:value-of select="//HSoThueDTu/HSoKhaiThue/TTinChung/TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhaiTuNgay"/> Đến ngày <xsl:value-of select="HSoThueDTu/HSoKhaiThue/TTinChung/TTinTKhaiThue/TKhaiThue/KyKKhaiThue/kyKKhaiDenNgay"/>
            <br/>
            <br/>
          </center>
        </li>
        <li style="line-height: 20px;">
          <!--Mẫu số:-->
          <b>
            <!--01- 1/GTGT-->
          </b>
          <br/>
          <i>
            <!--(Ban hành kèm theo Thông tư số-->
            <br/>
            <!--119/2014/TT-BTC ngày 25/8/2014-->
            <br/>
            <!--của Bộ Tài chính)-->
          </i>
        </li>
      </ul>
      <div style="margin-top:20px;line-height: 24px;">
        <b>[02]Tên đơn vị:</b><xsl:value-of select="//HSoThueDTu/HSoKhaiThue/TTinChung/TTinTKhaiThue/NNT/tenNNT"/>
        <br/>
        <b>[03]</b>Mã số thuế: <xsl:value-of select="//HSoThueDTu/HSoKhaiThue/TTinChung/TTinTKhaiThue/NNT/mst"/>
        <br/>
        <b>
          <!--[04]Tên đại lý thuế(nếu có):-->
        </b>
        <br/>
      </div>
      <div style="width:129%;text-align:right" >
        <p>Đơn vị tiền: đồng Việt Nam</p>
      </div>
      <table width="130%" border="0" cellspacing="0" cellpadding="0" class="report-used-list" style=" border:thin solid #000;">
        <thead>
          <tr style=" border:thin solid #000;">
            <th rowspan="1">STT</th>
            <!--<th rowspan="1">Mã số thuế bán</th>-->
            <th rowspan="1">Mã Hàng</th>
            <th rowspan="1">Tên hàng</th>
            <th rowspan="1">Đơn vị tính</th>
            <th rowspan="1">Số lượng</th>
            <th rowspan="1">Đơn giá</th>
            <th rowspan="1">Doanh thu</th>
            <th rowspan="1">Tiền thuế</th>
            <th rowspan="1">Thành tiền</th>
            <th rowspan="1">Ghi chú</th>
          </tr>
        </thead>
        <tbody>
          <tr style=" border:thin solid #000;">
            <td style ="text-align:center; width: 50px">
              <i >[1]</i>
            </td>
            <!--<td style ="text-align:center; width: 70px">
              <i >[2]</i>
            </td>-->
            <td style ="text-align:center; width: 80px">
              <i >[2]</i>
            </td>
            <td style ="text-align:center; width: 100px">
              <i >[3]</i>
            </td>
            <td style ="text-align:center; width: 100px">
              <i >[4]</i>
            </td>
            <td style ="text-align:center; width: 100px">
              <i>[5]</i>
            </td>
            <td style ="text-align:center; width: 100px">
              <i>[6]</i>
            </td>
            <td style ="text-align:center; width: 100px">
              <i>[7]</i>
            </td>
            <td style ="text-align:center; width: 100px">
              <i>[8]</i>
            </td>
            <td style ="text-align:center; width: 100px">
              <i>[9]</i>
            </td>
            <td style ="text-align:center; width: 100px">
              <i>[10]</i>
            </td>
          </tr>
          <xsl:for-each select="//HSoThueDTu/HSoKhaiThue/PLuc/PL01_1_GTGT/BangKeBanRa/HDonBRa">
            <tr style=" border:thin solid #000;">
              <td style ="text-align:center;">
                <xsl:value-of select="position()"/>
              </td>
              <td style ="text-align:left;">
                <xsl:value-of select="MaHang"/>
              </td>
              <td style ="text-align:left;">
                <xsl:value-of select="TenHang"/>
              </td>
              <td style ="text-align:left;">
                <xsl:value-of select="DonVi"/>
              </td>
              <td style ="text-align:left;">
                <xsl:choose>
                  <xsl:when test="SoLuong!=''">
                    <xsl:value-of select="SoLuong"/>
                  </xsl:when>
                  <xsl:otherwise>
                    0
                  </xsl:otherwise>
                </xsl:choose>
              </td>
              <td style ="text-align:left;">
                <xsl:choose>
                  <xsl:when test="DonGia!=''">
                    <xsl:value-of select="DonGia"/>
                  </xsl:when>
                  <xsl:otherwise>
                    0
                  </xsl:otherwise>
                </xsl:choose>
              </td>
              <td style ="text-align:left;">
                <xsl:choose>
                  <xsl:when test="dsoBanChuaThue!=''">
                    <xsl:value-of select="dsoBanChuaThue"/>
                  </xsl:when>
                  <xsl:otherwise>
                    0
                  </xsl:otherwise>
                </xsl:choose>
              </td>
              <td style ="text-align: right" class="text">
                <xsl:choose>
                  <xsl:when test="TienThue!=''">
                    <xsl:value-of select="TienThue"/>
                  </xsl:when>
                  <xsl:otherwise>
                    0
                  </xsl:otherwise>
                </xsl:choose>
              </td>
              <td style ="text-align: right" class="text">
                <xsl:choose>
                  <xsl:when test="ThanhTien!=''">
                    <xsl:value-of select="ThanhTien"/>
                  </xsl:when>
                  <xsl:otherwise>
                    0
                  </xsl:otherwise>
                </xsl:choose>
              </td>
              <td style ="text-align:left;">
                <xsl:value-of select="ghiChu"/>
              </td>
            </tr>
          </xsl:for-each>
        </tbody>
      </table>
      <p>
        <b>Tổng doanh thu hàng hoá, dịch vụ bán ra chịu thuế GTGT:</b>&#160;&#160;
        <b>
          <!--<xsl:value-of select="translate(translate(translate(format-number(//HSoThueDTu/HSoKhaiThue/PLuc/PL01_1_GTGT/tongDThuBRa, '###,###.##'),',','?'),'.',','),'?','.')"/>-->
          <xsl:value-of select="//HSoThueDTu/HSoKhaiThue/PLuc/PL01_1_GTGT/tongDThuBRa"/>
        </b>
      </p>
      <p>
        <b> Tổng số thuế GTGT của hàng hóa, dịch vụ bán ra:</b>&#160;&#160; <b>

          <!--<xsl:value-of select="translate(translate(translate(format-number(//HSoThueDTu/HSoKhaiThue/PLuc/PL01_1_GTGT/tongThueBRa, '###,###.##'),',','?'),'.',','),'?','.')"/>-->
          <xsl:value-of select="//HSoThueDTu/HSoKhaiThue/PLuc/PL01_1_GTGT/tongThueBRa"/>
        </b>
      </p>
      <p>
        <b> Tổng tiền hóa đơn:</b>&#160;&#160; <b>

          <!--<xsl:value-of select="translate(translate(translate(format-number(//HSoThueDTu/HSoKhaiThue/PLuc/PL01_1_GTGT/tongThueBRa, '###,###.##'),',','?'),'.',','),'?','.')"/>-->
          <xsl:value-of select="//HSoThueDTu/HSoKhaiThue/PLuc/PL01_1_GTGT/tongtien"/>
        </b>
      </p>
      <!--<p>Tôi cam đoan số liệu khai trên là đúng và chịu trách nhiệm trước pháp luật về những số liệu đã khai./... </p>
      <ul style="list-style:none">
        <li style="float:left;padding-right:280px">
          <b>NHÂN VIÊN ĐẠI LÝ THUẾ</b>
          <br/>Họ và tên:
          <br/>Chứng chỉ hành nghề số:
        </li>
        <li style="text-align:center">
          <span>
            Hà nội,Ngày <xsl:value-of select="substring(/HSoThueDTu/HSoKhaiThue/TTinChung/TTinTKhaiThue/TKhaiThue/ngayLapTKhai,9,2)"/> tháng <xsl:value-of select="substring(/HSoThueDTu/HSoKhaiThue/TTinChung/TTinTKhaiThue/TKhaiThue/ngayLapTKhai,6,2)"/> năm <xsl:value-of select="substring(/HSoThueDTu/HSoKhaiThue/TTinChung/TTinTKhaiThue/TKhaiThue/ngayLapTKhai,1,4)"/>
          </span>
          <BR/>
          <strong>
            NGƯỜI NỘP THUẾ hoặc <br/>
            ĐẠI DIỆN HỢP PHÁP CỦA NGƯỜI NỘP THUẾ
          </strong>
          <br/>
          <span style="padding-left:450px">Ký, ghi rõ họ tên, chức vụ và đóng dấu (nếu có)</span>
        </li>
      </ul>
      <br/>
      <br/>
      <strong style=" text-decoration:underline">Ghi chú:</strong>
      <p>(*) Tổng doanh thu hàng hóa, dịch vụ bán ra chịu thuế GTGT là tổng cộng số liệu tại cột 6 của dòng tổng của các chỉ tiêu 2, 3, 4.</p>
      <p>(**) Tổng số thuế GTGT của hàng hóa, dịch vụ bán ra là tổng cộng số liệu tại cột 7 của dòng tổng của các chỉ tiêu 2, 3, 4.</p>-->
    </div>
  </xsl:template>
</xsl:stylesheet>
