﻿
function DepartmentChange() {
    var jsondata = "idDepartment=" + $("#idDepartment").val();
    $.ajax({
        type: "POST",
        url: "/DSVNExternal/GetChildDepartment",
        data: jsondata,
        dataType: "json",
        async: false,
        success: function (result) {

            $("#idChildDepartment").empty();
            $("#idChildDepartment").append("<option  value=''>-- Chọn ga --</option>");
            $.each(result, function (index, optionData) {
                $("#idChildDepartment").append("<option value='" + optionData.ID + "'>" + optionData.Name + "</option>");
            });
        }
    });
}
$(function ($) {
    $(".vietnameseDate").mask("99/99/9999");
    $(".year").mask("9999");
});
function getHiddenValue() {
    var stringDate1 = $("#tungay").val();
    var d1 = Date.parseExact(stringDate1, "dd/MM/yyyy");
    if (d1 == null && stringDate1 != null) { // nhập không đúng định dạng ngày tháng
        $("#tungay").val("");
    }
    var stringDate2 = $("#denngay").val();
    var d2 = Date.parseExact(stringDate2, "dd/MM/yyyy");
    if (d2 == null && stringDate2 != null) { // nhập d2 không đúng định dạng
        $("#denngay").val("");
    }
    if (d1 != null && d2 != null && d1 > d2) {
        alert("");
        $("#denngay").val("");
    }
}
//Chi cho nhap so
function keypress(e) {
    var keypressed = null;
    if (window.event) {
        keypressed = window.event.keyCode; //IE
    }
    else {
        keypressed = e.which; //NON-IE, Standard
    }
    if (keypressed < 48 || keypressed > 57) {
        if (keypressed == 8 || keypressed == 127) {
            return;
        }
        return false;
    }
}
jQuery(function ($) {
    $.datepicker.regional['vi'] = {
        closeText: 'Đóng',
        prevText: '&#x3c;Trước',
        nextText: 'Tiếp&#x3e;',
        currentText: 'Hôm nay',
        monthNames: ['Tháng Một', 'Tháng Hai', 'Tháng Ba', 'Tháng Tư', 'Tháng Năm', 'Tháng Sáu',
'Tháng Bảy', 'Tháng Tám', 'Tháng Chín', 'Tháng Mười', 'Tháng Mười Một', 'Tháng Mười Hai'],
        monthNamesShort: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6',
'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
        dayNames: ['Chủ Nhật', 'Thứ Hai', 'Thứ Ba', 'Thứ Tư', 'Thứ Năm', 'Thứ Sáu', 'Thứ Bảy'],
        dayNamesShort: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
        dayNamesMin: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
        weekHeader: 'Tu',
        dateFormat: 'dd/mm/yy',
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
});
function test() {
    var FromDate = $("#tungay").val();
    var ToDate = $("#dengay").val();
    if (FromDate != "" && ToDate != "") {
        document.forms[0].submit()
    }
    if (FromDate != "" || ToDate != "") {
        if (FromDate != "" && ToDate == "") {
            alert("Từ ngày thống kê không được để trống.");
        }
        if (FromDate == "" && ToDate != "") {
            alert("Đến ngày thống kê không được để trống.");
        }
        return false;
    }
    else {
        document.forms[0].submit()
    }
}
$(document).ready(function () {
    $.datepicker.setDefaults($.datepicker.regional['vi']);
    $(".datepicker").datepicker({
        showOn: "button",
        buttonImage: "/Content/images/calendar.gif",
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true
    });
    $("#boxLoadPage .pager a").each(function () {
        $(this).bind("click", Pager);
        //$(this).click(function (e) {
        //});
    });
    function Pager(e) {
        e.preventDefault();
        var link = this.href;
        $("#boxLoadPage").load(link + " #contentLoad", function () {
            $("#contentLoad .pager a").each(function () {
                $(this).bind("click", Pager);
            });
        });
    }
});