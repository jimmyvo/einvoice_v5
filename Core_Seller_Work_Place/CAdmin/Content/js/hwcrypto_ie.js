/*
This is hwcrypto_ie.js 0.0.20 generated on 2016-09-21
Ngoc Tan (c) 2016
Updated on 2016-10-25
Tran Ha (c) 2016 - VNPT Software Company
DO NOT EDIT (use src/hwcrypto_ie.js)
*/

var Base64 = {
    // private property
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
	
    // public method for encoding
    encode: function(input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;

        input = Base64._utf8_encode(input);

        while (i < input.length) {

            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output +
                this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
                this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
        }

        return output;
    },

    // public method for decoding
    decode: function(input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;

        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        while (i < input.length) {
            enc1 = this._keyStr.indexOf(input.charAt(i++));
            enc2 = this._keyStr.indexOf(input.charAt(i++));
            enc3 = this._keyStr.indexOf(input.charAt(i++));
            enc4 = this._keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }
        }
        output = Base64._utf8_decode(output);
        return output;
    },

    // private method for UTF-8 encoding
    _utf8_encode: function(string) {
        string = string.replace(/\r\n/g, "\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {
            var c = string.charCodeAt(n);
            if (c < 128) {
                utftext += String.fromCharCode(c);
            } else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            } else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
        }
        return utftext;
    },

    // private method for UTF-8 decoding
    _utf8_decode: function(utftext) {
        var string = "";
        var i = 0;
        var c = 0;
        var c1 = 0;
        var c2 = 0;

        while (i < utftext.length) {
            c = utftext.charCodeAt(i);
            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            } else if ((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i + 1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            } else {
                c2 = utftext.charCodeAt(i + 1);
                c3 = utftext.charCodeAt(i + 2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }
        }
        return string;
    }
};

function str_obj(str) {
		str = str.split(',');
		var result = {};
		for (var i = 0; i < str.length; i++) {
			var cur = str[i].split('=');
			result[cur[0]] = cur[1];
		}
		return result;
}
	
String.prototype.replaceAll = function(search, replacement) {
var target = this;
return target.split(search).join(replacement);
};

function nonce() {
    var val = "";
    var hex = "abcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 16; i++) val += hex.charAt(Math.floor(Math.random() * hex.length));
    return val;
}


function ie_getCertificate() {
    try {
		var json = {
                type: [],
                lang:[],
                nonce:[],
                src:[],
                origin:[],
                tab:[]      
        };
        json.type = "CERT";
        json.lang = "en";
		json.nonce = nonce();
		json.src="page.js";
		json.origin="0";
		json.tab ="1";
		
		var jsonString= JSON.stringify(json);
        document.TOKENSIGN.MyParam = jsonString;
        var response = document.TOKENSIGN.Open();
        return response;
    } catch (error) {
      var message = error.message;
      message = message.replace(/(\r\n|\n|\r)/gm,"");
      var bError = '{"message" : "' + message + '", "result" : "technical_error"}';
      var response = Base64.encode(bError);
      return response;
    }
}
//-- thuuu show cer
function ie_showCertificate(cert){
    try {
		var json = {
                type: [],
                lang:[],
                nonce:[],
                src:[],
                origin:[],
                cert:[],
                tab:[]      
        };
        json.type = "SHOWCER";
        json.lang = "en";
		json.nonce = nonce();
		json.src="page.js";
		json.origin="0";
        json.cert=cert.cert;
		json.tab ="1";
		
		var jsonString= JSON.stringify(json);
        document.TOKENSIGN.MyParam = jsonString;
        var response = document.TOKENSIGN.Open();
        return response;
    } catch (error) {
      var message = error.message;
      message = message.replace(/(\r\n|\n|\r)/gm,"");
      var bError = '{"message" : "' + message + '", "result" : "technical_error"}';
      var response = Base64.encode(bError);
      return response;
    }
}

function ie_signHashData(cert,hash){
	try {
		var json = {
                type: [],
                lang:[],
                nonce:[],
                src:[],
                origin:[],
                tab:[],
                hashtype:[],
                cert:[],
				hash:[]
        };
        json.type = "SIGN";
        json.lang = "en";
		json.hashtype=hash.type;
		json.cert = cert;
		json.nonce = nonce();
		json.hash = hash.hex;
		json.src="page.js";
		json.origin="0";
		json.tab ="1";
		
		var jsonString= JSON.stringify(json);
		document.TOKENSIGN.MyParam = jsonString;
        var response = document.TOKENSIGN.Open();
        return response;
    } catch (error) {
      var message = error.message;
      message = message.replace(/(\r\n|\n|\r)/gm,"");
      var bError = '{"message" : "' + message + '", "result" : "technical_error"}';
      var response = Base64.encode(bError);
      return response;
    }
	
}

function ie_signXMLData(hashtype,hash){
	try {
		var json = {
                type: [],
                lang:[],
                nonce:[],
                src:[],
                origin:[],
                tab:[],
                hashtype:[],
                cert:[],
				hash:[]
        };
        json.type = "SIGN";
        json.lang = "en";
		json.hashtype=hashtype.hashtype;
		json.cert = "";
		json.nonce = nonce();
		json.hash = hashtype.hex;
		json.src="page.js";
		json.origin="0";
		json.tab ="1";
		var jsonString= JSON.stringify(json);
        document.TOKENSIGN.MyParam = jsonString;
        var response = document.TOKENSIGN.Open();
        return response;
    } catch (error) {
      var message = error.message;
      message = message.replace(/(\r\n|\n|\r)/gm,"");
      var bError = '{"message" : "' + message + '", "result" : "technical_error"}';
      var response = Base64.encode(bError);
      return response;
    }
}


function ie_signMultiXMLData(hashArr,hashtype){
	try {
		var json = {
                type: [],
                lang:[],
                nonce:[],
                src:[],
                origin:[],
                tab:[],
                hashtype:[],
                cert:[],
				hash:[]
        };
        json.type = "SIGN";
        json.lang = "en";
		json.hashtype=hashtype.hashtype;
		json.cert = hashtype.cer;
		json.nonce = nonce();
		json.hash = hashArr;
		json.src="page.js";
		json.origin="0";
		json.tab ="1";
		
		var jsonString= JSON.stringify(json);
        document.TOKENSIGN.MyParam = jsonString;
        var response = document.TOKENSIGN.Open();
        return response;
    } catch (error) {
      var message = error.message;
      message = message.replace(/(\r\n|\n|\r)/gm,"");
      var bError = '{"message" : "' + message + '", "result" : "technical_error"}';
      var response = Base64.encode(bError);
      return response;
    }
}

function ie_doCryptoFunction(type, file_uplink, file_downlink, options) {
	try {
    var request = "";
    var jsonCookies = "";
	//var cryptOptions = options.lang.replace(new RegExp('\"', 'g'), '"');
	//cryptOptions = options.lang.replace(new RegExp('\\\"', 'g'), '"');
		var getLink = "";
            var allCookieInfo = "";
            if (file_downlink) {
               if (file_downlink.constructor === Array) {
                  getLink = file_downlink[0];
                } else {
                  getLink = file_downlink;
                }
            }
	
		var cryptoki = {
                type: [],
                lang:[],
                nonce:[],
                src:[],
                origin:[],
                tab:[],
                uplink:[],
                downlink:[]
        };
        cryptoki.type = type;
        cryptoki.lang = options.lang;
        cryptoki.nonce = nonce();
        cryptoki.src = "page.js";
        cryptoki.origin = "0";
        cryptoki.tab = "1";
        cryptoki.uplink = file_uplink;
        cryptoki.downlink = file_downlink;
        
		var cookies = document.TOKENSIGN.getCookies(getLink);
		
		if (cookies === "")
		{
			//request =  '{"type":"' + type +'", "uplink" : "' + file_uplink + '", "downlink" : "' + file_downlink + '", "lang" : "' + cryptOptions + '", "nonce" : "' + nonce() + '", "src" : "page.js", "origin" : "0", "tab" : 1}';
		} else {
		var members = cookies.split('#');
		for (var i = 0; i < members.length; i++)
		{
		 
			jsonCookies += JSON.stringify(str_obj(members[i]));
			if (i !== members.length - 1)
				jsonCookies += "#";
		}
		
		var Jcookies = jsonCookies.replaceAll('"', '\\"').replaceAll(" ", "");
		if (Jcookies) {
		  //request =  '{"type":"' + type +'", "uplink" : "' + file_uplink + '", "downlink" : "' + file_downlink + '", "cookie": "'+ Jcookies
		  //+ '", "lang" : "' + cryptOptions + '", "nonce" : "' + nonce() + '", "src" : "page.js", "origin" : "0", "tab" : 1}';
		  cryptoki.cookie = Jcookies;
		}
		}
		request = JSON.stringify(cryptoki);
		document.TOKENSIGN.MyParam = request;
		var response = document.TOKENSIGN.Open();
		return response;
	} catch (error) {
		  var message = error.message;
      message = message.replace(/(\r\n|\n|\r)/gm,"");
      var bError = '{"message" : "' + message + '", "result" : "technical_error"}';
      var response = Base64.encode(bError);
      return response;
	}
}