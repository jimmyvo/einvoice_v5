﻿$(document).ready(function () {
    changeVAT();
});

this.changeVAT = function () {
    $("#VATRate").change(function () {
        var vatRate = $("#VATRate option:selected").val();
        for (var i = 0; i < $(".VATRate").length; i++) {
            $(".VATRate")[i].value = vatRate;
        }
    });
};
