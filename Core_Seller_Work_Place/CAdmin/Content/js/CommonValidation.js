﻿
//---Validation
$('form:first').validate({
    rules: {
        Serial: {
            required: true
        },
        CusName: {
            required: true
        },
        PaymentMethod: {
            required: true
        },
        Buyer: {
            required: true
        }
    },
    messages: {
        Serial: {
            required: "Bạn chưa nhập thông tin."
        },
        CusName: {
            required: "Bạn chưa nhập thông tin."
        },
        PaymentMethod: {
            required: "Bạn chưa nhập thông tin."
        },
        Buyer: {
            required: "Bạn chưa nhập thông tin."
        }
    }
});

//---/Validation
