﻿(function ($) {
    $.fn.extend({
        ContentPagination: function (options) {
            var defaults = {
                height: 400
            };
            var options = $.extend(defaults, options);

            //Creating a reference to the object
            var objContent = $(this);

            // other inner variables
            var fullPages = new Array();
            var subPages = new Array();
            var height = 0;
            var activePage = 1;
            var pageCount = 1;
            // initialization function
            init = function () {
                objContent.children().each(function (i) {
                    if (height + this.clientHeight > options.height) {
                        fullPages.push(subPages);
                        subPages = new Array();
                        height = 0;
                        pageCount++;
                    }

                    height += this.clientHeight;
                    subPages.push(this);
                    $(this).addClass("p" + pageCount);
                });

                if (height > 0) {
                    fullPages.push(subPages);
                }

                // draw controls
                showPaginationBar(pageCount);

                // show first page
                showPageContent(1);
            };

            // show page content function
            showPageContent = function (page) {
                objContent.children().css("display", "none");
                $(".p" + page).css("display", "");
                $(".number").removeClass("selected");
                $("#ap" + page).addClass("selected");
                activePage = page;
            };

            // show pagination bar function (draw switching numbers)
            showPaginationBar = function (numPages) {
                var pagins = '';
                for (var i = 1; i <= numPages; i++) {
                    pagins += '<span class="number" id="ap' + i + '" onclick="showPageContent(' + i + '); return false;">' + i + '</span>';
                }
                $('.pagination span:first-child').after(pagins);
            };

            // perform initialization
            init();

            // and binding 2 events - on clicking to Prev
            $('.pagination #prev').click(function () {
                if (activePage > 1)
                    showPageContent(activePage - 1);
            });
            // and Next
            $('.pagination #next').click(function () {
                if (activePage < pageCount)
                    showPageContent(activePage + 1);
            });
            // print function
            var printContent = "";
            $('.pagination #convertPagination').click(function () {
                $(".pagination").css("display", "none");
                for (var i = 1; i <= pageCount; i++) {
                    showPageContent(i);
                    tempContent = $("#ViewInvoice").html();
                    printContent += tempContent + "<p style='page-break-before: always'/>";
                }
                $("#ds").html(printContent);
                var printElement = document.getElementById("ds");
                $(printElement).printArea({
                    mode: "iframe",
                    popWd: 900,
                    popHt: 600,
                    popClose: false
                });
            });

        },
        ProductNumberPagination: function (options) {
            var defaults = {
                number: 10
            };
            var options = $.extend(defaults, options);
            //Creating a reference to the object
            var objContent = $(this);
            // other inner variables
            var fullPages = new Array();
            var subPages = new Array();
            var k = 0;
            var activePage = 1;
            var pageCount = 1;
            // initialization function
            init = function () {
                objContent.children('div.pagecurrent').slice(0, objContent.children('div.pagecurrent').length).each(function (i) {
                    k++;
                    if (k > options.number) {
                        fullPages.push(subPages);
                        subPages = new Array();
                        subPages.push(this);
                        k = 1;
                        pageCount++;
                        $(this).addClass("p" + pageCount);
                    }

                    //k++;
                    subPages.push(this);
                    $(this).addClass("p" + pageCount);
                });

                if (k > 0) {
                    fullPages.push(subPages);
                }

                // draw controls
                showPaginationBar(pageCount);

                // show first page
                showPageContent(1);
            };

            // show page content function
            showPageContent = function (page) {
                objContent.children().css("display", "none");
                $(".p" + page).css("display", "");
                $(".number").removeClass("selected");
                $("#ap" + page).addClass("selected");
                $("#countPage").text("tiep theo trang truoc - trang " + page + "/" + pageCount);
                //th1.chi co mot trang
                if (pageCount == 1) {
	            $("#countPage").text("");
                    //$("#countPage").text("trang " + page + "/" + pageCount);
                    $("#countPage").css("display", "none");
                    $("#desHead").css("display", "none");
                    $(".Amount_words").css("display", "");
                    $(".Amount").css("display", "");
                    $(".InsurranceRate").css("display", "");
                    $(".VAT_Rate").css("display", "");
                    $(".VAT_Amount").css("display", "");
                    $(".Total").css("display", "");
                }
                //th2.truong hop nhieu trang
                if (pageCount > 1) {

                    //trang thu 1
                    if (page == 1) {
                        var t = fullPages;
                        var tong = 0;
                        $("#countPage").text("");
                       $("#countPage").text("trang " + page + "/" + pageCount);
                        $("#desHead").css("display", "none");
                        $("#desHead").css("text-align", "center");
                        $(".pageDescription").text("Tiếp trang sau");
                        $(".Amount_words").css("display", "none");
                        $(".Amount").css("display", "none");
                        $(".InsurranceRate").css("display", "none");
                        $(".bgimg").css("display", "none");
                        $(".Total").css("display", "none");
                        $(".nextpage").css("display", "none");
                        //                        $(".desFoot").css("display", "none");
                        //                        $(".desFoot").css("text-align", "center");
                        $(".pageDescriptionFoot").text("Tiếp trang sau");
                    }
                        //trang tiep theo
                    else if (page > 1 && page < pageCount) {
                        //	                    $("#countPage").text("tiep theo trang truoc - trang " + page + "/" + pageCount);                        
                        $(".Amount_words").css("display", "none");
                        $(".Amount").css("display", "none");
                        $(".InsurranceRate").css("display", "none");
                        $(".bgimg").css("display", "none");
                        $(".Total").css("display", "none");
                        $(".nextpage").css("display", "none");
                    }
                        //trang cuoi cung
                    else if (page == pageCount) {                       
                        //	                    $("#countPage").text("tiep theo trang truoc - trang " + page + "/" + pageCount);	                                
                        $(".Amount_words").css("display", "");
                        $(".Amount").css("display", "");
                        $(".InsurranceRate").css("display", "");
                        $(".ComNameSignDate").css("display", "");
                        $('.bgimg').css("display", "");
                        $(".Total").css("display", "");
                        $(".nextpage").css("display", "");
                    }
                }
            };
            // show pagination bar function (draw switching numbers)
            showPaginationBar = function (numPages) {
                var pagins = '';
                for (var i = 1; i <= numPages; i++) {
                    pagins += '<span class="number" id="ap' + i + '" onclick="showPageContent(' + i + '); return false;">' + i + '</span>';
                }
                $('.pagination span:first-child').after(pagins);
            };

            // perform initialization
            init();

            // and binding 2 events - on clicking to Prev
            $('.pagination #prev').click(function () {
                if (activePage > 1){
                    showPageContent(activePage - 1);
                    activePage = activePage - 1;
                }
            });
            // and Next
            $('.pagination #next').click(function () {
                if (activePage < pageCount) {
                    showPageContent(activePage + 1);
                    activePage = activePage + 1;
                }
            });
            // print function
            var printContent = "";
            $('.pagination #convertPagination').click(function () {
                $(".pagination").css("display", "none");
                $('.pagecurrent').children().css("display", "block");
                var printElement = document.getElementById("ViewInvoice");
                $('#ViewInvoice').bPopup().close();
                $(printElement).printArea({
                    mode: "iframe",
                    popWd: 900,
                    popHt: 600,
                    popClose: false
                });
               
                //NQuyet - 18/04/2017 - Sua reload page
              
            });
        }
    });
}(jQuery));
