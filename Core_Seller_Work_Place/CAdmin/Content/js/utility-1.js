﻿// needed for Table Column ordering
function tableOrdering(order, dir) {
    // get form element
    var form = document.adminForm;
    
    // Change talbe order
    form.order.value = order;
    form.orderDir.value = dir;
    
    // Submit the admin form
    form.submit();
}
function ShowDialog(strdialog) {
    $('#' + strdialog).dialog('open');
};
function CloseDialog(strdialog) {
    $('#' + strdialog).dialog('close');
};
function ChangeStatusEnquiry(strStatus, val) {
    document.getElementById(strStatus).value = val;
};
function ChangeRequiredClass(tagreq, isrequired) {
    if (parseInt(isrequired) > 0) {
        $("#" + tagreq).addClass('required');
    } else {
        $("#" + tagreq).removeClass('required');
    }
};
function DoPrint() {
    this.print();
};
function split(val) {
    return val.split(/,\s*/);
};
function extractLast(term) {
    return split(term).pop();
};
function changeStatusImage(idimage, valuechange, enquiryid) {

    var strImg = "";
    if (valuechange == 1) {
        strImg = "<img src=" + "\"/Images/icon-24-accept.png\"" + " onclick= \"changeStatusImage('" + idimage + "',0," + enquiryid + ");\"/>";
    } else {
        strImg = "<img src=" + "\"/Images/icon-24-login.png\"" + " onclick= \"changeStatusImage('" + idimage + "',1," + enquiryid + ");\"/>";
    }
    document.getElementById(idimage).innerHTML = "";
    document.getElementById(idimage).innerHTML = strImg;
    var url = '/AjaxData/ChangePublic/' + enquiryid;
    $.ajax({
        url: url,
        cache: false,
        success: function(html) {
            var result = html;
            if (result == "1") {
                alert("Thay đổi trạng thái của bài viết thành công!");
            } else {
            alert("Thay đổi trạng thái của bài viết thất bại");
            }
        }
    });
};
