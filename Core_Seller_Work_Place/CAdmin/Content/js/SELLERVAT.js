﻿var totalOld;
var VATAmountOld;
var AmountOld;
$(document).ready(function () {
    ///Chặn Copy Paste Cut trong các mục quantity price amount bảng Product
    $('table.products tbody tr').each(function (i, row) {
        $(row).children('td:nth-child(6)').bind('cut copy paste', function (e) { return false; });
        $(row).children('td:nth-child(7)').bind('cut copy paste', function (e) { return false; });
        $(row).children('td:nth-child(8)').bind('cut copy paste', function (e) { return false; });
        $(row).children('td:nth-child(9)').bind('cut copy paste', function (e) { return false; });
        $(row).children('td:nth-child(10)').bind('cut copy paste', function (e) { return false; });
        $(row).children('td:nth-child(11)').bind('cut copy paste', function (e) { return false; });
    });
    totalOld = parseFloat($('#Total').val().FormatNumUK());
    VATAmountOld = parseFloat($('#VATAmount').val().FormatNumUK());
    AmountOld = parseFloat($('#Amount').val().FormatNumUK());

    $('input[type=text]:first').focus();
    //$('#CusTaxCode').ForceNumericOnly();
    // just remove function format number in input has class _number
    /// (Just remove)Sự kiện tải thông tin khách hàng 20170315

    
    typeCurrency = $("#CurrencyUnit").val();
    searchCusName();
    searchCodeCus();
    bindEvents2Table();
    $('.formatTaxCode').formatTaxCode();
    checkTaxCode();
    //--- tỷ giá tiền tệ
    loadCurrencyUnit();
    changeCurrencyUnit();
    changeExchangeRate();
    $("#moneySymbol")[0].innerHTML = typeCurrency;
    //--- /tỷ giá tiền tệ
});
this.typeCurrency = "";
this.submit = function () {
    /// Nếu mã số thuế không đúng, không cho submitform
    //Extra_begin
    var lstDic = new Array();
    $("[id*='Extra_']").each(function (index, element) {
        if (element.type == "checkbox") lstDic.push({ key: element.name, value: element.checked });
        else
            lstDic.push({ key: element.name, value: element.value });
    });

    $("#DicExsource").val(JSON.stringify(lstDic))
    //Extra_end
    if ($('#errorTaxCode').length > 0) {
        $('#errorTaxCode').show();
        $('#CusTaxCode').addClass('error');

        if (!confirm('Xin lỗi, mã số thuế của khách hàng không chuẩn.\n- Bạn có muốn tiếp tục lưu hóa đơn không?')) {
            return false;
        }
    }
    if ($('#CusTaxCode').val()) {
        $('#CusAddress').addClass("required");
    }
    else {
        $('#CusAddress').removeClass("required");
    }

    if (parseFloat($('#Total').val().FormatNumUK()) >= 20000000 && $('#PaymentMethod').val() != 'CK') {
        if (!confirm('Số tiền quá lớn, bạn nên thanh toán chuyển khoản.\n- Bạn có muốn tiếp tục lưu hóa đơn không?')) {
            return false;
        }
    }

    var _products = [];
    var tgFlag = '';
    var prodNameFlag = '';
    var prodVatRateFlag = '';
    /// Lấy danh sách sản phẩm
    $('table.products tbody tr').each(function (i, row) {
        var _product = {};
        var _pCode = $(row).children('td:nth-child(3)').find('input[type=text]:first');
        var _pName = $(row).children('td:nth-child(4)').find('input[type=text]:first');
        var _pUnit = $(row).children('td:nth-child(5)').find('input[type=text]:first');
        var _pQuantity = $(row).children('td:nth-child(6)').find('input[type=text]:first');
        var _pPrice = $(row).children('td:nth-child(7)').find('input[type=text]:first');
        var _pTotal = $(row).children('td:nth-child(8)').find('input[type=text]:first');
        var _pVatRate = $(row).children('td:nth-child(9)').find(':selected:first').val();
        var _pVatAmount = $(row).children('td:nth-child(10)').find('input[type=text]:first');
        var _pAmount = $(row).children('td:nth-child(11)').find('input[type=text]:first');
        var _pIsSum = $(row).children('td:nth-child(12)').find('input[type=checkbox]:first');
        if (_pName.val()) {
            _product.Code = _pCode.val() || '';
            _product.Name = _pName.val() || '';
            _product.Unit = _pUnit.val() || '';
            _product.Quantity = _pQuantity.val() || 0;
            _product.Price = _pPrice.val() || 0;
            _product.Total = _pTotal.val() || 0;
            _product.VATRate = _pVatRate || -1;
            _product.VATAmount = _pVatAmount.val() || 0;
            _product.Amount = _pAmount.val() || 0;
            _product.ProdType = 1;
            _product.IsSum = _pIsSum.is(':checked') == false ? 0 : 1;
            _products.push(_product);
            // NQuyet mô đi phai
            // if nếu nhập số lượng mà méo nhập thành tiền thì bắt nó nhập = đc thì thôi
            if ((!_pPrice.val() && _pQuantity.val()) || (!_pQuantity.val() && _pPrice.val())) {
                tgFlag = 'false';
                return false;
            }

            // Nếu có sản phẩm nhưng chưa chọn thuế suất => Alert
            if (_pVatRate == '' || _pVatRate == null) {
                prodVatRateFlag = 'false';
                return false;
            }
        }
        if ((!_pName.val() && _pQuantity.val()) || (!_pName.val() && _pPrice.val()) || (!_pName.val() && !_pPrice.val() && !_pQuantity.val() && _pTotal.val())) {
            prodNameFlag = 'false';
        }
        // NQuyet end 
    });
    if (_products.length == 0 || prodNameFlag == 'false') {
        alert('Xin lỗi, danh sách sản phẩm chưa có hoặc thông tin sản phẩm bị thiếu!');
        return false;
    }

    if (tgFlag) {
        alert('Xin lỗi, danh sách sản phẩm có số lượng nhưng chưa có đơn giá hoặc ngược lại!');
        return false;
    }

    if (prodVatRateFlag == 'false') {
        alert('Xin lỗi, Sản phẩm chưa chọn thuế suất!');
        return false;
    }
    if ($('#VATAmount').val().toString().indexOf('-') >= 0 || $('#Total').val().toString().indexOf('-') >= 0 || $('#Amount').val().toString().indexOf('-') >= 0) {
        var r = confirm("Hóa đơn có giá trị [Cộng tiền dịch vụ] hoặc [Tiền thuế] hoặc [Tổng cộng tiền thanh toán] âm , bạn có muốn tiếp tục ?");
        if (r == true) {
        } else {
            return false;
        }
    }
    //Kiểm tra các giá trị âm
    if ($('#CusName').val() && $('#PaymentMethod').val()) {
        if ($('#VATAmount').val().toString().indexOf('-') >= 0) {
            var _vatAmount = $('#VATAmount').val().FormatNumUK() * (-1);
            $('#VATAmount').val(_vatAmount.format(0, 3, typeCurrency))
        }
        if ($('#Total').val().toString().indexOf('-') >= 0) {
            var _total = $('#Total').val().FormatNumUK() * (-1);
            $('#Total').val(_total.format(0, 3, typeCurrency))
        }
        if ($('#Amount').val().toString().indexOf('-') >= 0) {
            var _amount = $('#Amount').val().FormatNumUK() * (-1);
            $('#Amount').val(_amount.format(0, 3, typeCurrency))
        }
    }
    $('#PubDatasource').val(JSON.stringify(_products));
    $("input[type=text]._number").each(function (i, item) {
        var _val = $(this).val();
        $(this).val(_val.replaceAll('.', ''))
    });
};
/// Tải lại toàn bộ các sự kiện cho table
/// Sự kiện tải thông tin khách hàng
this.searchCusName = function () {
    $("#CusName").autocomplete({
        focus: function (event, ui) { }, minLength: 1, select: function (event, ui) {
            $("#CusTaxCode").val(ui.item.TaxCode);
            $("#CusAddress").val(ui.item.Address);
            $("#CusPhone").val(ui.item.Phone);
            $("#CusCode").val(ui.item.Code);
            $("#EmailDeliver").val(ui.item.EmailDeliver);
        }, source: function (request, response) {
            $.ajax({
                data: { searchText: request.term }, dataType: "JSON", success: function (data) {
                    response($.map(data, function (item) {
                        return { id: item.cusid, value: item.cusname, TaxCode: item.TaxCode, Address: item.Address, Phone: item.Phone, Code: item.Code, EmailDeliver: item.EmailDeliver }
                    }))
                }, type: "POST", url: "/Customer/SeachByName"
            })
        },
        appendTo: "#searchAutocomplete",
        open: function () {
            var position = $(this).position(),
            left = position.left, top = position.top + 20;
            $("#searchAutocomplete > ul").css({
                left: left + "px",
                top: top + "px"
            });
        }
    }).change(function () {
        if (!$(this).val() && !$("#Buyer").val()) {
            $("#CusTaxCode").val("");
            $("#CusAddress").val("");
            $("#CusPhone").val("");
            $("#CusCode").val("");
            $("#EmailDeliver").val("");
        }
    });
};
this.searchCodeCus = function () {
    /// Sự kiện tải thông tin khách hàng
    $("#CusCode").autocomplete({
        focus: function (event, ui) { }, minLength: 1, select: function (event, ui) {
            $("#CusTaxCode").val(ui.item.TaxCode);
            $("#CusAddress").val(ui.item.Address);
            $("#CusPhone").val(ui.item.Phone);
            $("#Buyer").val(ui.item.cusname);
            $("#EmailDeliver").val(ui.item.EmailDeliver);
        }, source: function (request, response) {
            $.ajax({
                data: { searchText: request.term }, dataType: "JSON", success: function (data) {
                    response($.map(data, function (item) {
                        return { id: item.cusid, value: item.Code, TaxCode: item.TaxCode, Address: item.Address, Phone: item.Phone, cusname: item.cusname, EmailDeliver: item.EmailDeliver }
                    }))
                }, type: "POST", url: "/Customer/SeachByCusCode"
            })
        },
        appendTo: "#searchAutocomplete",
        open: function () {
            var position = $(this).position(),
            left = position.left, top = position.top + 20;
            $("#searchAutocomplete > ul").css({
                left: left + "px",
                top: top + "px"
            });
        }
    }).change(function () {
        if (!$(this).val()) {
            $("#CusTaxCode").val("");
            $("#CusAddress").val("");
            $("#CusPhone").val("");
            $("#Buyer").val("");
            $("#EmailDeliver").val("");
        }
    });
};

this.bindEvents2Table = function () {
    $('table.products tbody input')
        .unbind() // gỡ các sự kiện
        .removeData(); // gỡ toàn bộ dữ liệu
        //.autocomplete('destroy');
    formatTagInputNumber($("#CurrencyUnit").val());
    /// Nạp sự kiện xóa dòng cho các dòng
    $('table.products tbody tr').each(function (i, row) {
        var _del = $(row).children('td:first');
        $(_del).unbind().click(function () {
            if ($('table.products tbody tr').length > 2) {
                if (confirm('Bạn có chắc chắn muốn xóa dòng sản phẩm này?')) {
                    $(row).remove();
                    $('table.products tbody tr').each(function (ii, rr) {
                        $(rr).children('td:nth-child(2)').text(ii + 1);
                    });
                    $('table.products tbody input.Total:first').trigger('change');
                    Totalofpayment();
                }
            }
        })
    });

    /// Sự kiện thay đổi số lượng
    $('table.products tbody .quantity').change(function () {
        $(this).parents('tr:first').find('input.TotalQuantity').trigger('change');
        $(this).parents('tr:first').find('input.Total').trigger('change');
        $(this).parents('tr:first').find('input.VATAmount').trigger('change');
        $(this).parents('tr:first').find('input.amount').trigger('change');
        Totalofpayment();
    });

    /// Sự kiện thay đổi giá tiền
    $('table.products tbody .price').change(function () {
        $(this).parents('tr:first').find('input.Total').trigger('change');
        $(this).parents('tr:first').find('input.VATAmount').trigger('change');
        $(this).parents('tr:first').find('input.amount').trigger('change');
        Totalofpayment();
    });
    /// Sự kiện thay đổi thuế xuất % amount
    $('table.products tbody .VATRate').change(function () {
        var _VATRate = $(this).val();
        if ($(this).val() != '' && (parseInt(_VATRate) != 0 && parseInt(_VATRate) != 5 && parseInt(_VATRate) != 10 && parseInt(_VATRate) != -1 && parseInt(_VATRate) != -2)) {
            alert('Giá trị thuế suất của sản phẩm không đúng!');
        }
        $(this).parents('tr:first').find('input.Total').trigger('change');
        $(this).parents('tr:first').find('input.VATAmount').trigger('change');
        $(this).parents('tr:first').find('input.amount').trigger('change');
        Totalofpayment();
    });
    /// Sự kiện thay đổi thành tiền
    $('table.products tbody .Total').change(function () {
        var _quantity = parseFloat($(this).parents('tr:first').find('input.quantity').val().FormatNumUK() || 0) || 0;
        var _price = parseFloat($(this).parents('tr:first').find('input.price').val().FormatNumUK() || 0) || 0;

        /// Cập nhật thành tiền
        /// Bắt sự kiện thành tiền để cập nhật tổng tiền
        if (_quantity * _price > 0) {
            $(this).val(parseFloat(_quantity * _price).format(0, 3, typeCurrency));
        }
        else {
            var _total = $(this).val().FormatNumUK();
            if (_total != "") {
                $(this).parents('tr:first').find('input.amount').val(_total.format(0, 3, typeCurrency));
            }
            Totalofpayment();
        };

    });
    //sự kiên thay đổi tiền thuế
    $('table.products tbody .VATAmount').change(function () {
        //var _vatRate = parseFloat($(this).parents('tr:first').find('input.VATRate').val().replaceAll(',', '') || 0) || 0;
        var _vatRate = parseFloat($(this).parents('tr:first').find('select option:selected').val());
        var _total = parseFloat($(this).parents('tr:first').find('input.Total').val().FormatNumUK() || 0) || 0;
        if (_vatRate > 0) {
            $(this).val(parseFloat(_total / 100 * _vatRate).format(0, 3, typeCurrency));
        }
        else if (_vatRate == 0) {
            $(this).val(0);
        } else {
            $(this).val("");
        }

    });
    //sự kiện thay đổi tổng tiền=tiền thuế + tiền sản phẩm
    $('table.products tbody .amount').change(function () {
        var _total = parseFloat($(this).parents('tr:first').find('input.Total').val().FormatNumUK() || 0) || 0;
        var _VATAmount = parseFloat($(this).parents('tr:first').find('input.VATAmount').val().FormatNumUK() || 0) || 0;
        if (_total + _VATAmount > 0) {
            $(this).val(parseFloat(_total + _VATAmount).format(0, 3, typeCurrency));
        };
    });
    /// Không tính tiền
    $('table.products tbody input[type=checkbox]').change(function () {
        Totalofpayment();
    });
    /// Tự động điền sản phẩm từ tên sản phẩm
    $('table.products tbody input.name')
        .data('autocomplete_on', false)
        .bind('autocompleteopen', function (event, ui) {
            $(this).data('autocomplete_on', true);
        })
        .bind('autocompleteclose', function (event, ui) {
            $(this).data('autocomplete_on', false);
        })
        .autocomplete({
            source: function (request, response) {
                $.ajax(
                {
                    type: "POST",
                    url: '/Product/SeachByName',
                    dataType: "JSON",
                    data: {
                        searchText: request.term
                    },
                    success: function (data) {
                        response($.map(data, function (item) {
                            return {
                                label: item.Name,
                                Name: item.Name,
                                Code: item.Code,
                                Unit: item.Unit,
                                Price: item.Price
                            }
                        }));
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert(textStatus);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                $(this).parents('tr:first').find('input.code').val(ui.item.Code);
                $(this).parents('tr:first').find('input.unit').val(ui.item.Unit);
                $(this).parents('tr:first').find('input.name').val(ui.item.Name);
                $(this).parents('tr:first').find('input.price').val(ui.item.Price).trigger('focusout');
                if (event.which != 9 || event.keyCode != 9) {
                    $(this).parents('tr:first').find('input.quantity').focus();
                } else {
                    $(this).parents('tr:first').find('input.unit').focus();
                };
                //$(this).val(ui.item.Name).trigger('change');
                return false;
            },
            appendTo: "#searchAutocomplete",
            open: function () {
                var position = $(this).position(),
                left = position.left, top = position.top + 20;
                $("#searchAutocomplete > ul").css({
                    left: left + "px",
                    top: top + "px"
                });
            }
        });
    /// Tự động điền thong tin san pham tu ma san pham
    $('table.products tbody input.code')
        .data('autocomplete_on', false)
        .bind('autocompleteopen', function (event, ui) {
            $(this).data('autocomplete_on', true);
        })
        .bind('autocompleteclose', function (event, ui) {
            $(this).data('autocomplete_on', false);
        })
        .autocomplete({
            source: function (request, response) {
                $.ajax(
                {
                    type: "POST",
                    url: '/Product/SeachByCodeProduct',
                    dataType: "JSON",
                    data: {
                        searchText: request.term
                    },
                    success: function (data) {
                        response($.map(data, function (item) {
                            return {
                                label: item.Code,
                                Code: item.Code,
                                Name: item.Name,
                                Unit: item.Unit,
                                Price: item.Price
                            }
                        }));
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert(textStatus);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                $(this).parents('tr:first').find('input.name').val(ui.item.Name);
                $(this).parents('tr:first').find('input.unit').val(ui.item.Unit);
                $(this).parents('tr:first').find('input.price').val(ui.item.Price).trigger('focusout');
                if (event.which != 9 || event.keyCode != 9) {
                    $(this).parents('tr:first').find('input.quantity').focus();
                } else {
                    $(this).parents('tr:first').find('input.unit').focus();
                }
                $(this).val(ui.item.Code).trigger('change');
                return false;
            },
            appendTo: "#searchAutocomplete",
            open: function () {
                var position = $(this).position(),
                left = position.left, top = position.top + 20;
                $("#searchAutocomplete > ul").css({
                    left: left + "px",
                    top: top + "px"
                });
            }
        });

    

    // Nếu là dòng cuối cùng thì thêm dòng mới
    $('table.products tbody input:text').change(function () {
        var _this = $(this);
        var _rowsc = _this.parents('tbody').children('tr').length;
        if (_this.parents('tr:first').children('td:nth-child(2)').text() == _rowsc) {
            var _addable = false;
            _this.parents('tr:first').find('input:text').each(function (i, item) {
                if ($(item).val()) {
                    _addable = true;
                }
            });
            if (_addable) {
                var _row = $('<tr><td class="delr"></td>' +
                                  '<td class="center">' + (_rowsc + 1) + '</td>' +
                                  '<td><input type="text" class="code"></td>' +
                                  '<td><input type="text" class="name"></td>' +
                                  '<td><input type="text" class="unit"></td>' +
                                  '<td><input type="text" class="quantity textr _number"></td>' +
                                  '<td><input type="text" class="price textr _number"></td>' +
                                  '<td><input type="text" class="Total textr _number"></td>' +
                                  '<td><select class="VATRate" id="VATRate" name="VATRate"><option selected="selected" value="">Chọn</option><option value="-1">Không chịu thuế</option><option value="-2">Không kê khai và nộp thuế</option><option value="0">0 %</option><option value="5">5 %</option><option value="10">10 %</option></select></td>' +
                                  '<td><input type="text" class="VATAmount textr _number"></td>' +
                                  '<td><input type="text" class="amount textr _number"></td>' +
                                  '<td class="center"><input type="checkbox"></td></tr>');
                $('table.products tbody').append(_row);
                /// Bind lại toàn bộ sự kiện
                bindEvents2Table();
            }
        }
    });
};

function Totalofpayment() {
    var _grossAmount = 0;
    var _grossAmount0 = 0;
    var _grossAmount5 = 0;
    var _grossAmount10 = 0;
    var _grossAmount_NonTax = 0;
    var _totalGrossAmount = 0;
    var _totalQuantity = 0;
    var _vatAmount5 = 0;
    var _vatAmount10 = 0;
    var _totalVatAmount = 0;
    $('table.products tbody tr').each(function (i, row) {
        var _pTotal = $(row).children('td:nth-child(8)').find('input[type=text]').val().FormatNumUK();
        if (_pTotal == "") return;
        var _checkboxIsum = $(row).children('td:nth-child(12)').find('input[type=checkbox]:first').is(':checked');
        if (_checkboxIsum == true) return;
        //var _pVatRate = $(row).children('td:nth-child(9)').find('input[type=select]').val();
        var _pVatRate = $(row).children('td:nth-child(9)').find(':selected').val();
        var _pVatAmount = $(row).children('td:nth-child(10)').find('input[type=text]').val().FormatNumUK();

        if (_pVatRate == "0") {
            _grossAmount0 += parseFloat(_pTotal);
        }
        else if (_pVatRate == "5") {
            _grossAmount5 += parseFloat(_pTotal);
            _vatAmount5 += parseFloat(_pVatAmount);
        }
        else if (_pVatRate == "10") {
            _grossAmount10 += parseFloat(_pTotal);
            _vatAmount10 += parseFloat(_pVatAmount);
        }
        else {
            if (_pVatRate == "-1") {
                $(row).children('td:nth-child(9)').find('input[type=text]').val('');
                $(row).children('td:nth-child(10)').find('input[type=text]').val('');
                $(row).children('td:nth-child(11)').find('input[type=text]').val(_pTotal.format(0, 3, typeCurrency));
                _grossAmount += parseFloat(_pTotal);
            } else {
                if (_pVatRate == "-2") {
                    $(row).children('td:nth-child(9)').find('input[type=text]').val('');
                    $(row).children('td:nth-child(10)').find('input[type=text]').val('');
                    $(row).children('td:nth-child(11)').find('input[type=text]').val(_pTotal.format(0, 3, typeCurrency));
                    _grossAmount_NonTax += parseFloat(_pTotal);
                }
            }
        }
        var _pTotalQuantity = $(row).children('td:nth-child(6)').find('input[type=text]').val().FormatNumUK();
        _totalQuantity += _pTotalQuantity;
    })
    //tinh tien truoc thue
    $("#GrossValue").val(parseFloat(_grossAmount).format(0, 3, typeCurrency));
    $("#GrossValue0").val(parseFloat(_grossAmount0).format(0, 3, typeCurrency));
    $("#GrossValue5").val(parseFloat(_grossAmount5).format(0, 3, typeCurrency));
    $("#GrossValue10").val(parseFloat(_grossAmount10).format(0, 3, typeCurrency));
    $("#GrossValueNonTax").val(parseFloat(_grossAmount_NonTax).format(0, 3, typeCurrency));
    //Tổng số lượng
    $("#TotalQuantity").val(_totalQuantity.format(0, 3, typeCurrency));
    //tong tien truoc thue
    var _amount = parseFloat(_grossAmount) + parseFloat(_grossAmount0) + parseFloat(_grossAmount5) + parseFloat(_grossAmount10) + parseFloat(_grossAmount_NonTax);
    totalOld = _amount;
    $("#Total").val(_amount.format(0, 3, typeCurrency));
    //tinh tien sau thue
    $("#VatAmount0").val(0);
    $("#VatAmount5").val(parseFloat(_vatAmount5).format(0, 3, typeCurrency));
    $("#VatAmount10").val(parseFloat(_vatAmount10).format(0, 3, typeCurrency));
    //tong tien thue
    var _vatamount = parseFloat(_vatAmount5) + parseFloat(_vatAmount10);
    VATAmountOld = _vatamount;
    $("#VATAmount").val(_vatamount.format(0, 3, typeCurrency));
    //tong tien
    AmountOld = _amount + _vatamount;
    $("#Amount").val(parseFloat(_amount + _vatamount).format(0, 3, typeCurrency));
    //tong tien viet bang chu
    $("#AmountInWords").val(($("#Amount").val().FormatNumUK()).ReadNumber(typeCurrency));
    // when amount change convert money again
    convertAmount();
}
/// Thay đổi số tiền
$('#Total').change(function () {
    var Total = $('#Total').val();
    var $label = $("label[for='lblMesErr']");
    //if (totalOld - Total < -1000 || totalOld - Total > 1000) {
    //    alert("Cộng tiền dịch vụ chỉ chênh 1.000 VNĐ so với giá trị thực");
    //    //$label.text('Cộng tiền dịch vụ chỉ chênh 1.000 VNĐ so với giá trị thực');
    //    $('#Total').val(totalOld);
    //}
    //else {
    //    $label.text('');
    //    var _amount = parseFloat($('#Total').val().replaceAll(',', '')) + parseFloat($('#VATAmount').val().replaceAll(',', ''));
    //    $("#Amount").val(_amount.format(0, 3, typeCurrency));
    //    $('#AmountInWords').val(_amount.ReadNumber(typeCurrency));
    //    AmountOld = parseFloat($('#Amount').val().replaceAll(',', ''));
    //};
    $label.text('');
    var _amount = parseFloat($('#Total').val().FormatNumUK()) + parseFloat($('#VATAmount').val().FormatNumUK());
    $("#Amount").val(_amount.format(0, 3, typeCurrency));
    $('#AmountInWords').val($('#Amount').val().FormatNumUK().ReadNumber(typeCurrency));
    AmountOld = parseFloat($('#Amount').val().FormatNumUK());
});

/// Thay đổi tiền thuế
$('#VATAmount').change(function () {
    var VATAmount = $('#VATAmount').val();
    var $label = $("label[for='lblMesErr']");
    $label.text('');
    var _amount = parseFloat($('#Total').val().FormatNumUK()) + parseFloat($('#VATAmount').val().FormatNumUK());
    $("#Amount").val(_amount.format(0, 3, typeCurrency));
    $('#AmountInWords').val($('#Amount').val().FormatNumUK().ReadNumber(typeCurrency));
    AmountOld = parseFloat($('#Amount').val().FormatNumUK());
});

/// Thay đổi tiền phải thanh toán cuối cùng
$('#Amount').change(function () {
    var Amount = $('#Amount').val().FormatNumUK();
    var $label = $("label[for='lblMesErr']");
    if (AmountOld - Amount < -1000 || AmountOld - Amount > 1000) {
        alert("Tổng tiền thanh toán chỉ chênh 1.000 VNĐ so với giá trị thực");
        //$label.text('Tổng tiền thanh toán chỉ chênh 1.000 VNĐ so với giá trị thực');
        $('#Amount').val(AmountOld);
    }
    else {
        $label.text('');
        var _amount = parseFloat($('#Amount').val().FormatNumUK());
        $('#AmountInWords').val(_amount.ReadNumber(typeCurrency));
    }
});
/// Sao chép event
jQuery.event.copy = function (from, to) {
    from = from.jquery ? from : jQuery(from);
    to = to.jquery ? to : jQuery(to);

    var events = from[0].events || jQuery.data(from[0], "events") || jQuery._data(from[0], "events");
    if (!from.length || !to.length || !events) return;

    return to.each(function () {
        for (var type in events) {
            debugger;
            for (var handler in events[type])
                jQuery.event.add(this, type, events[type][handler], events[type][handler].data);
        }
    });
};
/// just remove Chỉ cho nhập số vào textbox
/// just remove Format số tiền 20170315  
// just remove Replaces all instances of the given substring.15

// xử lý chọn ngày tháng datetime
//function getHiddenValue() {
//    var stringDate1 = $("#ArisingDate").val();
//    var d1 = Date.parseExact(stringDate1, "d/M/yyyy");
//    if (d1 == null && stringDate1 != null) { // nhập không đúng định dạng ngày tháng
//        $("#ArisingDate").val("");
//    }
//}

//$(function ($) {
//    $(".vietnameseDate").mask("99/99/9999");
//});

function activeAddress() {
    var str = $("#CusTaxCode").val();
    if (str) {
        $('#CusAddress').addClass("required");
    }
    else {
        $('#CusAddress').removeClass("required");
    }
}

//--- Validate Check Mã số thuế
this.checkTaxCode = function () {
    $('#CusTaxCode').blur(function () {
        var _this = $(this);
        $('#errorTaxCode').remove();
        if (_this.val()) {
            $.ajax({
                type: "POST",
                url: "/Company/checkMST/",
                data: {
                    mst: $(this).val()
                },
                success: function (data) {
                    if (!data) {
                        _this.addClass('error').parent('.control').append('<label id="errorTaxCode" for="CusTaxCode" generated="true" class="error">Mã số thuế không hợp lệ</label>');
                        $('#CusTaxCode').focus();
                    }
                    else {
                        _this.removeClass('error');
                        $('#CusAddress').addClass("required");
                    }
                }
            });
        }
        else {
            _this.removeClass('error');
            $('#CusAddress').removeClass("required");
        }
    });
};
//--- /Validate Check Mã số thuế
// ---phần thêm tỷ giá tiền tệ
this.loadCurrencyUnit = function () {
    var _kieuHd = $("#CurrencyUnit").val();
    if (_kieuHd == "VND") {
        document.getElementById("divTyGia").style.display = "none";
        document.getElementById("divTongTien").style.display = "none";
    } else if (_kieuHd == "USD") {
        document.getElementById("divTyGia").style.display = "block";
        document.getElementById("divTongTien").style.display = "block";
    }
};
this.changeCurrencyUnit = function () {
    $("#CurrencyUnit").change(function () {
        $("#ExchangeRate").val(0);
        $("#ConvertedAmount").val(0);
        if ($(this).val() == "VND") {
            document.getElementById("divTyGia").style.display = "none";
            document.getElementById("divTongTien").style.display = "none";
        } else if ($(this).val() == "USD") {
            document.getElementById("divTyGia").style.display = "block";
            document.getElementById("divTongTien").style.display = "block";
        };
        typeCurrency = $("#CurrencyUnit").val();
        formatTagInputNumber(typeCurrency);
        $("#moneySymbol")[0].innerHTML = $(this).val();
        $("#VATRate").trigger("change");
    });
};
this.changeExchangeRate = function () {
    $("#ExchangeRate").focusout(function () {
        convertAmount();
    });
};
this.convertAmount = function () {
    var _tyGia;
    var strTyGia = $("#ExchangeRate").val().FormatNumUK();
    if (strTyGia) {
        _tyGia = parseFloat(strTyGia);
    } else {
        _tyGia = 0;
    }
    if ($("#Amount").val() && _tyGia > 0 && $("#CurrencyUnit").val() == "USD") {
        var _amount = parseFloat($("#Amount").val().FormatNumUK());
        var _TongSoTien = parseFloat(_amount * _tyGia);
        //$("#TongSoTien").val(_TongSoTien).attr("value", _TongSoTien.format(0, 3));
        $("#ConvertedAmount").val(_TongSoTien.format(0, 3, "USD"));
    }
};
this.formatTagInputNumber = function (typeCurrency) {
    $("#ExchangeRate").formatOnlyNumber();
    $("#ConvertedAmount").InitialMoneyUSD();
    $('table.products tbody input._number').unbind('focusout');
    $('table.products tbody input._number')
       .each(function (i, item) {
           fnInitialFormatNumber(this);
           $(this).parents('tr:first').find('input.price').trigger('change');
       }).ForceNumericOnly()
       .focus(function () {
           var _val = $(this).val();
           $(this).val(_val.replaceAll('.', ''));
       })
       .focusout(function () {
           fnFocusOut(this);
           $(this).parents('tr:first').find('input.price').trigger('change');
       });
    $('table.list-bin-bottom tbody input._number').unbind('focusout');
    $('table.list-bin-bottom tbody input._number')
      .each(function (i, item) {
          fnInitialFormatNumber(this);
      }).ForceNumericOnly()
      .focus(function () {
          var _val = $(this).val();
          $(this).val(_val.replaceAll('.', ''));
      })
      .focusout(function () {
          fnFocusOut(this);
      });
};




// ---/phần thêm tỷ giá tiền tệ