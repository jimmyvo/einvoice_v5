﻿$(document).ready(function () {
    // remove 
});
var ChuSo = new Array(" không ", " một ", " hai ", " ba ", " bốn ", " năm ", " sáu ", " bảy ", " tám ", " chín ");
var Tien = new Array("", " nghìn", " triệu", " tỉ", " nghìn tỉ", " triệu tỉ");

//1. Hàm đọc số có 3 chữ số
function DocSo3ChuSo(baso) {
    var tram;
    var chuc;
    var donvi;
    var KetQua = "";
    tram = parseInt(baso / 100);
    chuc = parseInt((baso % 100) / 10);
    donvi = baso % 10;
    if (tram == 0 && chuc == 0 && donvi == 0) return "";
    if (tram != 0) {
        KetQua += ChuSo[tram] + " trăm ";
        if ((chuc == 0) && (donvi != 0)) KetQua += " linh ";
    }
    if ((chuc != 0) && (chuc != 1)) {
        KetQua += ChuSo[chuc] + " mươi";
        if ((chuc == 0) && (donvi != 0)) KetQua = KetQua + " linh ";
    }
    if (chuc == 1) KetQua += " mười ";
    switch (donvi) {
        case 1:
            if ((chuc != 0) && (chuc != 1)) {
                KetQua += " một ";
            }
            else {
                KetQua += ChuSo[donvi];
            }
            break;
        case 5:
            if (chuc == 0) {
                KetQua += ChuSo[donvi];
            }
            else {
                KetQua += " lăm ";
            }
            break;
        default:
            if (donvi != 0) {
                KetQua += ChuSo[donvi];
            }
            break;
    }
    return KetQua;
} // End fucntion Docsoco3chuso
//2. Hàm đọc thành chữ (Sử dụng hàng đọc có 3 chữ số)
Number.prototype.ReadNumber = function (typeCurrence) {
    var SoTien = this.valueOf();
    var _typeCurrence = typeCurrence;
    var strTienNguyen = SoTien.toString().split('.')[0];
    var strTienLe = SoTien.toString().split('.')[1];
    var strDocTienLe = '';
    var lan = 0;
    var i = 0;
    var so = 0;
    var KetQua = "";
    var tmp = "";
    var ViTri = new Array();
    if (SoTien < 0) return "Số tiền âm !";
    if (SoTien == 0) {
        if (typeCurrence === "VND") {
            return "Không đồng";
        } else {
            if (typeCurrence === "USD") {
                return "Không đô la Mỹ";
            }
        }
    }
    if (SoTien > 0) {
        so = SoTien;
    }
    else {
        so = -SoTien;
    }
    if (SoTien > 8999999999999999) {
        //SoTien = 0;
        return "Sô quá lớn!";
    }
    ViTri[5] = Math.floor(so / 1000000000000000);
    if (isNaN(ViTri[5]))
        ViTri[5] = "0";
    so = so - parseFloat(ViTri[5].toString()) * 1000000000000000;
    ViTri[4] = Math.floor(so / 1000000000000);
    if (isNaN(ViTri[4]))
        ViTri[4] = "0";
    so = so - parseFloat(ViTri[4].toString()) * 1000000000000;
    ViTri[3] = Math.floor(so / 1000000000);
    if (isNaN(ViTri[3]))
        ViTri[3] = "0";
    so = so - parseFloat(ViTri[3].toString()) * 1000000000;
    ViTri[2] = parseInt(so / 1000000);
    if (isNaN(ViTri[2]))
        ViTri[2] = "0";
    ViTri[1] = parseInt((so % 1000000) / 1000);
    if (isNaN(ViTri[1]))
        ViTri[1] = "0";
    ViTri[0] = parseInt(so % 1000);
    if (isNaN(ViTri[0]))
        ViTri[0] = "0";
    if (ViTri[5] > 0) {
        lan = 5;
    }
    else if (ViTri[4] > 0) {
        lan = 4;
    }
    else if (ViTri[3] > 0) {
        lan = 3;
    }
    else if (ViTri[2] > 0) {
        lan = 2;
    }
    else if (ViTri[1] > 0) {
        lan = 1;
    }
    else {
        lan = 0;
    }
    for (i = lan; i >= 0; i--) {
        tmp = DocSo3ChuSo(ViTri[i]);
        KetQua += tmp;
        if (ViTri[i] > 0) KetQua += Tien[i];
        if ((i > 0) && (tmp.length > 0)) KetQua += ',';//&& (!string.IsNullOrEmpty(tmp))
    }
    if (KetQua.substring(KetQua.length - 1) == ',') {
        KetQua = KetQua.substring(0, KetQua.length - 1);
    }
    if (strTienLe !== undefined && parseFloat(strTienLe) !== 0) {
        strTienLe = strTienLe + "00";
        strDocTienLe = DocSo3ChuSo(strTienLe.substring(0, 2));
        if (_typeCurrence == "VND") {
            KetQua = KetQua.substring(1, 2).toUpperCase() + KetQua.substring(2) + " phẩy " + strDocTienLe + " đồng";
        } else {
            KetQua = KetQua.substring(1, 2).toUpperCase() + KetQua.substring(2) + " đô la Mỹ " + strDocTienLe + " cents";
        }

    } else {
        if (_typeCurrence == "VND") {
            KetQua = KetQua.substring(1, 2).toUpperCase() + KetQua.substring(2) + " đồng";
        } else {
            KetQua = KetQua.substring(1, 2).toUpperCase() + KetQua.substring(2) + " đô la Mỹ";
        }
    }
    return KetQua;
}

///3. Format số tiền ####
Number.prototype.format = function (n, x, typeCurrency, isQuantity) {

    var numberBehindComma = 2
    if ($("#NumberBehindComma").val() !== undefined & $("#NumberBehindComma").val() !== null) {
        numberBehindComma = $("#NumberBehindComma").val();
    }
    var re = "";
    if (isQuantity) {
        var n = numberBehindComma, x = 3;
    } else {
        if (typeCurrency == "USD") {
            n = numberBehindComma;
        } else {
            // nếu là VND 
            n = numberBehindComma;
        };
    };
    if (isNaN(this.valueOf())) {
        return undefined;
    }
    var value = parseFloat(this.toFixed(numberBehindComma));
    re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    value = value.toFixed(Math.max(0, ~ ~n)).replace(new RegExp(re, 'g'), '$&,');
    var result = value.toString().replaceAll(",", "_").replaceAll(".", ",").replaceAll("_", ".");
    return result;
};
Number.prototype.formatGroupTotal = function (typeCurrent) {
    if (isNaN(this.valueOf())) {
        return undefined;
    };
    var n = 0, x = 3;
    if (typeCurrent == "VND") {
        n = 0;
    } else {
        if (typeCurrent == "USD") {
            n = 2;
        }
    };
    var value = parseFloat(this.toFixed(n));
    re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    value = value.toFixed(Math.max(0, ~ ~n)).replace(new RegExp(re, 'g'), '$&,');
    var result = value.toString().replaceAll(",", "_").replaceAll(".", ",").replaceAll("_", ".");
    return result;
};
//4. Replaces all instances of the given substring.15
String.prototype.replaceAll = function (
    strTarget, // The substring you want to replace
    strSubString // The string you want to replace in.
    ) {
    var strText = this;
    var intIndexOfMatch = strText.indexOf(strTarget);

    // Keep looping while an instance of the target string
    // still exists in the string.
    while (intIndexOfMatch != -1) {
        // Relace out the current instance.
        strText = strText.replace(strTarget, strSubString)

        // Get the index of any next matching substring.
        intIndexOfMatch = strText.indexOf(strTarget);
    } // -- End strSubString

    // Return the updated string with ALL the target strings
    // replaced out with the new substring.
    return (strText);
}
//5. dịnh dạng lại datepicker theo tieng viet
jQuery(function ($) {
    $.datepicker.regional['vi'] = {
        closeText: 'Đóng',
        prevText: '&#x3c;Trước',
        nextText: 'Tiếp&#x3e;',
        currentText: 'Hôm nay',
        monthNames: ['Tháng Một', 'Tháng Hai', 'Tháng Ba', 'Tháng Tư', 'Tháng Năm', 'Tháng Sáu',
'Tháng Bảy', 'Tháng Tám', 'Tháng Chín', 'Tháng Mười', 'Tháng Mười Một', 'Tháng Mười Hai'],
        monthNamesShort: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6',
'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
        dayNames: ['Chủ Nhật', 'Thứ Hai', 'Thứ Ba', 'Thứ Tư', 'Thứ Năm', 'Thứ Sáu', 'Thứ Bảy'],
        dayNamesShort: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
        dayNamesMin: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
        weekHeader: 'Tu',
        dateFormat: 'dd/mm/yy',
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
});
//6. Chỉ cho nhập số vào textbox (nếu input có class comma thì có thể nhập dấu ','?)
this.checkDotComma = function (numberStr, keyPress) {
    if (numberStr !== null & numberStr !== undefined) {
        var arr = numberStr.toString().split('.');
        // check prevent more than 2 dot and comma after dot
        if (arr.length == 2) {
            if (arr[1].indexOf(',') > 0) {
                return false;
            }
            if (keyPress === 46 || keyPress === 44) {
                return false;
            }
            return true;
        };
        if (arr.length > 2) {
            return false;
        };
        return true;
    }
}
jQuery.fn.ForceNumericOnly = function () {
    return this.each(function () {
        $(this).blur(function () {
            if ($(this).val() == "NaN") {
                $(this).val(0);
            }
        });
        $(this).keypress(function (e) {
            var className = $(this).attr('class');
            //---function base only number allow '-','.',',' 
            var keypressed = null;
            if (window.event) { //IE
                keypressed = window.event.keyCode;
                if (className.indexOf('number') > -1 || className.indexOf('quantity') > -1) {
                    // 46 là dấu '.'; 45 là dấu âm; 44 là dấu ','
                    if ((keypressed < 48 && keypressed != 46 && keypressed != 44) || (keypressed > 57 && keypressed != 189)) {
                        return false;
                    } else {
                        return checkDotComma(e.target.value, keypressed);
                    }
                }
            } else {
                keypressed = e.which; //NON-IE, Standard
                if (keypressed === 8) { return; } // if key is backspace allow remove character
                if (className.indexOf('number') > -1 || className.indexOf('quantity') > -1) {
                    // 46 là dấu '.'; 45 là dấu âm; 44 là dấu ','
                    if ((keypressed < 48 && keypressed != 46 && keypressed != 44) || (keypressed > 57 && keypressed != 189)) {
                        return false;
                    } else {
                        return checkDotComma(e.target.value, keypressed);
                    }
                }
            };
            //--- CND 20170317

            //--- /CND 20170317
        });
    });
};
jQuery.fn.formatTaxCode = function () {
    // only keydown you can get backspace
    $(this).bind("keydown", function (e) {
        var code = e.keyCode || e.which;
        if (code === 8) { // if keydown is backspace allow remove character
            var element = e.target;
            setTimeout(function (e) {
                var value = $(element).val();
                value = value.replace('-', '');
                if (value.length > 10) {
                    value = value.substring(0, 10) + '-' + value.substring(10, value.length);
                    $(element).val(value);
                } else {
                    $(element).val(value)
                };
            }, 10);
        }
    });

    $(this).keypress(function (e) {
        var className = $(this).attr('formatTaxCode');
        //---function base only number allow '-','.',',' 
        var keypressed = e.keyCode || e.which; // IE (Chrom) hoặc Firfox
        var value = e.target.value;
        if (keypressed === 8) { return; } // if key is backspace allow remove character
        if ((47 < keypressed && 58 > keypressed) || keypressed === 45) {
            if (value.length === 10 && keypressed !== 45) {
                $(this)[0].value = value + "-";
                return true;
            };
            if (keypressed === 45 && value.length != 10) {
                return false;
            };
        } else {
            return false;
        };
    });

    $(this).bind('paste', function (e) {
        var element = this;
        setTimeout(function () {
            var value = $(element).val();
            value = value.replace('-', '');
            if (value.match(/^[0-9]+$/) == null) {
                $(element).val('');
            } else {
                if (value.length > 13) {
                    $(element).val('');
                } else {
                    if (value.length > 10) {
                        value = value.substring(0, 10) + '-' + value.substring(10, value.length);
                        $(element).val(value);
                    } else {
                        $(element).val(value);
                    }
                }
            };
        }, 10);
    });
};
jQuery.fn.InitialMoneyUSD = function () {
    var _val = $(this).val();
    if (_val) {
        _val = _val.replaceAll('.', '').replaceAll(',', '.');
        if (_val.indexOf('.') > -1) {
            $(this).val(parseFloat(_val).format(2, 3, "USD", true));
        } else {
            $(this).val(parseFloat(_val).format(0, 3));
        }
    };
    $(this).ForceNumericOnly()
       .focus(function () {
           var _val = $(this).val();
           $(this).val(_val.replaceAll('.', ''));
       })
       .focusout(function () {
           var _val = $(this).val().replaceAll('.', '').replaceAll(',', '.');
           if (_val) {
               var _fr = _val.indexOf('.') > -1 ? parseFloat(_val).format(2, 3, "USD", true) : parseFloat(_val).format(0, 3, "USD", true);
               $(this).val(_fr || '');
               $(this).trigger('change');
           };
       });
}
jQuery.fn.formatOnlyNumber = function () {
    var _val = $(this).val();
    if (_val) {
        _val = _val.replaceAll('.', '').replaceAll(',', '.');
        $(this).val(parseFloat(_val).format(0, 3));
    };
    $(this).ForceNumericOnly()
       .focus(function () {
           var _val = $(this).val();
           $(this).val(_val.replaceAll('.', ''));
       })
       .focusout(function () {
           var _val = $(this).val().replaceAll('.', '').replaceAll(',', '.');
           if (_val) {
               var _fr = _val.indexOf('.') > -1 ? parseFloat(_val).format(0, 3) : parseFloat(_val).format(0, 3);
               $(this).val(_fr || '');
               $(this).trigger('change');
           };
       });
};

this.fnInitialFormatNumber = function (element) {
    var _val = $(element).val();
    if (_val) {
        _val = _val.replaceAll('.', '').replaceAll(',', '.');
        if ($(element)[0].className.indexOf('quantity') > -1) {
            if (_val.indexOf('.') > -1) {
                $(element).val(parseFloat(_val).format(2, 3, typeCurrency, true));
            } else {
                $(element).val(parseFloat(_val).format(0, 3));
            }
        } else {
            if (typeCurrency == "VND") {
                $(element).val(parseFloat(_val).format(0, 3));
            } else {
                if (typeCurrency == "USD") {
                    $(element).val(parseFloat(_val).format(2, 3, typeCurrency));
                }
            }
        }
    };
};

this.fnFocusOut = function (element) {
    var _val = $(element).val().replaceAll('.', '').replaceAll(',', '.');
    if ($(element)[0].className.indexOf('quantity') > -1) {
        if (_val) {
            var _fr = _val.indexOf('.') > -1 ? parseFloat(_val).format(2, 3, typeCurrency, true) : parseFloat(_val).format(0, 3, typeCurrency, true);
            $(element).val(_fr || '').trigger('change');
        }
    } else {
        if (_val) {
            var _fr = _val.indexOf('.') > -1 ? parseFloat(_val).format(2, 3, typeCurrency, false) : parseFloat(_val).format(0, 3, typeCurrency, false);
            $(element).val(_fr || '').trigger('change');
        }
    };
};

this.focusOutGroupTotal = function (element, typeCurrency) {
    var _val = $(element).val().replaceAll('.', '').replaceAll(',', '.');
    if (_val) {
        var _fr = parseFloat(_val).formatGroupTotal(typeCurrency);
        $(element).val(_fr || '');
    }
}

//4. Replaces all instances of the given substring.15
String.prototype.FormatNumUK = function () {
    if (this.toString() === "") {
        return "";
    };
    var result = this.toString().replaceAll(".", "").replace(",", ".");
    result = parseFloat(result);
    return result;
};

String.prototype.FormatNumUK2VN = function () {
    var result = this.replaceAll(",", "_").replaceAll(".", ",").replaceAll("_", ".");
    return result;
};
//5. round result decimal to read money.
Number.prototype.FormatRound = function (typeCurrency) {
    var _typeCurrency = typeCurrency === undefined ? "VND" : typeCurrency;
    var n = 0;
    var x = 3;
    if (_typeCurrency === "VND") {
        n = 0;
    };
    if (_typeCurrency === "USD") {
        n = 2;
    };
    var value = parseFloat(this.toFixed(n));
    re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    value = value.toFixed(Math.max(0, ~ ~n)).replace(new RegExp(re, 'g'), '$&,');
    var result = value.toString().replaceAll(",", "_").replaceAll(".", ",").replaceAll("_", ".");
    return result;
}
