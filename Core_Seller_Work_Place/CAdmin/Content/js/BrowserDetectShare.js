﻿/// <reference path="jquery-1.3.2.js" />
/// file này dành cho việc download plugin và hiển thị hóa đơn ajax
function create(htmlStr) {
    var frag = document.createDocumentFragment(),
        temp = document.createElement('div');
    temp.innerHTML = htmlStr;
    while (temp.firstChild) {
        frag.appendChild(temp.firstChild);
    }
    return frag;
}

$(function () {
    //    var fragment = create('<object id="plugin0" width="1" height="1" type="application/x-testplugin"></object>');
    //    document.body.insertBefore(fragment, document.body.childNodes[0]);
    //    fragment = create('<div id="4plugin"></div>')
    //    document.body.insertBefore(fragment, document.body.childNodes[0]);
    fragment = create('<div id="ViewInvoice"><div id="container"></div></div>')
    if ($("#ViewInvoice").length == 0)
    document.body.insertBefore(fragment, document.body.childNodes[0]);
    //    plugin = plugin0;

    //    var BrowserDetect = {
    //        init: function () {
    //            this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
    //            this.PluginRedirect(this.browser)
    //        },
    //        searchString: function (data) {
    //            for (var i = 0; i < data.length; i++) {
    //                var dataString = data[i].string;
    //                var dataProp = data[i].prop;
    //                this.versionSearchString = data[i].versionSearch || data[i].identity;
    //                if (dataString) {
    //                    if (dataString.indexOf(data[i].subString) != -1)
    //                        return data[i].identity;
    //                }
    //                else if (dataProp)
    //                    return data[i].identity;
    //            }
    //        },
    //        PluginRedirect: function (browser) {
    //            if (browser == 'Explorer') {
    //                //try {
    //                //    var plugin = new ActiveXObject('Home.TestPlugin.1');
    //                //} catch (e) {
    //                document.getElementById("4plugin").innerHTML = '<object classid="clsid:CD4C6734-0B0C-5439-8F4F-9CF57DBC26E9" width="1" height="1" VIEWASTEXT codebase="/Content/cab/CertActiveX.cab#Version=1,0,0,0"></object>';
    //                //}                      
    //            }
    //            else if (browser == "Firefox") {
    //                document.getElementById("4plugin").innerHTML = '<object id="thePlugin" type="application/x-testplugin" width="1" height="1" codebase="/Content/cab/CertInfo.xpi"><a href="/Content/cab/CertInfo.xpi">Install plugin for Firefox</a></object>';
    //            }
    //            else if (browser == "Chrome") {
    //                for (i = 0; i < navigator.plugins.length; i++) {
    //                    if (navigator.plugins[i].filename == "npTestPlugin.dll")
    //                        return;
    //                }
    //                document.getElementById("4plugin").innerHTML = '<a href="/Content/cab/CertPlugin.crx">Download Plugin for Chrome</a>';
    //            }
    //        },
    //        dataBrowser: [
    //            {
    //                string: navigator.userAgent,
    //                subString: "Chrome",
    //                identity: "Chrome"
    //            },
    //            {
    //                string: navigator.userAgent,
    //                subString: "Firefox",
    //                identity: "Firefox"
    //            },
    //            {
    //                string: navigator.userAgent,
    //                subString: "MSIE",
    //                identity: "Explorer",
    //                versionSearch: "MSIE"
    //            }
    //        ]
    //    };
    //    BrowserDetect.init();
})



function ajxCall(idInvoice, pattern) {
    var jsondata = { idInvoice: idInvoice, pattern: pattern };
    $.ajax({
        type: "POST",
        url: "/Share/ajxPreview",
        data: jsondata,
        success: function (data) {
            $("#container").html(data + "<div class='pagination'><span id='prev' class='prevnext'></span><span id='next' class='prevnext'></span><input type='button' style='float:right' class='button' value='In hóa đơn' id='convertPagination' onclick=\"printScr('" + idInvoice + "','" + pattern + "');\" /></div>");
            $(function () {
                //$("#ViewInvoice").dialog({
                //    modal: true,
                //    height: 660,
                //    width: 880
                //});
                $("#4plugin").remove();
                $("#plugin0").remove();
                $('#ViewInvoice').bPopup({
                    easing: 'easeOutBack', //uses jQuery easing plugin
                    speed: 450,
                    transition: 'slideDown'
                });
                if ($('.pagination span').length == 2)
                    $("#quantitypages").ProductNumberPagination({ number: 1 });
            });
        }
    });
}

function ajxCall4Convert(idInvoice, pattern, str) {
    var jsondata = { id: idInvoice, patt: pattern };
    $.ajax({
        type: "POST",
        url: "/InvConvertion/" + str,
        data: jsondata,
        success: function (data) {
            //document.getElementById("ViewInvoice").innerHTML = data;
            if (data == "nochange" || data == "nosuccess") {
                ChangePage();
            }
            else {
                if (str == "ConvertForVerify") {
               
                    $($("input:radio[name='cbid']:checked").parent().parent().find("td")[6]).html("Đã Chuyển đổi");
                    $("input:radio[name='cbid']:checked").attr("onclick", "CheckConverted(1)");
                    document.getElementById("sourceBtn").disabled = true;
                }
                //phan trang
                //                $("#container").html(data + "<div class='pagination'><span id='prev' class='prevnext'><</span><span id='next' class='prevnext'>><input type='button' value='Chuyển đổi' id='convertPagination'/></span></div>");
                $("#container").html(data + "<div class='pagination'><span id='prev' class='prevnext'></span><span id='next' class='prevnext'></span><input type='button' style='float:right' class='button' value='Chuyển đổi' id='convertPagination' onclick=\"printScr('" + idInvoice + "','" + pattern + "');\" /></div>");
                var fragment = create('<div id="ds" style="display:none"></div>')
                document.body.insertBefore(fragment, document.body.childNodes[0]);
                $(function () {
                    //$("#ViewInvoice").dialog({
                    //    modal: true,
                    //    height: 650,
                    //    width: 830,
                    //    close: function (ev, ui) {
                    //        ChangePage();
                    //    }
                    //});
                    $('#ViewInvoice').bPopup({
                        easing: 'easeOutBack', //uses jQuery easing plugin
                        speed: 450,
                        transition: 'slideDown'
                    });
                    //phan trang
                    if ($('.pagination span').length == 2)
                        $("#quantitypages").ProductNumberPagination({ number: 1 });
                });
            }
        }

    });
}

function showCer(certBase64) {
    //var certBase64 = document.getElementById('ShowCertificateViewer').value;
    if (certBase64 === null || certBase64.length === 0) {
        alert("Không tìm thấy thông tin chứng thư số");
        return;
    }
    vnpt_plugin.checkPlugin().then(function (data) {
        vnpt_plugin.setLicenseKey(keyPluginv2).then(function (data) {
            var jsObLicense = JSON.parse(data);
            if (jsObLicense.code !== 1) {
                alert("Chưa set license cho plugin ký số!");
                return;
            }
            vnpt_plugin.ShowCertificateViewer(certBase64).then(function (data) {
                console.log(data);
                //alert(data);
            });
        }).catch(function (e) {
            alert("Set License: " + e);
        });;
    }).catch(function (e) {
        $('#dialogCheckEx').bPopup({
            easing: 'easeOutBack', //uses jQuery easing plugin
            speed: 450,
            transition: 'slideDown'
        });
    });;
}

function showMessage(data) {
    var jsOb = JSON.parse(JSON.parse(data)[0]);
    if (jsOb.code === 0) {
        //document.getElementById('output').value = jsOb.data;
    }
    else {
        var err = "Lỗi không xác định";
        switch (jsOb.code) {
            case 1:
                err = "Dữ liệu đầu vào không đúng định dạng";
                break;
            case 2:
                err = "Không lấy được thông tin chứng thư số";
                break;
            case 3:
                err = "Có lỗi trong quá trình ký số";
                break;
            case 4:
                err = ("Chứng thư số không có khóa bí mật");
                break;
            case 5:
                err = ("Lỗi không xác định");
                break;
            case 6:
                err = ("Ký pdf: không tìm thấy tham số số trang cần ký");
                break;
            case 7:
                err = ("Ký pdf: trang đặt chữ ký không tồn tại");
                break;
            case 8:
                err = ("Ký xml: không tìm thấy thẻ ký số");
                break;
            case 9:
                err = ("Ký pdf: không tìm thấy id của thẻ ký số");
                break;
            case 10:
                err = ("Dữ liệu ký đã chứa một hoặc nhiều chữ ký không hợp lệ");
                break;
            case 11:
                err = ("Người dùng hủy bỏ");
                break;
            default:
                err = ("Lỗi không xác định");
                break;
        }
        alert(err);
        //document.getElementById('output').value = err;
    }
}
function signArrDataAdvanced(hash) {
    var arrData = [];
    var dataJS = {};
    var arrayHash = new Array();
    arrayHash = hash.split(",");
    for (i = 0; i < arrayHash.length; i++) {
        dataJS.data = arrayHash[i];
        dataJS.type = "hash"
        var jsData = "";
        jsData += JSON.stringify(dataJS);
        arrData.push(jsData);
    }
    var serial = $('#serialCertSign').val();
    //var serial = "";
    vnpt_plugin.checkPlugin().then(function (data) {
        vnpt_plugin.setLicenseKey(keyPluginv2).then(function (data) {
            var jsObLicense = JSON.parse(data);
            if (jsObLicense.code !== 1) {
                alert("Chưa set license cho plugin ký số!");
                return;
            }
            vnpt_plugin.signArrDataAdvanced(arrData, serial, true).then(function (data) {
                var jsOb = JSON.parse(data);
                if (JSON.parse(jsOb[0]).code === 0) {
                    $("#signValue").val(JSON.parse(jsOb[0]).data);
                    $('#base64Hash').val(hash);
                    $('#hdPattern').val($('#Pattern').val());
                    $('#Serial').val($('#Serial').val());
                    //$('#serialCertSign').val($('#serialCert').val());
                    $.blockUI({
                        message: '<h1><img src="/Content/Images/please_wait.gif" />Xin vui lòng đợi!</h1>', fadeIn: 0,
                        fadeOut: 10, timeout: 3000
                    });
                    $('input[type=submit]').click();
                } else showMessage(data);
            });

        }).catch(function (e) {
            alert("LicenseKey: " + e);
        });;
    }).catch(function (e) {
        $('#dialogCheckEx').bPopup({
            easing: 'easeOutBack', //uses jQuery easing plugin
            speed: 450,
            transition: 'slideDown'
        });
    });;
}
function signArrDataAdvancedIndex(hash) {
    var arrData = [];
    var dataJS = {};
    var arrayHash = new Array();
    arrayHash = hash.split(",");
    for (i = 0; i < arrayHash.length; i++) {
        dataJS.data = arrayHash[i];
        dataJS.type = "hash"
        var jsData = "";
        jsData += JSON.stringify(dataJS);
        arrData.push(jsData);
    }
    var serial = $('#serialCert').val();
    //var serial = "";
    vnpt_plugin.checkPlugin().then(function (data) {
        vnpt_plugin.setLicenseKey(keyPluginv2).then(function (data) {
            var jsObLicense = JSON.parse(data);
            if (jsObLicense.code !== 1) {
                alert("Chưa set license cho plugin ký số!");
                return;
            }
            vnpt_plugin.signArrDataAdvanced(arrData, serial, true).then(function (data) {
                if (data != "") {
                    var jsOb = JSON.parse(data);
                    if (JSON.parse(jsOb[0]).code === 0) {
                        var jsSigns = [];
                        for (i = 0; i < arrayHash.length; i++) {
                            var jsSign = {};
                            jsSign.hashValue = arrayHash[i];
                            jsSign.signedValue = JSON.parse(jsOb[i]).data;
                            jsSigns.push(jsSign);
                        }
                        $("#signValue").val(JSON.stringify(jsSigns));
                        $('#base64Hash').val(hash);
                        $('#hdPattern').val($('#Pattern').val());
                        $('#Serial').val($('#Serial').val());
                        $('#serialCertSign').val($('#serialCert').val());
                        $.blockUI({
                            message: '<h1><img src="/Content/Images/please_wait.gif" />Xin vui lòng đợi!</h1>', fadeIn: 0,
                            fadeOut: 10, timeout: 3000
                        });
                        $("#danhsachHD").attr("action", "/EInvoice/LaunchChoiceWithToken");
                        document.forms["danhsachHD"].submit();
                    } else showMessage(data);
                } else alert("Không lấy được chứng thư!");
            });

        }).catch(function (e) {
            alert("LicenseKey: " + e);
        });;
    }).catch(function (e) {
        $('#dialogCheckEx').bPopup({
            easing: 'easeOutBack', //uses jQuery easing plugin
            speed: 450,
            transition: 'slideDown'
        });
    });;
}