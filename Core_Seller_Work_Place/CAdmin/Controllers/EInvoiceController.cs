﻿/*
 * 10/1/2014: Create by hientt
 * 20/5/201 update by truongtx on...
 * 26/08/2014 update by quanglt on ...
 * 06/11/2015 update by hientt on ...
 * 28/11/2015 update by duyetnv on ... xóa action sualai
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using EInvoice.Core.IService;
using EInvoice.Core.Domain;
using FX.Core;
using FX.Utils;
using EInvoice.Core;
using FX.Utils.MVCMessage;
using FX.Utils.MvcPaging;
using IdentityManagement.Authorization;
using EInvoice.CAdmin.Models;
using EInvoice.Core.Launching;
using System.Text;
using System.IO;
using EInvoice.CAdmin.IService;
using System.Security.Cryptography.X509Certificates;
using System.Configuration;
using System.Threading;
using System.Web.Caching;
using System.Xml.Linq;
using System.Xml;
using FX.Data;
using IdentityManagement.WebProviders;
using EInvoice.Core.ServiceImp;
using EInvoice.CAdmin.ServiceImp;
using EInvoice.Core.Viewer;

namespace EInvoice.CAdmin.Controllers
{
    class KeyValuesEx
    {
        public string key { set; get; }
        public string value { set; get; }
    }


    [MessagesFilter]
    public class EInvoiceController : ShareController
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(EInvoiceController));

        /// <summary>
        /// Indexes the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="page">The page.</param>
        /// <param name="Pagesize">The pagesize.</param>
        /// <returns></returns>
        /// <Modified>
        /// Author          Dates          Comments
        /// TớiNT          7/10/2018       Create
        /// </Modified>
        [RBACAuthorize(Permissions = "Search_inv")]
        [HttpGet]
        public ActionResult Index(EInvoiceIndexModel model, string resourcecode, int? page, int? Pagesize)
        {
            Log.Info("Start Search Einvoice");
            var pubIn = IoC.Resolve<IPublishInvoiceService>();
            Log.Debug("EInvoiceController Index");
            Company currentCom = ((EInvoiceContext)FXContext.Current).CurrentCompany;

            try
            {
                Log.Info("Index" + currentCom.id + " pattern: " + model.Pattern + " serial: " + model.Serial);

                var membershipCompanyProvider = IoC.Resolve<IRBACMembershipCompanyProvider>();
                var objUser = membershipCompanyProvider.GetUserByNameComid(HttpContext.User.Identity.Name, currentCom.id.ToString(), true);
                if (objUser == null)
                {
                    Messages.AddErrorFlashMessage("User không tồn tại");
                    return RedirectToAction("Index", "Home");
                }

                if (WSHelper.Instance.CheckRoleSerial(currentCom, objUser))
                {
                    model.lstpattern = pubIn.GetLstPatternByUser(currentCom.id, objUser.userid, 1);
                    if (model.lstpattern.Count == 0)
                    {
                        Messages.AddErrorFlashMessage(Resources.Message.MInv_SMesNoRoleSerial);
                        return RedirectToAction("Index", "Home");
                    }
                    model.Pattern = string.IsNullOrEmpty(model.Pattern) ? model.lstpattern[0] : model.Pattern;
                    model.lstserial = pubIn.GetLstSerialByUser(currentCom.id, model.Pattern, 1, objUser.userid);
                }
                else
                {
                    model.lstpattern = pubIn.LstByPattern(currentCom.id, 1);
                    if (model.lstpattern.Count == 0)
                    {
                        Messages.AddErrorFlashMessage(Resources.Message.MInv_SMesNoPubAccess);
                        return RedirectToAction("Index", "Home");
                    }
                    model.Pattern = string.IsNullOrEmpty(model.Pattern) ? model.lstpattern[0] : model.Pattern;
                    model.lstserial = (from p in pubIn.Query where p.Status != 0 && p.RegisterTemplate.InvPattern == model.Pattern && p.RegisterTemplate.ComId == currentCom.id select p.InvSerial).Distinct().ToList();
                }

                int defautPagesize = Pagesize.HasValue ? Convert.ToInt32(Pagesize) : 10;
                int currentPageIndex = page.HasValue ? page.Value - 1 : 0;
                int totalRecords;

                IInvoiceService invSrv = InvServiceFactory.GetService(model.Pattern, currentCom.id);
                IList<IInvoice> lst;
                if (!model.InvNo.HasValue || model.InvNo == null)
                {
                    DateTime? dateFrom = null;
                    DateTime? dateTo = null;
                    if (!string.IsNullOrWhiteSpace(model.FromDate)) dateFrom = DateTime.ParseExact(model.FromDate, "dd/MM/yyyy", null);
                    if (!string.IsNullOrWhiteSpace(model.ToDate)) dateTo = DateTime.ParseExact(model.ToDate, "dd/MM/yyyy", null);
                    if (dateFrom != null && dateTo != null && dateFrom > dateTo)
                    {
                        Messages.AddErrorMessage("Nhập đúng dữ liệu tìm kiếm theo ngày!");
                        dateFrom = dateTo = null;
                    }
                    bool isResourceCode = WSHelper.Instance.CheckResourceCode(currentCom);
                    if (WSHelper.Instance.CheckRoleSerial(currentCom, objUser))
                        lst = invSrv.SearchByCustomer(currentCom.id, model.Pattern, model.Serial, resourcecode, dateFrom, dateTo, (InvoiceStatus)model.Status, model.nameCus, model.code, model.CodeTax, (InvoiceType)model.typeInvoice, currentPageIndex, defautPagesize, out totalRecords, objUser, isResourceCode);
                    else
                        lst = invSrv.SearchByCustomer(currentCom.id, model.Pattern, model.Serial, resourcecode, dateFrom, dateTo, (InvoiceStatus)model.Status, model.nameCus, model.code, model.CodeTax, (InvoiceType)model.typeInvoice, currentPageIndex, defautPagesize, out totalRecords, false, isResourceCode);
                }
                else
                {
                    lst = new List<IInvoice>();
                    IInvoice inv = invSrv.GetByNo(currentCom.id, model.Pattern, model.Serial, model.InvNo.Value);
                    if (inv != null)
                        lst.Add(inv);
                    totalRecords = lst.Count;
                }

                IKeyStoresService keySrv = IoC.Resolve<IKeyStoresService>();
                KeyStores keyStores = keySrv.Query.Where(ks => ks.ComID == currentCom.id).FirstOrDefault();
                string serialCert = "";
                string cert = "";
                decimal typeCert = 0;
                if (keyStores != null)
                {
                    typeCert = keyStores.Type;
                    serialCert = keyStores.SerialCert;
                    ICertificateService iCerSrv = IoC.Resolve<ICertificateService>();
                    Certificate objCeroder = iCerSrv.Query.Where(ce => ce.ComID == currentCom.id && ce.SerialCert.ToUpper().Equals(serialCert)).FirstOrDefault();
                    if (objCeroder != null)
                    {
                        cert = objCeroder.Cert;
                    }
                }
                model.typeCert = typeCert;
                model.serialCert = serialCert;
                model.base64Cert = cert;
                ViewData["resourcecode"] = resourcecode;
                model.PageListINV = new PagedList<IInvoice>(lst, currentPageIndex, defautPagesize, totalRecords);
                AttachmentService _attachmentServiceSrv = IoC.Resolve<AttachmentService>();
                model.PageListAttach = _attachmentServiceSrv.SearhByLstInv(lst);
                Log.Info("End Search Einvoice");
                return View(model);

            }
            catch (UnAuthorizedException e)
            {
                Log.Error("Index -" + e.Message);
                Messages.AddErrorMessage("Có lỗi xảy ra, vui lòng thực hiện lại!");
                return RedirectToAction("Index", "Home");
            }
            catch (ArgumentException ex)
            {
                Log.Error("Index -" + ex.Message);
                return Redirect("/Home/PotentiallyError");
            }
            catch (HttpRequestValidationException ex)
            {
                Log.Error("Index -" + ex.Message);
                return RedirectToAction("PotentiallyError", "Home");
            }
        }

        //Tự chọn id của từng hóa đơn để phát hành
        [RBACAuthorize(Permissions = "Release_invInList")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LaunchChoice(string[] cbid, string hdPattern, string Serial)
        {
            Log.Info("LaunchChoice by: " + HttpContext.User.Identity.Name);
            Company currentCom = ((EInvoiceContext)FXContext.Current).CurrentCompany;
            try
            {
                IInvoiceService IInvSrv = InvServiceFactory.GetService(hdPattern, currentCom.id);
                int[] ids = (from s in cbid select Convert.ToInt32(s)).ToArray();
                if (ids.Length < 0)
                {
                    Messages.AddErrorFlashMessage("Bạn chưa chọn hóa đơn.");
                    return RedirectToAction("Index", new { Pattern = hdPattern, Serial = Serial });
                }
                IList<IInvoice> lst = IInvSrv.GetByID(currentCom.id, ids);
                if (lst.Count <= 0)
                {
                    Messages.AddErrorFlashMessage("Bạn chưa chọn hóa đơn.");
                    return RedirectToAction("Index", new { Pattern = hdPattern, Serial = Serial });
                }
                var result = from item in lst
                             from other in lst
                             where item.Pattern != other.Pattern ||
                                   item.Serial != other.Serial
                             select item;
                if (result != null && result.Count() > 0)
                {
                    Messages.AddErrorFlashMessage("Các hóa đơn cần phát hành phải cùng một mẫu số và ký hiệu!");
                    return RedirectToAction("Index", new { Pattern = hdPattern, Serial = Serial });
                }
                ILauncherService _launcher = IoC.Resolve(Type.GetType(currentCom.Config["LauncherType"])) as ILauncherService;

                var resultNo = from item in lst
                               where item.No != 0 &&
                                     item.Status != InvoiceStatus.NewInv
                               select item;

                if (resultNo.Any())
                {
                    Messages.AddErrorFlashMessage("Tồn tại hóa đơn đã phát hành!");
                    return RedirectToAction("Index", new { Pattern = hdPattern, Serial });
                }

                _launcher.PublishInv(hdPattern, lst.FirstOrDefault().Serial, lst.ToArray(), HttpContext.User.Identity.Name);

                string strResult = _launcher.Message;
                if (strResult.Contains("OK"))
                {
                    Log.Info("Publish EInvoice by: " + HttpContext.User.Identity.Name + " Info-- Pattern: " + hdPattern + "; Serial: " + Serial + "; SoLuongPhatHanh: " + ids.Length.ToString());
                    Messages.AddFlashMessage(Resources.Message.PInv_IMesSuccess + ids.Length + Resources.Message.Minvoice);
                }
                else if (strResult.Contains("ERR:14"))
                {
                    Log.Error("ERR:14 " + strResult);
                    Messages.AddErrorFlashMessage("Có lô hóa đơn khác đang được phát hành, xin vui lòng thực hiện lại.");
                }
                else if (strResult.Contains("ERR:26"))
                {
                    Log.Error("ERR:26 " + strResult);
                    Messages.AddErrorFlashMessage("Thời gian sử dụng chứng thư chưa phù hợp.");
                }
                else
                {
                    Log.Error("ERR: " + strResult);
                    Messages.AddErrorFlashMessage(strResult);
                }

                return RedirectToAction("Index", new { Pattern = hdPattern, Serial = Serial });
            }
            catch (Exception ex)
            {
                //log.Error("Exception: " + ex);
                Messages.AddErrorFlashMessage("Có lỗi xảy ra, vui lòng thực hiện lại!");
                Log.Error("LaunchChoice-" + ex);
                return RedirectToAction("Index", new { Pattern = hdPattern, Serial = Serial });
            }
        }

        //hiển thị form nhập thông tin để phát hành hóa đơn theo lô        
        /// <summary>
        /// Launches this instance.
        /// </summary>
        /// <returns></returns>
        /// <Modified>
        /// Author          Dates          Comments
        /// TớiNT          7/11/2018       Create
        /// </Modified>
        [RBACAuthorize(Permissions = "Release_invInTime")]
        public ActionResult Launch()
        {
            IPublishInvoiceService pubIn = IoC.Resolve<IPublishInvoiceService>();
            Company currentCom = ((EInvoiceContext)FXContext.Current).CurrentCompany;
            LaunchModel model = new LaunchModel();
            try
            {
                var membershipCompanyProvider = IoC.Resolve<IRBACMembershipCompanyProvider>();
                var userPublishInvoice = IoC.Resolve<IUserPublishInvoiceService>();
                var objUser = membershipCompanyProvider.GetUserByNameComid(HttpContext.User.Identity.Name, currentCom.id.ToString(), true);
                if (objUser == null)
                {
                    Messages.AddErrorFlashMessage("User không tồn tại");
                    return RedirectToAction("Index", "Home");
                }

                List<string> lstpattern;
                List<string> oserial;
                if (WSHelper.Instance.CheckRoleSerial(currentCom, objUser))
                {
                    lstpattern = pubIn.GetLstPatternByUser(currentCom.id, objUser.userid, 1);
                    oserial = (from s in pubIn.Query
                               join userp in userPublishInvoice.Query on s.id equals userp.PublishInvoiceId
                               where s.RegisterTemplate.ComId == currentCom.id &&
                                     (s.Status == 1 || s.Status == 2) &&
                                     userp.UserId == objUser.userid
                               select s.InvSerial).ToList();
                }
                else
                {
                    lstpattern = pubIn.LstByPattern(currentCom.id, 1);
                    oserial = (from s in pubIn.Query where s.RegisterTemplate.ComId == currentCom.id && (s.Status == 1 || s.Status == 2) select s.InvSerial).ToList();
                }

                model.Listpattern = new SelectList(lstpattern);
                model.Listserial = new SelectList(oserial);
            }
            catch (Exception ex)
            {
                Messages.AddErrorFlashMessage(Resources.Message.Key_MesReqConfig);
                Log.Error(" Launch -" + ex.Message);
                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }

        //phát hành hóa đơn theo lô
        [RBACAuthorize(Permissions = "Release_invInTime")]
        public bool LaunchCollectInvoice(string FromDate, string ToDate, string pattern, string serial)
        {
            DateTime? DateFrom = null;
            DateTime? DateTo = null;
            if (!string.IsNullOrWhiteSpace(FromDate)) DateFrom = DateTime.ParseExact(FromDate, "dd/MM/yyyy", null);
            if (!string.IsNullOrWhiteSpace(ToDate)) DateTo = DateTime.ParseExact(ToDate, "dd/MM/yyyy", null);
            try
            {
                Company currentCom = ((EInvoiceContext)FXContext.Current).CurrentCompany;
                IInvoiceService IInvSrv = InvServiceFactory.GetService(pattern, currentCom.id);
                // chỉ tìm NewInv
                int totalRecords = 0;
                IList<IInvoice> listInvoice = IInvSrv.SearchInvoice(currentCom.id, pattern, serial, DateFrom, DateTo, 0, 0, out totalRecords, InvoiceStatus.NewInv);
                if (listInvoice.Count > 0)
                {
                    //edit phat hanh hoa don 03/07/2014
                    int[] ids = listInvoice.Select(c => c.id).ToArray();

                    //Poolservice.Pooling pool_Srv = new Poolservice.Pooling();
                    //pool_Srv.publishInv(ids, pattern, serial, HttpContext.User.Identity.Name);
                    ILauncherService _launcher = IoC.Resolve(Type.GetType(currentCom.Config["LauncherType"])) as ILauncherService;

                    _launcher.PublishInv(pattern, serial, listInvoice.ToArray(), HttpContext.User.Identity.Name);
                    //Launcher.Instance.Launch(pattern, serial, listInvoice.ToArray());

                    Log.Info("Publish EInvoice by: " + HttpContext.User.Identity.Name + " Info-- Pattern: " + pattern + " Serial: " + serial + " SoLuongPhatHanh: " + listInvoice.Count.ToString());
                    string strResult = _launcher.Message;
                    if (strResult.Contains("OK"))
                        Messages.AddFlashMessage(strResult);
                    else if (strResult.Contains("ERR:14"))
                        Messages.AddErrorFlashMessage("Có lô hóa đơn khác đang được phát hành, xin vui lòng thực hiện lại.");
                    else
                        Messages.AddErrorFlashMessage(strResult);
                    Log.Info("Launch Result: " + strResult);
                }
                else
                {
                    Messages.AddErrorFlashMessage(Resources.Message.PInv_MInvNonExitsInTime);
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Messages.AddErrorFlashMessage(Resources.Message.MInv_MesError);
                Log.Error(" LaunchCollectInvoice -:" + ex);
                return false;
            }
        }

        //Tạo mới hóa đơn        
        /// <summary>
        /// Creates the specified pattern.
        /// </summary>
        /// <param name="Pattern">The pattern.</param>
        /// <returns></returns>
        /// <Modified>
        /// Author          Dates          Comments
        /// TớiNT          7/11/2018       Create
        /// </Modified>
        [RBACAuthorize(Permissions = "Create_inv")]
        public ActionResult Create(string Pattern)
        {
            if (string.IsNullOrEmpty(Pattern))
            {
                Messages.AddErrorFlashMessage(Resources.Message.MInv_SMesNoRoleSerial);
                return RedirectToAction("Index", "Home");
            }

            ICompanyService comSrv = IoC.Resolve<ICompanyService>();
            Company currentCom = comSrv.Getbykey(((EInvoiceContext)FXContext.Current).CurrentCompany.id);
            IPublishInvoiceService pubIn = IoC.Resolve<IPublishInvoiceService>();
            IRegisterTempService reTemSvc = IoC.Resolve<IRegisterTempService>();

            var membershipCompanyProvider = IoC.Resolve<IRBACMembershipCompanyProvider>();
            var objUser = membershipCompanyProvider.GetUserByNameComid(HttpContext.User.Identity.Name, currentCom.id.ToString(), true);
            if (objUser == null)
            {
                Messages.AddErrorFlashMessage("User không tồn tại");
                return RedirectToAction("Index", "Home");
            }

            string viewName = InvServiceFactory.GetView(Pattern, currentCom.id) + "Create";
            IInvoice model = InvServiceFactory.NewInstance(Pattern, currentCom.id);
            model.Pattern = Pattern;
            model.ComID = currentCom.id;
            model.Name = reTemSvc.SeachNameInv(Pattern, currentCom.id);
            List<string> ser;
            if (WSHelper.Instance.CheckRoleSerial(currentCom, objUser))
                ser = pubIn.GetLstSerialByUser(currentCom.id, Pattern, 0, objUser.userid, true);
            else
                ser = pubIn.GetSerialByPatter(Pattern, currentCom.id);
            ViewData["ser"] = ser;
            ViewData["company"] = currentCom;
            return View(viewName, model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [RBACAuthorize(Permissions = "Create_inv")]
        public ActionResult Create(string Pattern, string PubDatasource, string DicExsource)
        {
            ICompanyService comSrv = IoC.Resolve<ICompanyService>();
            Company currentCom = comSrv.Getbykey(((EInvoiceContext)FXContext.Current).CurrentCompany.id);
            IPublishInvoiceService _PubIn = IoC.Resolve<IPublishInvoiceService>();
            IRegisterTempService _ReTemSvc = IoC.Resolve<IRegisterTempService>();
            Dictionary<string, object> DicEx = new Dictionary<string, object>();
            string ErrorMessage = "";
            IInvoice model = InvServiceFactory.NewInstance(Pattern, currentCom.id);
            IInvoiceService IInvSrv = InvServiceFactory.GetService(Pattern, currentCom.id);
            try
            {
                TryUpdateModelFromType(model.GetType(), model);
                var tem = InvServiceFactory.GetInvoiceType(Pattern, currentCom.id);
                if (!String.IsNullOrEmpty(PubDatasource))
                {
                    //Kiển tra loại khách hàng trong hóa đơn
                    ICustomerService _CusSvc = FX.Core.IoC.Resolve<ICustomerService>();
                    var Typecus = (from c in _CusSvc.Query where c.Code == model.CusCode && c.CusType == 1 && c.ComID == currentCom.id select c.CusType).FirstOrDefault();
                    if (Typecus == 0)
                    {
                        model.CusSignStatus = cusSignStatus.NocusSignStatus;
                    }
                    else
                    {
                        model.CusSignStatus = cusSignStatus.NoSignStatus;
                    }

                    IList<ProductInv> lstproduct = (IList<ProductInv>)PubDatasource.DeserializeJSON<ProductInv>(typeof(IList<ProductInv>));
                    List<KeyValuesEx> lstKeyValuesEx = (List<KeyValuesEx>)DicExsource.DeserializeJSON<KeyValuesEx>(typeof(List<KeyValuesEx>));
                    if (lstKeyValuesEx != null && lstKeyValuesEx.Count > 0)
                    {
                        //IConfigService _Config = FX.Core.IoC.Resolve<IConfigService>();
                        Dictionary<string, string> DicConfig = currentCom.Config.Where(a => a.Key.Contains("Extra_")).ToDictionary(obj => obj.Key, obj => ValueDic(obj.Value));
                        Log.Info("currentCom.id: " + currentCom.id);
                        Log.Info("DicConfig: " + DicConfig.Count());
                        foreach (var item in lstKeyValuesEx)
                        {
                            if (DicConfig.ContainsKey(item.key))
                            {

                                string caseSwitch = DicConfig[item.key];
                                switch (caseSwitch)
                                {
                                    case "string":
                                        DicEx.Add(item.key, item.value);
                                        break;
                                    case "bool":
                                        DicEx.Add(item.key, bool.Parse(item.value));
                                        break;
                                    case "decimal":
                                        DicEx.Add(item.key, decimal.Parse(string.IsNullOrEmpty(item.value) ? "-1" : item.value));
                                        break;
                                    default:

                                        break;
                                }

                            }

                        }
                    }

                    // Trường hợp cấp phát số và phát hành tách riêng
                    // ToiNT
                    string typeLancherStr = "";
                    if (currentCom.Config.Keys.Contains("TypeLauncher"))
                        typeLancherStr = currentCom.Config["TypeLauncher"];

                    model.Products = (from pr in lstproduct select pr as IProductInv).ToList();

                    IInvoice[] invoicemodels = { model };
                    if (!string.IsNullOrEmpty(typeLancherStr) && typeLancherStr.Equals("EInvoice.Core.Launching.LBLauncherSigned"))
                    {
                        int ErrorColumn = 0;
                        typeLancherStr = "EInvoice.Core.Launching.LBLauncherAllocated, EInvoice.Core";
                        ILauncher lancherService = IoC.Resolve(Type.GetType(typeLancherStr)) as ILauncher;
                        lancherService.Launch(model.Pattern, model.Serial, invoicemodels, out ErrorColumn, out ErrorMessage, currentCom, null, null, false);
                        if (string.IsNullOrEmpty(ErrorMessage))
                        {
                            Log.Info("Create EInvoice by: " + HttpContext.User.Identity.Name + " Info-- TenKhachHang: " + model.CusName + " MaKhachHang: " + model.CusCode + " SoTien: " + model.Amount);
                            Messages.AddFlashMessage(Resources.Message.MInv_IMesSuccess);
                            return RedirectToAction("Index", new { Pattern, model.Serial });
                        }
                    }
                    else
                    {
                        if (IInvSrv.CreateInvoice(lstproduct, model, DicEx, out ErrorMessage))
                        {
                            Log.Info("Create EInvoice by: " + HttpContext.User.Identity.Name + " Info-- TenKhachHang: " + model.CusName + " MaKhachHang: " + model.CusCode + " SoTien: " + model.Amount);
                            Messages.AddFlashMessage(Resources.Message.MInv_IMesSuccess);
                            return RedirectToAction("Index", new { Pattern, model.Serial });
                        }
                    }

                    Messages.AddErrorMessage(ErrorMessage);
                }
                else
                    Messages.AddErrorMessage(Resources.Message.MInv_IMesLstProd);
                List<string> ser = _PubIn.GetSerialByPatter(Pattern, currentCom.id);
                ViewData["ser"] = ser;
                ViewData["company"] = currentCom;
                string ViewName = InvServiceFactory.GetView(Pattern, currentCom.id) + "Create";
                return View(ViewName, model);
            }
            catch (HttpRequestValidationException ex)
            {
                Log.Error("ArgumentException: " + ex);
                Messages.AddErrorMessage("Dữ liệu không hợp lệ hoặc có chứa mã gây nguy hiểm tiềm tàng cho hệ thống!");
                model = InvServiceFactory.NewInstance(Pattern, currentCom.id);
                List<string> ser = _PubIn.GetSerialByPatter(Pattern, currentCom.id);
                ViewData["ser"] = ser;
                ViewData["company"] = currentCom;
                string ViewName = InvServiceFactory.GetView(Pattern, currentCom.id) + "Create";
                return View(ViewName, model);
            }
            catch (Exception ex)
            {
                Log.Error(" Create - " + ex);
                Messages.AddErrorMessage("Có lỗi xảy ra, vui lòng thực hiện lại!");
                List<string> ser = _PubIn.GetSerialByPatter(Pattern, currentCom.id);
                ViewData["ser"] = ser;
                ViewData["company"] = currentCom;
                if (!String.IsNullOrEmpty(PubDatasource))
                {
                    IList<ProductInv> lstproduct = (IList<ProductInv>)PubDatasource.DeserializeJSON<ProductInv>(typeof(IList<ProductInv>));
                    model.Products = (from pr in lstproduct select pr as IProductInv).ToList();
                }
                string ViewName = InvServiceFactory.GetView(Pattern, currentCom.id) + "Create";
                return View(ViewName, model);
            }
        }

        string ValueDic(string json)
        {
            try
            {
                if (!string.IsNullOrEmpty(json))
                {
                    ExtraModel obj = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<ExtraModel>(json);
                    return obj.DataType;
                }
                return "";
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace);
                return "";
            }
        }

        [HttpPost]
        [RBACAuthorize(Permissions = "Create_inv")]
        public ActionResult CreateCoupon(string pattern)
        {
            ICompanyService comSrv = IoC.Resolve<ICompanyService>();
            Company currentCom = comSrv.Getbykey(((EInvoiceContext)FXContext.Current).CurrentCompany.id);
            IPublishInvoiceService _PubIn = IoC.Resolve<IPublishInvoiceService>();
            IRegisterTempService _ReTemSvc = IoC.Resolve<IRegisterTempService>();
            IInvoice model = InvServiceFactory.NewInstance(pattern, currentCom.id);
            IInvoiceService IInvSrv = InvServiceFactory.GetService(pattern, currentCom.id);
            try
            {
                TryUpdateModelFromType(model.GetType(), model);
                IInvSrv.CreateNew(model);
                IInvSrv.CommitChanges();
                Messages.AddFlashMessage("Tạo phiếu thu thành công.");
                return RedirectToAction("Index", new { Pattern = pattern });
            }
            catch (Exception ex)
            {
                Log.Error(" Create - " + ex);
                Messages.AddErrorMessage("Có lỗi xảy ra, vui lòng thực hiện lại!");
                List<string> ser = _PubIn.GetSerialByPatter(pattern, currentCom.id);
                ViewData["ser"] = ser;
                ViewData["company"] = currentCom;
                string ViewName = InvServiceFactory.GetView(pattern, currentCom.id) + "Create";
                return View(ViewName, model);
            }
        }
        //chỉnh sửa hóa đơn chưa phát hành
        [RBACAuthorize(Permissions = "Edit_inv")]
        public ActionResult Edit(string Pattern, int id)
        {
            ICompanyService comSrv = IoC.Resolve<ICompanyService>();
            Company currentCom = comSrv.Getbykey(((EInvoiceContext)FXContext.Current).CurrentCompany.id);
            IPublishInvoiceService _PubIn = IoC.Resolve<IPublishInvoiceService>();
            IRegisterTempService _ReTemSvc = IoC.Resolve<IRegisterTempService>();
            string ViewName = InvServiceFactory.GetView(Pattern, currentCom.id) + "Edit";
            //khoi tao service
            IInvoiceService IInvSrv = InvServiceFactory.GetService(Pattern, currentCom.id);
            //lay ve mot ban ghi hoa don 
            var lstPatternInStaff = new List<string>();
            var lstSerialInStaff = new List<string>();
            IInvoice model = IInvSrv.Getbykey<IInvoice>(id);
            //string[] arr = new[] { "Kế Toán Trưởng", "Admin" };
            //if (!((IFanxiPrincipal)HttpContext.User).IsInRole(arr))
            //{
            //    if (!model.CreateBy.Equals(HttpContext.User.Identity.Name))
            //    {
            //        Messages.AddErrorFlashMessage("Tài khoản chỉ được quyền sửa hóa đơn mình tạo");
            //        return RedirectToAction("Index", new { Pattern = Pattern, Serial = model.Serial });
            //    }
            //    string msg = GetPatternSerialInStaff(currentCom.id, out lstPatternInStaff, out lstSerialInStaff);
            //    if (!string.IsNullOrEmpty(msg))
            //    {
            //        Messages.AddErrorMessage(msg);
            //        return RedirectToAction("Index", "Home");
            //    }
            //    if (!lstPatternInStaff.Any(t => t.Equals(Pattern)))
            //    {
            //        Messages.AddErrorMessage(msg);
            //        return RedirectToAction("Index", "Home");
            //    }
            //}
            if (model.Status != InvoiceStatus.NewInv)
            {
                Messages.AddErrorFlashMessage(Resources.Message.MInv_UMesCantEdit);
                return RedirectToAction("Index", new { Pattern = Pattern, Serial = model.Serial });
            }
            //lay va doi danh sach cac san pham thanh doi tuong json
            //nếu Unit=null thì mặc định hiển thị ""
            foreach (var item in model.Products)
            {
                if (item.Unit == null)
                {
                    item.Unit = "";
                }
            }
            //lay ra danh sach cac serial
            List<string> ser = _PubIn.GetSerialByPatter(model.Pattern, currentCom.id);
            ViewData["ser"] = ser;
            //lay thong tin ve don vi ban hang
            ViewData["company"] = currentCom;
            model.Note = "";
            return View(ViewName, model);
        }

        [HttpPost]
        [RBACAuthorize(Permissions = "Edit_inv")]
        public ActionResult Edit(string Pattern, int id, string PubDatasource, string DicExsource)
        {
            if (id <= 0)
                throw new HttpRequestValidationException();
            ICompanyService comSrv = IoC.Resolve<ICompanyService>();
            Company currentCom = comSrv.Getbykey(((EInvoiceContext)FXContext.Current).CurrentCompany.id);
            IPublishInvoiceService _PubIn = IoC.Resolve<IPublishInvoiceService>();
            IRegisterTempService _ReTemSvc = IoC.Resolve<IRegisterTempService>();
            IInvoiceService IInvSrv = InvServiceFactory.GetService(Pattern, currentCom.id);
            IInvoice model = IInvSrv.Getbykey<IInvoice>(id);
            //string NoteOrder = model.Note;
            try
            {
                string ErrorMessage = "";
                TryUpdateModelFromType(model.GetType(), model);
                if(!string.IsNullOrEmpty(model.CusName))
                    model.CusName = model.CusName.Trim();
                //model.Note = NoteOrder + " || " + model.Note;
                if (!String.IsNullOrEmpty(PubDatasource) && PubDatasource != "[]")
                {
                    ICustomerService _CusSvc = IoC.Resolve<ICustomerService>();
                    var Typecus = (from c in _CusSvc.Query where c.Code == model.CusCode && c.CusType == 1 && c.ComID == currentCom.id select c.CusType).FirstOrDefault();
                    if (Typecus == 0)
                    {
                        model.CusSignStatus = cusSignStatus.NocusSignStatus;
                    }
                    else
                    {
                        model.CusSignStatus = cusSignStatus.NoSignStatus;
                    }
                    IList<ProductInv> lstproduct = (IList<ProductInv>)PubDatasource.DeserializeJSON<ProductInv>(typeof(IList<ProductInv>));
                    Dictionary<string, object> DicEx = new Dictionary<string, object>();
                    List<KeyValuesEx> lstKeyValuesEx = (List<KeyValuesEx>)DicExsource.DeserializeJSON<KeyValuesEx>(typeof(List<KeyValuesEx>));
                    if (lstKeyValuesEx != null && lstKeyValuesEx.Count > 0)
                    {
                        //IConfigService _Config = FX.Core.IoC.Resolve<IConfigService>();
                        Dictionary<string, string> DicConfig = currentCom.Config.Where(a => a.Key.Contains("Extra_")).ToDictionary(obj => obj.Key, obj => ValueDic(obj.Value));
                        foreach (var item in lstKeyValuesEx)
                        {
                            if (DicConfig.ContainsKey(item.key))
                            {

                                string caseSwitch = DicConfig[item.key];
                                switch (caseSwitch)
                                {
                                    case "string":
                                        DicEx.Add(item.key, item.value);
                                        break;
                                    case "bool":
                                        DicEx.Add(item.key, bool.Parse(item.value));
                                        break;
                                    case "decimal":
                                        DicEx.Add(item.key, decimal.Parse(string.IsNullOrEmpty(item.value) ? "-1" : item.value));
                                        break;
                                    default:

                                        break;
                                }

                            }

                        }
                    }
                    if (IInvSrv.UpdateInvoice(lstproduct, model, DicEx, out ErrorMessage) == true)
                    {
                        Log.Info("Edit EInvoice by: " + HttpContext.User.Identity.Name + " Info-- TenKhachHang: " + model.CusName + " MaKhachHang: " + model.CusCode + " SoTien: " + model.Amount);
                        Messages.AddFlashMessage(Resources.Message.MInv_UMesSuccess);
                        return RedirectToAction("Index", new { Pattern, model.Serial });
                    }
                    else Messages.AddErrorMessage(ErrorMessage);
                    model.Products = (from pr in lstproduct select pr as IProductInv).ToList();
                }
                else Messages.AddErrorMessage(Resources.Message.MInv_UMesRequestProd);
                model.Name = _ReTemSvc.SeachNameInv(Pattern, currentCom.id);
                List<string> ser = _PubIn.GetSerialByPatter(model.Pattern, currentCom.id);
                ViewData["ser"] = ser;
                ViewData["company"] = currentCom;
                string ViewName = InvServiceFactory.GetView(Pattern, currentCom.id) + "Edit";
                return View(ViewName, model);
            }
            catch (HttpRequestValidationException ex)
            {
                Log.Error("ArgumentException: " + ex);
                Messages.AddErrorMessage("Dữ liệu không hợp lệ hoặc có chứa mã gây nguy hiểm tiềm tàng cho hệ thống!");
                model = IInvSrv.Getbykey<IInvoice>(id);
                List<string> ser = _PubIn.GetSerialByPatter(Pattern, currentCom.id);
                ViewData["ser"] = ser;
                ViewData["company"] = currentCom;
                string ViewName = InvServiceFactory.GetView(Pattern, currentCom.id) + "Edit";
                return View(ViewName, model);
            }
            catch (Exception ex)
            {
                Log.Error("Edit -", ex);
                model.Name = _ReTemSvc.SeachNameInv(Pattern, currentCom.id);
                List<string> ser = _PubIn.GetSerialByPatter(model.Pattern, currentCom.id);
                ViewData["ser"] = ser;
                ViewData["company"] = currentCom;
                Messages.AddErrorMessage(Resources.Message.MInv_UMesRequestProd);
                if (!String.IsNullOrEmpty(PubDatasource))
                {
                    IList<ProductInv> lstproduct = (IList<ProductInv>)PubDatasource.DeserializeJSON<ProductInv>(typeof(IList<ProductInv>));
                    model.Products = (from pr in lstproduct select pr as IProductInv).ToList();
                }
                string ViewName = InvServiceFactory.GetView(Pattern, currentCom.id) + "Edit";
                return View(ViewName, model);
            }
        }

        [HttpPost]
        [RBACAuthorize(Permissions = "Edit_inv")]
        public ActionResult UpdateCoupon(string Pattern, int id)
        {
            if (id <= 0)
                throw new HttpRequestValidationException();
            ICompanyService comSrv = IoC.Resolve<ICompanyService>();
            Company currentCom = comSrv.Getbykey(((EInvoiceContext)FXContext.Current).CurrentCompany.id);
            IPublishInvoiceService _PubIn = IoC.Resolve<IPublishInvoiceService>();
            IRegisterTempService _ReTemSvc = IoC.Resolve<IRegisterTempService>();
            IInvoiceService IInvSrv = InvServiceFactory.GetService(Pattern, currentCom.id);
            IInvoice model = IInvSrv.Getbykey<IInvoice>(id);
            string NoteOrder = model.Note;
            try
            {
                TryUpdateModelFromType(model.GetType(), model);
                model.Note = NoteOrder + " || " + model.Note;
                IInvSrv.Update(model);
                IInvSrv.CommitChanges();
                Log.Info("Edit EInvoice by: " + HttpContext.User.Identity.Name + " Info-- TenKhachHang: " + model.CusName + " MaKhachHang: " + model.CusCode + " SoTien: " + model.Amount);
                Messages.AddFlashMessage(Resources.Message.MInv_UMesSuccess);
                return RedirectToAction("Index", new { Pattern = Pattern, Serial = model.Serial });
            }
            catch (HttpRequestValidationException ex)
            {
                Log.Error("ArgumentException: " + ex);
                Messages.AddErrorMessage("Dữ liệu không hợp lệ hoặc có chứa mã gây nguy hiểm tiềm tàng cho hệ thống!");
                model = IInvSrv.Getbykey<IInvoice>(id);
                List<string> ser = _PubIn.GetSerialByPatter(Pattern, currentCom.id);
                ViewData["ser"] = ser;
                ViewData["company"] = currentCom;
                string ViewName = InvServiceFactory.GetView(Pattern, currentCom.id) + "Edit";
                return View(ViewName, model);
            }
            catch (Exception ex)
            {
                Log.Error("Edit -", ex);
                model.Name = _ReTemSvc.SeachNameInv(Pattern, currentCom.id);
                List<string> ser = _PubIn.GetSerialByPatter(model.Pattern, currentCom.id);
                ViewData["ser"] = ser;
                ViewData["company"] = currentCom;
                Messages.AddErrorMessage(Resources.Message.MInv_UMesRequestProd);
                string ViewName = InvServiceFactory.GetView(Pattern, currentCom.id) + "Edit";
                return View(ViewName, model);
            }
        }
        //xóa hóa đơn chưa phát hành
        [RBACAuthorize(Permissions = "Del_inv")]
        public ActionResult Delete(string Pattern, int id)
        {
            if (id <= 0)
                throw new HttpRequestValidationException();
            Log.Debug("Access - Delete");
            Company currentCom = ((EInvoiceContext)FXContext.Current).CurrentCompany;
            IInvoiceService IInvSrv = InvServiceFactory.GetService(Pattern, currentCom.id);
            IInvoice model = IInvSrv.Getbykey<IInvoice>(id);
            IInvSrv.isStateFull = false;
            try
            {
                string ErrorMessage = "";
                if (IInvSrv.DeleteInvoice(model, out ErrorMessage) == true)
                {
                    Log.Info("Delete EInvoice by: " + HttpContext.User.Identity.Name + " Info-- ID: " + model.id + "Pattern: " + model.Pattern + " Serial: " + model.Serial + " SoHoaDon: " + model.No.ToString() + " TenKhachHang: " + model.CusName + " MaKhachHang: " + model.CusCode + " SoTien: " + model.Amount);
                    Messages.AddFlashMessage(Resources.Message.MInv_DMesSuccess);
                    Log.Info("Delete successfull id =" + id);
                }
                else
                {
                    Messages.AddErrorFlashMessage(ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(" Delete-" + ex.Message);
            }
            return RedirectToAction("Index", new { Pattern = Pattern, Serial = model.Serial });
        }

        /// <summary>
        /// Gets the serial by patter.
        /// </summary>
        /// <param name="opattern">The opattern.</param>
        /// <returns></returns>
        /// <Modified>
        /// Author          Dates          Comments
        /// TớiNT          7/11/2018       Create
        /// </Modified>
        public ActionResult GetSerialByPatter(string opattern)
        {
            Company currentCom = ((EInvoiceContext)FXContext.Current).CurrentCompany;
            IPublishInvoiceService pubIn = IoC.Resolve<IPublishInvoiceService>();
            var membershipCompanyProvider = IoC.Resolve<IRBACMembershipCompanyProvider>();
            var userPublishInvoice = IoC.Resolve<IUserPublishInvoiceService>();
            var objUser = membershipCompanyProvider.GetUserByNameComid(HttpContext.User.Identity.Name, currentCom.id.ToString(), true);

            List<string> pu;
            if (WSHelper.Instance.CheckRoleSerial(currentCom, objUser))
                pu = (from s in pubIn.Query
                      join userp in userPublishInvoice.Query on s.id equals userp.PublishInvoiceId
                      where s.RegisterTemplate.ComId == currentCom.id &&
                            (s.Status == 1 || s.Status == 2) &&
                            s.RegisterTemplate.InvPattern == opattern &&
                            userp.UserId == objUser.userid
                      select s.InvSerial).Distinct().ToList();
            else
                pu = (from c in pubIn.Query where c.RegisterTemplate.InvPattern == opattern && c.RegisterTemplate.ComId == currentCom.id && (c.Status == 1 || c.Status == 2) select c.InvSerial).Distinct().ToList();

            //var pu = (from c in pubIn.Query where c.RegisterTemplate.InvPattern == opattern && c.RegisterTemplate.ComId == currentCom.id && (c.Status == 1 || c.Status == 2) select c.InvSerial).Distinct().ToList();
            return Json(new
            {
                pu
            });
        }

        //lấy danh sách các serial từ pattern
        //status!=0
        public ActionResult GetSerial(string Pattern)
        {
            IPublishInvoiceService pubIn = IoC.Resolve<IPublishInvoiceService>();
            Company currentCom = ((EInvoiceContext)FXContext.Current).CurrentCompany;
            var membershipCompanyProvider = IoC.Resolve<IRBACMembershipCompanyProvider>();
            var objUser = membershipCompanyProvider.GetUserByNameComid(HttpContext.User.Identity.Name, currentCom.id.ToString(), true);
            List<string> pu;
            if (WSHelper.Instance.CheckRoleSerial(currentCom, objUser))
                pu = pubIn.GetLstSerialByUser(currentCom.id, Pattern, 1, objUser.userid);
            else
                pu = pubIn.LstBySerial(currentCom.id, Pattern);

            //var pu = pubIn.LstBySerial(currentCom.id, Pattern);
            return Json(new
            {
                pu
            });
        }

        //lấy danh sách thông tin khách hàng từ id khách hàng
        public ActionResult ajx(int id)
        {
            Company currentCom = ((EInvoiceContext)FXContext.Current).CurrentCompany;
            ICustomerService _CusSvc = IoC.Resolve<ICustomerService>();
            Customer ic = (from c in _CusSvc.Query where (c.id == id && c.ComID == currentCom.id) select c).Single();
            return Json(new
            {
                ic.Name,
                ic.TaxCode,
                ic.Address,
                ic.BankNumber,
                ic.BankName,
            }
            );
        }

        /// <summary>
        /// NQuyet Lay thong tin Vung du lieu day len View
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public JsonResult getResource(string key)
        {
            IResourceAreasService prSrv = IoC.Resolve<IResourceAreasService>();
            Company currentCompany = ((EInvoiceContext)FXContext.Current).CurrentCompany;
            IGroupResourceService grResourceSrv = IoC.Resolve<IGroupResourceService>();
            UserGroup userRsr = grResourceSrv.GetbyCurrent(HttpContext.User.Identity.Name);
            if (userRsr.IsAdmin == 2)
            {
                var v = prSrv.Query.Where(p => p.ComID == currentCompany.id && p.Code.ToUpper().Contains(key.ToUpper())).Select(c => new { c.Code, c.Name }).Take(30);
                return Json(v);
            }
            var vv = userRsr.Resources.Where(p => p.ToUpper().Contains(key.ToUpper())).Select(c => new { Code = c, Name = c }).Take(30);
            return Json(vv);
        }

        #region
        //viet ghi chu trong hoa don
        [HttpGet]
        public ActionResult WriteNote(string Pattern, int id, string TypeView)
        {
            Company currentCom = ((EInvoiceContext)FXContext.Current).CurrentCompany;
            IInvoiceService IInvSrv = InvServiceFactory.GetService(Pattern, currentCom.id);
            IInvoice Einv = IInvSrv.Getbykey<IInvoice>(id);
            //thong tin view model
            WriteNoteModel model = new WriteNoteModel();
            model.id = id;
            model.pattern = Pattern;
            model.TypeView = TypeView;
            if (string.IsNullOrEmpty(Einv.Note)) model.Note = "";
            else model.Note = Einv.Note;
            return View(model);
        }
        [HttpPost]
        public ActionResult WriteNote(WriteNoteModel model)
        {
            if (model.id <= 0)
                throw new HttpRequestValidationException();
            Company currentCom = ((EInvoiceContext)FXContext.Current).CurrentCompany;
            IInvoiceService IInvSrv = InvServiceFactory.GetService(model.pattern, currentCom.id);
            IInvoice EInvoiceEditNote = IInvSrv.Getbykey<IInvoice>(model.id);
            try
            {
                EInvoiceEditNote.Note = model.Note;
                IInvSrv.Save(EInvoiceEditNote);
                IInvSrv.CommitChanges();
                Log.Info("Writenote EInvoice by: " + HttpContext.User.Identity.Name);
                Messages.AddFlashMessage("Ghi chú thành công!");
            }
            catch (Exception ex)
            {
                Log.Error(" WriteNote-" + ex.Message);
                Messages.AddErrorFlashMessage("Ghi chú không thành công!");
            }
            if (model.TypeView == "0") return RedirectToAction("Index", new { Pattern = model.pattern, Serial = EInvoiceEditNote.Serial });
            else if (model.TypeView == "1")
                return RedirectToAction("ReplaceInvIndex", "Adjust", new { pattern = model.pattern, Serial = EInvoiceEditNote.Serial });
            else
                return RedirectToAction("AdjustInvIndex", "Adjust", new { pattern = model.pattern, Serial = EInvoiceEditNote.Serial });

        }
        #endregion

        private MessageViewData Messages
        {
            get
            {
                if (!ViewData.ContainsKey("Messages"))
                {
                    throw new InvalidOperationException("Messages are not available. Did you add the MessageFilter attribute to the controller?");
                }
                return (MessageViewData)ViewData["Messages"];
            }
        }
        #region phát hành bằng token
        [RBACAuthorize(Permissions = "Release_invInList")]
        public ActionResult LaunchChoiceWithToken(string hdPattern, string Serial, string[] cbid, string signValue, string idInvoice, string serialCertSign, string base64Hash)
        {
            Log.Info("LaunchChoiceWithToken by: " + HttpContext.User.Identity.Name);
            Company currentCom = ((EInvoiceContext)FXContext.Current).CurrentCompany;
            try
            {
                int[] ids = (from s in cbid select Convert.ToInt32(s)).ToArray();
                if (ids.Length < 0)
                {
                    Messages.AddErrorFlashMessage("Bạn chưa chọn hóa đơn.");
                    return RedirectToAction("Index", new { Pattern = hdPattern, Serial = Serial });
                }
                IKeyStoresService KeySrv = IoC.Resolve<IKeyStoresService>();
                KeyStores keyStores = KeySrv.Query.Where(ks => ks.ComID == currentCom.id).FirstOrDefault();
                if (keyStores == null)
                {
                    Messages.AddErrorFlashMessage("Công ty chưa đăng ký chứng thư!");
                    return RedirectToAction("Index", new { Pattern = hdPattern, Serial = Serial });
                }
                string serialDB = keyStores.SerialCert.ToUpper();
                if (string.IsNullOrEmpty(serialDB))
                {
                    Messages.AddErrorFlashMessage("Công ty chưa đăng ký chứng thư!");
                    return RedirectToAction("Index", new { Pattern = hdPattern, Serial = Serial });
                }
                if (!serialCertSign.ToUpper().Equals(serialDB))
                {
                    Messages.AddErrorFlashMessage("Chứng thư không đúng với chứng thư công ty đăng ký trên hệ thống!");
                    return RedirectToAction("Index", new { Pattern = hdPattern, Serial = Serial });
                }
                ICertificateService certSrv = IoC.Resolve<ICertificateService>();
                Certificate certModel = certSrv.Query.Where(ce => ce.ComID == currentCom.id && ce.SerialCert.ToUpper().Equals(serialDB)).FirstOrDefault();
                if (certModel == null)
                {
                    Messages.AddErrorFlashMessage("Chứng thư công ty chưa có trong hệ thống!");
                    return RedirectToAction("Index", new { Pattern = hdPattern, Serial = Serial });
                }
                byte[] raw = Convert.FromBase64String(certModel.Cert);
                X509Certificate2 cert = new X509Certificate2(raw);
                DateTime now = DateTime.Now;
                if (cert.NotBefore > now)
                {
                    Messages.AddErrorFlashMessage("Chứng thư chưa đến thời gian sử dụng!");
                    return RedirectToAction("Index", new { Pattern = hdPattern, Serial = Serial });
                }
                if (cert.NotAfter < now)
                {
                    Messages.AddErrorFlashMessage("Chứng thư đã hết hạn!");
                    return RedirectToAction("Index", new { Pattern = hdPattern, Serial = Serial });
                }
                IInvoiceService _InvoiSrc = InvServiceFactory.GetService(hdPattern, currentCom.id);
                IList<IInvoice> lstInvoice = _InvoiSrc.GetByID(currentCom.id, ids);
                var result = from item in lstInvoice
                             from other in lstInvoice
                             where item.Pattern != other.Pattern ||
                                   item.Serial != other.Serial
                             select item;
                if (result != null && result.Count() > 0)
                {
                    Messages.AddErrorFlashMessage("Các hóa đơn cần phát hành phải cùng một mẫu số và ký hiệu!");
                    return RedirectToAction("Index", new { Pattern = hdPattern, Serial = Serial });
                }

                List<SignTokenJsonModel> listCache = (List<SignTokenJsonModel>)HttpRuntime.Cache[base64Hash + "_" + currentCom.id];
                if (listCache == null || listCache.Count() <= 0)
                {
                    Messages.AddErrorFlashMessage("Có lỗi trong quá trình phát hành!");
                    Log.Error("ERR: LaunchChoiceWithToken listCache null");
                    return RedirectToAction("Index", new { Pattern = hdPattern, Serial = Serial });
                }
                IList<SignHashValueModel> lstSignValue = (IList<SignHashValueModel>)signValue.DeserializeJSON<SignHashValueModel>(typeof(IList<SignHashValueModel>));
                if (lstSignValue == null || lstSignValue.Count() <= 0)
                {
                    Messages.AddErrorFlashMessage("Có lỗi trong quá trình phát hành!");
                    Log.Error("ERR: LaunchChoiceWithToken lstSignValue null");
                    return RedirectToAction("Index", new { Pattern = hdPattern, Serial = Serial });
                }
                foreach (var itemListCache in listCache)
                {
                    foreach (var itemlstSignValue in lstSignValue)
                    {
                        if (itemlstSignValue.hashValue.Equals(itemListCache.hash))
                        {
                            itemListCache.signValue = itemlstSignValue.signedValue;
                        }
                    }
                }

                Dictionary<IInvoice, byte[]> dicInv = new Dictionary<IInvoice, byte[]>();
                foreach (var itemListCache in listCache)
                {
                    foreach (var itemLstInvoice in lstInvoice)
                    {
                        itemLstInvoice.TaxSignaltureDate = decimal.Parse(DateTime.Now.ToString("yyyyMMddHHmmss"));
                        if (int.Parse(itemListCache.idInv) == itemLstInvoice.id)
                        {
                            dicInv.Add(itemLstInvoice, Convert.FromBase64String(itemListCache.signValue));
                        }
                    }
                }
                ILauncherService _launcher = IoC.Resolve(Type.GetType(currentCom.Config["LauncherType"])) as ILauncherService;
                _launcher.remotePublishInv(hdPattern, lstInvoice.FirstOrDefault().Serial, dicInv);
                string strResult = _launcher.Message;
                if (strResult.Contains("OK"))
                {
                    bool isTax = false;
                    if (currentCom.Config.Keys.Contains("IsConnect2Tax") && currentCom.Config["IsConnect2Tax"].Equals("1")) // kiểm tra trạng thái có kết nối thuế không, cấu hình trong config
                    {
                        isTax = true; // có kết nối thuế
                    }
                    if (isTax)
                    {
                        StringBuilder xmlCompress = new StringBuilder();
                        string TaxCodeWS, PatternWS, SerialWS;
                        List<int> listId = new List<int>();
                        Log.Info("dong goi gui len service");
                        //vatInvoice = _InvoiSrc.GetByID(currentCom.id, hdPattern, int.Parse(idInvoice));
                        TaxCodeWS = currentCom.TaxCode;
                        PatternWS = hdPattern;
                        SerialWS = lstInvoice.FirstOrDefault().Serial;
                        xmlCompress.AppendLine("<Tvan_Invoices>");
                        xmlCompress.AppendLine("<Comtax_Code>" + TaxCodeWS + "</Comtax_Code><Pattern>" + PatternWS + "</Pattern><Serial>" + SerialWS + "</Serial>");
                        xmlCompress.AppendLine("<Data>");
                        foreach (var item in lstInvoice)
                        {
                            xmlCompress.AppendLine(string.Format("<Fkey>{0}</Fkey><Invoice>{1}</Invoice>", item.Fkey, item.Data.ToString()));
                            listId.Add(item.id);
                        }
                        xmlCompress.AppendLine("</Data>");
                        xmlCompress.AppendLine("</Tvan_Invoices>");
                        Thread t = new Thread(() => this.callWS(xmlCompress.ToString(), TaxCodeWS, PatternWS, SerialWS, "04", listId, true));
                        t.Start();
                    }
                    Log.Info("Publish EInvoice by: " + HttpContext.User.Identity.Name + " Info-- Pattern: " + hdPattern + "; Serial: " + Serial);
                    Messages.AddFlashMessage(Resources.Message.PInv_IMesSuccess + " " + ids.Length + " " + Resources.Message.Minvoice);
                }
                else if (strResult.Contains("ERR:14"))
                {
                    Log.Error("ERR:14 " + strResult);
                    Messages.AddErrorFlashMessage("Có lô hóa đơn khác đang được phát hành, xin vui lòng thực hiện lại.");
                }
                else
                {
                    Log.Error("ERR: " + strResult);
                    Messages.AddErrorFlashMessage(strResult);
                }

                return RedirectToAction("Index", new { Pattern = hdPattern, Serial = Serial });
            }
            catch (Exception ex)
            {
                Log.Error("Exception: " + ex);
                Messages.AddErrorFlashMessage("Có lỗi xảy ra, vui lòng thực hiện lại!");
                Log.Error(" LaunchChoice-" + ex.Message);
                return RedirectToAction("Index", new { Pattern = hdPattern, Serial = Serial });
            }

        }
        public string getHashInvoice(string pattern, string certinfo, string serialCert, string type, List<string> lstIdInvoice)
        {
            try
            {
                IKeyStoresService KeySrv = IoC.Resolve<IKeyStoresService>();
                ICertificateService _ICerSrv = IoC.Resolve<ICertificateService>();
                //IVATInvoiceService vatInvoiSrv = IoC.Resolve<IVATInvoiceService>();
                IRepositoryINV _repository = IoC.Resolve<IRepositoryINV>();
                Company currentCom = ((EInvoiceContext)FXContext.Current).CurrentCompany;
                KeyStores keyStores = KeySrv.Query.Where(ks => ks.ComID == currentCom.id).FirstOrDefault();
                if (keyStores == null)
                {
                    return "getCertErr0";
                }
                string serialDB = keyStores.SerialCert.ToUpper();
                if (string.IsNullOrEmpty(serialDB)) return "getCertErr0";
                string serialCertUP = serialCert.ToUpper();
                if (!serialCertUP.EndsWith(serialDB)) return "getCertErr1";
                Certificate objCeroder = _ICerSrv.Query.Where(ce => ce.ComID == currentCom.id && ce.SerialCert.ToUpper().Equals(serialCert)).FirstOrDefault();
                //Certificate objCeroder = _ICerSrv.GetCertFromSerial(serialDB);
                if (objCeroder != null)
                {
                    certinfo = objCeroder.Cert;
                }
                byte[] raw = Convert.FromBase64String(certinfo);
                X509Certificate2 cert = new X509Certificate2(raw);
                DateTime now = DateTime.Now;
                if (cert.NotBefore > now) return "getCertErr2";
                if (cert.NotAfter < now) return "getCertErr3";
                if (objCeroder == null)
                {
                    Certificate objCer = new Certificate();
                    string[] Issuer = cert.Issuer.Split(',');
                    string OwnCA = Array.Find(Issuer, i => i.Trim().StartsWith("CN=", StringComparison.Ordinal)).Trim().Replace("CN=", "");
                    string[] Subject = cert.Subject.Split(',');
                    string OrganizationCA = Array.Find(Subject, i => i.Trim().StartsWith("CN=", StringComparison.Ordinal)).Trim().Replace("CN=", "");
                    objCer.OwnCA = OwnCA;
                    objCer.OrganizationCA = OrganizationCA;
                    objCer.Cert = certinfo;
                    objCer.ValidFrom = cert.NotBefore;
                    objCer.ValidTo = cert.NotAfter;
                    objCer.SerialCert = cert.SerialNumber.ToUpper();
                    objCer.ComID = currentCom.id;
                    _ICerSrv.Save(objCer);
                    _ICerSrv.CommitChanges();
                }
                int[] ids = (from s in lstIdInvoice select Convert.ToInt32(s)).ToArray();
                IInvoiceService _InvoiSrc = InvServiceFactory.GetService(pattern, currentCom.id);
                IList<IInvoice> lst = _InvoiSrc.GetByID(currentCom.id, ids);
                var result = from item in lst
                             from other in lst
                             where item.Pattern != other.Pattern ||
                                   item.Serial != other.Serial
                             select item;
                if (result != null && result.Count() > 0)
                {
                    //Messages.AddErrorFlashMessage("Các hóa đơn cần phát hành phải cùng một mẫu số và ký hiệu!");
                    return "getCertErr5";
                }
                IDictionary<string, string> dic = Launcher.Instance.GetDigestForRemote(pattern, lst.FirstOrDefault().Serial, lst.ToArray());
                string base64Hash = "";
                int k = 1;
                foreach (KeyValuePair<string, string> entry in dic)
                {
                    if (k < dic.Count())
                    {
                        base64Hash += entry.Value.ToString() + ",";
                    }
                    else base64Hash += entry.Value.ToString();
                    k++;
                }
                List<SignTokenJsonModel> list = new List<SignTokenJsonModel>();
                list = dic.Select(p => new SignTokenJsonModel { idInv = p.Key, hash = p.Value }).ToList();
                HttpRuntime.Cache.Insert(base64Hash + "_" + currentCom.id, list, null, DateTime.Now.AddMinutes(5), Cache.NoSlidingExpiration);
                return base64Hash;
            }
            catch (Exception e)
            {
                Log.Error("CheckComCer: " + e.StackTrace);
                return "getCertErr";
            }
        }
        private void UpdateTvan(string transactionDataId)
        {
            var notGetFolder = ConfigurationManager.AppSettings["folderChuaLay"];
            var moveGetFolder = ConfigurationManager.AppSettings["folderDaLay"];
            if (!Directory.Exists(moveGetFolder))
                Directory.CreateDirectory(moveGetFolder);
            string msg = "OK";
            ILog log = LogManager.GetLogger(typeof(PublishService));
            try
            {
                string Results = Util.GetResults(transactionDataId);
                string status = "";
                string type = "";
                string bs64 = parseResults(Results, out status, out type);
                if (!"02".Equals(status))
                {
                    using (FileStream fs = System.IO.File.Create(notGetFolder + @"\" + transactionDataId)) { }
                    return;
                }
                byte[] xyz = Convert.FromBase64String(bs64);
                // giải nén
                byte[] buffer = Utils.Decompress(xyz);
                string _b2str = Encoding.UTF8.GetString(buffer);

                // phân tách kết quả
                RV obj = Utils.GetObjectXml<RV>(_b2str);
                OK ok = obj.OK;
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(_b2str);
                var lstUpdate = doc.DocumentElement.GetElementsByTagName("Invoice");

                ICompanyService companySrv = IoC.Resolve<ICompanyService>();
                Company modelCompany = companySrv.Query.FirstOrDefault(t => t.TaxCode.Equals(obj.ComTaxCode));
                bool flg = true;
                IInvoiceService IInvSrv = null;
                foreach (XmlElement item in lstUpdate)
                {
                    XElement it = XElement.Parse("<root>" + item.InnerXml + "</root>");
                    string vPattern = it.Element("Pattern").Value;
                    string vSerial = it.Element("Serial").Value;
                    string vNo = it.Element("No").Value;
                    string vBarcode = it.Element("Barcode").Value;
                    string vMTC = it.Element("MTC").Value;
                    if (flg)
                    {
                        IInvSrv = InvServiceFactory.GetService(vPattern, modelCompany.id);
                        flg = false;
                    }
                    string qr = "update INVOICEVAT set BARCODE = :BARCODE, MTC = :MTC where PATTERN = :PATTERN And  NO = :NO  And SERIAL = :SERIAL And COMID = :COMID";
                    IInvSrv.ExcuteNonQuery(qr, false, new SQLParam("BARCODE", vBarcode), new SQLParam("MTC", vMTC), new SQLParam("PATTERN", vPattern), new SQLParam("NO", vNo), new SQLParam("SERIAL", vSerial), new SQLParam("COMID", modelCompany.id));
                }
            }
            catch (Exception ex)
            {
                log.Error("Error ProcessUpdateTvan: " + ex);
                msg = "Error ProcessUpdateTvan: " + ex.Message;
                using (FileStream fs = System.IO.File.Create(notGetFolder + @"\" + transactionDataId)) { }
                //throw new SoapException("Error ProcessUpdateTvan: ", SoapException.ClientFaultCode, ex);
            }
            log.Info("end updateTvan: " + transactionDataId);
            using (FileStream fs = System.IO.File.Create(moveGetFolder + @"\" + transactionDataId)) { }
        }
        private static string parseResults(string msg, out string status, out string type)
        {
            string strResult = "";
            XElement elem = XElement.Parse(msg);
            type = elem.Element("Type").Value;
            XElement Result = elem.Element("Result");
            status = Result.Element("Status").Value;
            strResult = Result.Element("Data").Value;
            return strResult;
        }
        private bool Compress(Stream data, Stream outData, out string str)
        {
            str = "";
            try
            {
                using (ICSharpCode.SharpZipLib.Zip.ZipOutputStream zipStream = new ICSharpCode.SharpZipLib.Zip.ZipOutputStream(outData))
                {
                    zipStream.SetLevel(3);
                    ICSharpCode.SharpZipLib.Zip.ZipEntry newEntry = new ICSharpCode.SharpZipLib.Zip.ZipEntry("hoadon.xml");
                    newEntry.DateTime = DateTime.UtcNow;
                    zipStream.PutNextEntry(newEntry);
                    data.Position = 0;
                    int size = (data.CanSeek) ? Math.Min((int)(data.Length - data.Position), 0x2000) : 0x2000;
                    byte[] buffer = new byte[size];
                    int n;
                    do
                    {
                        n = data.Read(buffer, 0, buffer.Length);
                        zipStream.Write(buffer, 0, n);
                    } while (n != 0);
                    zipStream.CloseEntry();
                    zipStream.Flush();
                    zipStream.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                str = ex.Message;
                return false;
            }
        }
        private string ProcessXmlDataInvoce(string dataXml, string taxCode, string transactionTvan, string pattern, string serial, string type, out byte[] dataByte, out string fileName, out string str)
        {
            try
            {
                byte[] byteArray = Encoding.UTF8.GetBytes(dataXml);
                MemoryStream stream = new MemoryStream(byteArray);
                var dgFolder = ConfigurationManager.AppSettings["folderDG"];
                if (!Directory.Exists(dgFolder))
                    Directory.CreateDirectory(dgFolder);
                // Lưu ra ổ cài đặt
                fileName = dgFolder + @"\" + taxCode + @"_" + pattern.Replace("/", "-") + @"_" + serial.Replace("/", "") + @"_" + transactionTvan +
                           "_" + type + ".zip";
                Stream compStream = new FileStream(fileName, FileMode.CreateNew, FileAccess.ReadWrite, FileShare.ReadWrite);
                str = "";
                bool check = Compress(stream, compStream, out str);
                if (check)
                {
                    compStream.Close();
                    //// Chuyển file Zip thành mảng bytes
                    FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                    dataByte = new byte[fs.Length];
                    fs.Read(dataByte, 0, System.Convert.ToInt32(fs.Length));
                    fs.Close();
                    fileName = Path.GetFileName(fileName);
                    // Chuyển mảng bytes thành Base64string
                    return Convert.ToBase64String(dataByte);
                }
                else
                {
                    dataByte = new byte[10];
                    fileName = "";
                    return "";
                }
            }
            catch (Exception ex)
            {
                str = ex.Message;
                dataByte = new byte[10];
                fileName = "";
                return "";
            }

        }
        public void callWS(string compressXML, string TaxCodeWS, string PatternWS, string SerialWS, string typeWS, List<int> listId, bool flg) //flg = true new, replace, adjust
        {
            string compFolder = ConfigurationManager.AppSettings["folderDG"];
            string notSendFolder = ConfigurationManager.AppSettings["folderChuaGui"];
            string SendFolder = ConfigurationManager.AppSettings["folderDaGui"];
            string notGetFolder = ConfigurationManager.AppSettings["folderChuaLay"];
            string transactionTvan = Guid.NewGuid().ToString();
            ILog log = LogManager.GetLogger(typeof(PublishService));
            byte[] dtByte;
            string fileName = "";
            string str = "";
            log.Info("Dong goi file ");
            if (!Directory.Exists(notGetFolder))
                Directory.CreateDirectory(notGetFolder);
            if (!Directory.Exists(SendFolder))
                Directory.CreateDirectory(SendFolder);
            try
            {
                string dtBase64 = ProcessXmlDataInvoce(compressXML, TaxCodeWS, transactionTvan,
                                                          PatternWS, SerialWS, typeWS, out dtByte, out fileName, out str);

                log.Info("Kiem tra dong goi file ");
                if ("".Equals(dtBase64))
                {

                    InvoiceVATService vatInvoiceSrv1 = IoC.Resolve<InvoiceVATService>();
                    vatInvoiceSrv1.BeginTran();
                    string qr = "update InvoiceVAT set UploadStatus = :UploadStatus where id in (" + string.Join(",", listId) + ")";
                    if (flg)
                        vatInvoiceSrv1.ExcuteNonQuery(qr, true, new SQLParam("UploadStatus", 0));
                    else
                        vatInvoiceSrv1.ExcuteNonQuery(qr, true, new SQLParam("UploadStatus", 1));
                    vatInvoiceSrv1.CommitTran();
                    log.Error("Loi dong goi file " + str);
                }
                else
                {
                    log.Info("Dong goi file thanh cong va gui WS");

                    string msg = Util.importBarcode(typeWS, transactionTvan, TaxCodeWS, PatternWS, dtBase64);
                    string status = "";
                    log.Info("Parse XML");
                    string parseMsg = parseResultXML(msg, out status);
                    log.Info("Kiem tra file gui len WS ");
                    if ("02".Equals(status))
                    {
                        log.Info("File gui len WS thanh cong");
                        System.IO.File.Move(compFolder + @"\" + fileName, SendFolder + @"\" + fileName);
                        if (flg)
                        {
                            log.Info("Delay 2s de get KQ");
                            Thread.Sleep(2000);
                            log.Info("bat dau get KQ");
                            Thread t = new Thread(() => UpdateTvan(parseMsg));
                            t.Start();
                        }
                    }
                    else
                    {
                        log.Info("file gui len that bai " + parseMsg);
                        System.IO.File.Move(compFolder + @"\" + fileName, notSendFolder + @"\" + fileName);
                    }

                }
            }
            catch (Exception ex)
            {

                log.Error("Loi gui len WS:" + ex.Message);
                System.IO.File.Move(compFolder + @"\" + fileName, notSendFolder + @"\" + fileName);

            }

        }
        private string parseResultXML(string msg, out string status)
        {
            string Result = "";
            XElement elem = XElement.Parse(msg);
            string type = elem.Element("Type").Value;
            XElement ResultE = elem.Element("Result");
            XElement Err = elem.Element("ERR");

            status = ResultE.Element("Status").Value;
            if ("02".Equals(status))
            {
                Result = ResultE.Element("CodeTransactionTax").Value;
            }
            else
            {
                Result = Err.Element("Code").Value;
            }
            return Result;
        }
        #endregion

        public ActionResult getPrintData(string idInvoice, string pattern)
        {
            string str = "";
            Company currentCom = ((EInvoiceContext)FXContext.Current).CurrentCompany;
            int id = int.Parse(AesUtil.Decrypt(idInvoice).Replace(";", ""));
            IInvoiceService IInvSrv = InvServiceFactory.GetService(pattern, currentCom.id);
            IInvoice vat = IInvSrv.GetByID(currentCom.id, pattern, id);
            if (vat != null)
            {
                IRegisterTempService reSrv = IoC.Resolve<IRegisterTempService>();
                RegisterTemp regis = reSrv.GetbyPattern(pattern, vat.ComID);
                IInvTemplateService tempSvr = IoC.Resolve<IInvTemplateService>();
                InvTemplate temp = tempSvr.Getbykey(regis.InvoiceTemp.Id);
                IViewer _iViewerSrv = InvServiceFactory.GetViewer(pattern, currentCom.id);
                if (vat.Status != InvoiceStatus.NewInv)
                {
                    IRepositoryINV _iRepoSrv = IoC.Resolve<IRepositoryINV>();
                    str = _iViewerSrv.GetHtml(_iRepoSrv.GetData(vat), vat.Status);
                }
                else
                {
                    str = _iViewerSrv.GetHtml(Encoding.UTF8.GetBytes(vat.GetXMLData()), temp, vat.Status, "");
                }
            }
            return Json(str);
        }
    }
}
