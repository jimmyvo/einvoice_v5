﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EInvoice.Core.IService;
using log4net;
using FX.Utils.EmailService;
using System.IO;
using EInvoice.Core.Domain;
using FX.Core;

namespace EInvoice.Core
{
    class SendEmailDBSellerService : IEmailService
    {
        private static ILog log = LogManager.GetLogger(typeof(SendEmailDBSellerService));
        private static readonly string defaultExtension = ".txt";
        private string _templateDir;
        private string _language;
        private IEmailTemplateEngine _templateEngine;
        private ISendEmailService _saveEmail;

        public SendEmailDBSellerService(ISendEmailService saveEmail, IEmailTemplateEngine templateEngine)
        {
            this._saveEmail = saveEmail;
            this._templateEngine = templateEngine;
        }

        public void ProcessEmail(string from, string to, string templateName, Dictionary<string, string> subjectParams, Dictionary<string, string> bodyParams, string invToken, Company currentCom)
        {
            string templatePath = "";
            if (currentCom.Config.Keys.Contains(templateName))
                templatePath = templateName;
            else
                templatePath = DetermineTemplatePath(templateName);

            try
            {
                string[] subjectAndBody = this._templateEngine.ProcessTemplate(templatePath, subjectParams, bodyParams);
                SendMail sm = new SendMail();
                sm.EmailFrom = from;
                sm.Email = to;
                sm.Subject = subjectAndBody[0];
                sm.Body = subjectAndBody[1];
                sm.CreateDate = DateTime.Now;
                sm.Status = 0;
                sm.GroupName = currentCom.id.ToString();
                sm.InvToken = invToken;
                _saveEmail.Save(sm);
                _saveEmail.CommitChanges();
                //this._saveEmail.Save(from, to, subjectAndBody[0], subjectAndBody[1]);
            }
            catch (Exception ex)
            {
                log.Error("Unable to process email message", ex);
                throw;
            }
        }

        public string DetermineTemplatePath(string templateName)
        {
            string fileName = templateName + defaultExtension;
            if (this._language != null)
            {
                string fileNameWithLanguage = templateName + "." + this._language.ToLower() + defaultExtension;
                string filePathWithLanguage = Path.Combine(this._templateDir, fileNameWithLanguage);
                // Check if file exists. If yes, return the filePathWithLanguage, otherwise continue.
                if (File.Exists(filePathWithLanguage))
                {
                    return filePathWithLanguage;
                }
            }
            string filePath = Path.Combine(this._templateDir, fileName);
            if (!File.Exists(filePath)) filePath = AppDomain.CurrentDomain.BaseDirectory + filePath;
            if (File.Exists(filePath))
            {
                return filePath;
            }
            else
            {
                throw new FileNotFoundException("Unable to find the email template: " + templateName);
            }
        }

        public void ProcessEmail(string from, string to, string templateName, Dictionary<string, string> subjectParams, Dictionary<string, string> bodyParams)
        {
            Company currentCom = ((EInvoiceContext)FXContext.Current).CurrentCompany;
            string templatePath = "";
            if (currentCom.Config.Keys.Contains(templateName))
                templatePath = templateName;
            else
                templatePath = DetermineTemplatePath(templateName);

            try
            {
                string[] subjectAndBody = this._templateEngine.ProcessTemplate(templatePath, subjectParams, bodyParams);
                SendMail sm = new SendMail();
                sm.EmailFrom = from;
                sm.Email = to;
                sm.Subject = subjectAndBody[0];
                sm.Body = subjectAndBody[1];
                sm.CreateDate = DateTime.Now;
                sm.Status = 0;
                sm.GroupName = EInvoiceContext.Current.CurrentUser.GroupName;
                _saveEmail.Save(sm);
                //this._saveEmail.Save(from, to, subjectAndBody[0], subjectAndBody[1]);
            }
            catch (Exception ex)
            {
                log.Error("Unable to process email message", ex);
                throw;
            }
        }

        public string Language
        {
            set { this._language = value; }
        }

        public string TemplateDir
        {
            set { this._templateDir = value; }
        }
    }
}
