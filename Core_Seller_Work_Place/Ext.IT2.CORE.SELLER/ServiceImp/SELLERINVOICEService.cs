﻿using EInvoice.Core.Domain;
using EInvoice.Core.IService;
using FX.Data;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using IdentityManagement.Domain;
using FX.Core;

namespace EInvoice.Core.ServiceImp
{
    public class SELLERINVOICEService : InvoiceBaseService<SELLERINVOICE>
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SELLERINVOICEService));
        public SELLERINVOICEService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        {
        }
        private const int MAX_SECTION = 1000;
        public override bool ConfirmPayment(string[] fkeys, string noteappends = "")
        {
            if (fkeys.Length <= 0)
                return false;
            int total = fkeys.Length;
            int section = MAX_SECTION;
            int sectionTotal = total % section == 0 ? total / section : total / section + 1;
            int sodu = total % section;
            try
            {
                BeginTran();
                for (int i = 0; i < total; i += section)
                {
                    if (total < i + section)
                    {
                        section = total - i;
                    }
                    string subkeys = "";
                    for (int j = 0; j < section; j++) subkeys += ",'" + fkeys[i + j] + "'";
                    subkeys = subkeys.TrimStart(',');
                    string _HQuery = "Update VSCMNSELLERINVOICE set PAYMENTSTATUS=:_paymentstatus, Note= Note || :_note  where fkey in (:_fkeys)";
                    ExcuteNonQuery(_HQuery, true, new SQLParam("_paymentstatus", (int)Payment.Paid), new SQLParam("_note", noteappends), new SQLParam("_fkeys", subkeys));
                }

                CommitTran();
                return true;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                RolbackTran();
                return false;
            }
        }

        public override bool UpdatePayment(int[] ids, Payment _status, string note)
        {
            string qr = "Update VSCMNSELLERINVOICE set PAYMENTSTATUS=" + (int)_status + " , Note = Note || :note where id in (" + string.Join(",", ids) + ")";
            ExcuteNonQuery(qr, true, new SQLParam("note", note));
            return true;
        }

        public override bool UpdatePayment(int[] ids, Payment _status)
        {

            string _Query = "Update VSCMNSELLERINVOICE set PAYMENTSTATUS=" + (int)_status + "  where id in (" + string.Join(",", ids) + ")";
            ExcuteNonQuery(_Query);
            return true;
        }

        public override IList<InvStatement> GetReportDetail(int comId, string parttern, int year, int month, user objUser = null)
        {
            IQueryable<InvStatement> source = from item in base.GetQuery<SELLERINVOICE>()
                                              where item.ComID == comId && item.Pattern == parttern && item.ArisingDate.Month == month && item.ArisingDate.Year == year && (int)item.Status != 0
                                              select new InvStatement
                                              {
                                                  ID = item.id.ToString(),
                                                  status = item.Status,
                                                  tyle = item.Type,
                                                  soHoaDon = item.No,
                                                  ngayLapHD = item.ArisingDate.ToString("dd/MM/yyyy"),
                                                  tenNguoiMua = ((item.CusName == null) || (item.CusName == "") || (item.CusName == ".")) ? item.Buyer : item.CusName,
                                                  masoThueNguoiMua = item.CusTaxCode,
                                                  VATAmount = item.VATAmount,
                                                  Grossvalue = item.Total,
                                                  VatRate = item.VATRate
                                              };
            return source.ToList<InvStatement>();
        }

        public override IList<InvReportTax> GetReportDetailTax(int comId, string parttern, int year, int month, user objUser = null)
        {
            try
            {
                IProductInvService productSer = IoC.Resolve<IProductInvService>();
                IInvoiceService invSrv = InvServiceFactory.GetService(parttern, comId);
                var listinv = (from item in invSrv.GetQuery<SELLERINVOICE>()
                               join itempro in productSer.Query on item.id equals itempro.InvID
                               where item.ComID == comId && item.Pattern == parttern &&
                                   item.PublishDate.Month == month && item.PublishDate.Year == year &&
                                   item.Status != InvoiceStatus.NewInv
                               select new InvReportTax
                               {
                                   ID = item.id.ToString(),
                                   status = item.Status,
                                   tyle = item.Type,
                                   isSum = itempro.IsSum,
                                   //ComtaxCode = item.ComTaxCode,
                                   Pattern = item.Pattern,
                                   Serial = item.Serial,
                                   soHoaDon = item.No,
                                   ngayLapHD = item.ArisingDate.ToString("dd/MM/yyyy"),
                                   tenNguoiMua = ((item.CusName == null) || (item.CusName == "") || (item.CusName == ".")) ? item.Buyer : item.CusName,
                                   masoThueNguoiMua = item.CusTaxCode,
                                   MatHang = itempro.Name,
                                   Grossvalue = itempro.Total,
                                   VatAmount = itempro.VATAmount,
                                   _VatRate = itempro.VATRate,
                               }).OrderBy(sohd => sohd.soHoaDon).ToList();

                // Check phân quyền theo Serial
                if (objUser != null)
                {
                    var userPublishInvoice = IoC.Resolve<IUserPublishInvoiceService>();
                    var lstSerial = userPublishInvoice.LstSerialByUserPublishInvoice(objUser.userid, comId, parttern);
                    listinv = listinv.Where(inv => lstSerial.Contains(inv.Serial)).ToList();
                }

                return listinv;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public override IList<BaoCaoBanHangTongHop> GetBaoCaoBanHangTongHop(int comId, string parttern, int year, int month)
        {
            throw new System.NotImplementedException();
        }

        public override IList<BaoCaoBanHangChiTietModel> GetBaoCaoBanHangChiTiet(int comId, string parttern, int year, int month)
        {
            throw new System.NotImplementedException();
        }

        public override IList<InvStatement> GetReportDetailQuarter(int comId, string parttern, int year, int quarter)
        {
            throw new System.NotImplementedException();
        }

        public override IList<InvStatement> GetReportDetail(int comId, string parttern, int year, int month, bool isArisingDate, user objUser = null)
        {
            var ret = from item in GetQuery<SELLERINVOICE>()
                      where item.ComID == comId && item.Pattern == parttern &&
                          item.ArisingDate.Month == month && item.ArisingDate.Year == year &&
                          item.Status != InvoiceStatus.NewInv
                      select (
                          new InvStatement
                          {
                              ID = item.id.ToString(),
                              status = item.Status,
                              tyle = item.Type,
                              soHoaDon = item.No,
                              ngayLapHD = item.ArisingDate.ToString("dd/MM/yyyy"),
                              tenNguoiMua = ((item.CusName == null) || (item.CusName == "") || (item.CusName == ".")) ? item.Buyer : item.CusName,
                              masoThueNguoiMua = item.CusTaxCode,
                              VATAmount = item.VATAmount,
                              Grossvalue = item.Total,
                              VatRate = item.VATRate
                          }
                          );
            return ret.ToList();
        }
    }
}
