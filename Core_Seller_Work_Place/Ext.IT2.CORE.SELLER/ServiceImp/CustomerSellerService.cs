﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FX.Data;
using EInvoice.Core.Domain;
using EInvoice.Core.IService;
using IdentityManagement.Domain;
using FX.Core;
using IdentityManagement.Service;
using System.Web.Security;

namespace EInvoice.Core.ServiceImp
{
    public class CustomerSellerService : BaseService<Customer, int>, ICustomerService
    {
        public CustomerSellerService(string sessionFactoryConfigPath)
            : base(sessionFactoryConfigPath)
        { }
        //admin
        //Tìm kiếm  danh sách khách hàng 
        public IList<Customer> SearchByNameCode(string CusName, string Code, int company, int pageIndex, int pageSize, out int totalRecord)
        {
            string GroupComId = System.Configuration.ConfigurationManager.AppSettings["GroupComId"];
            IQueryable<Customer> qr;
            if (!string.IsNullOrEmpty(GroupComId))
            {
                int[] ArrGroupComId = GroupComId.Split(';').Select(int.Parse).ToArray();
                qr = from c in Query where ArrGroupComId.Contains(c.ComID) select c;
                 
            } else {
                qr = from c in Query where c.ComID == company select c;
            }
            //IQueryable<Customer> qr = from c in Query where c.ComID == company select c;
            if (CusName != null && CusName.Trim() != string.Empty)
            {
                CusName = CusName.Length > 200 ? CusName.Substring(0, 200) : CusName;
                qr = from c in qr where c.Name.ToUpper().Contains(CusName.ToUpper().Trim()) select c;
            }

            if (Code != null && Code.Trim() != string.Empty)
            {
                qr = from c in qr where c.Code.ToUpper().Contains(Code.ToUpper().Trim()) select c;
            }
            qr = qr.OrderByDescending(c => c.id);
            // Edit Paging 
            if (pageSize > 0)
            {
                int maxtemp = pageIndex <= 1 ? 4 - pageIndex : 2;//load tối đa 2 trang tiếp theo, nếu page =1 hoặc 2 thì sẽ load 4 trang hoặc 3 trang
                var ret = qr.Skip(pageIndex * pageSize).Take(pageSize * maxtemp + 1).ToList();
                totalRecord = pageIndex * pageSize + ret.Count;
                return ret.Take(pageSize).ToList();
            }
            else
            {
                var ret = qr.ToList();
                totalRecord = ret.Count;
                return ret;
            }
        }
        //admin
        //Tìm kiếm khách hàng
        public Customer SearchID(int id, int Comid)
        {
            //Customer qr = Query.Where(cus => (cus.id == id) && (cus.ComID == Comid)).FirstOrDefault();
            string GroupComId = System.Configuration.ConfigurationManager.AppSettings["GroupComId"];
            IQueryable<Customer> qr;
            if (!string.IsNullOrEmpty(GroupComId))
            {
                int[] ArrGroupComId = GroupComId.Split(';').Select(int.Parse).ToArray();
                qr = from c in Query where ArrGroupComId.Contains(c.ComID) && c.id == id select c;
            }
            else 
                qr = Query.Where(cus => (cus.id == id) && (cus.ComID == Comid));
            return qr.FirstOrDefault();
        }
        //portal
        //tim kiem khach hang dk username
        public Customer SearchUserName(string username, int ComId)
        {

            string GroupComId = System.Configuration.ConfigurationManager.AppSettings["GroupComId"];
            IQueryable<Customer> qr;
            if (!string.IsNullOrEmpty(GroupComId))
            {
                int[] ArrGroupComId = GroupComId.Split(';').Select(int.Parse).ToArray();
                qr = from c in Query where ArrGroupComId.Contains(c.ComID) && c.AccountName.Equals(username.Trim()) select c;

            }
            else
            {
                qr = from c in Query where c.ComID == ComId select c;
            }
            return qr.FirstOrDefault();
        }

        //txtruong mail
        public string Email(string codecus)
        {
            try
            {
                string email = (from cus in Query where (cus.Code.Equals(codecus)) select cus).FirstOrDefault().Email;
                return email;
            }
            catch (Exception)
            {
                return "";
            }

        }

        public Customer SearchCusCode(string cusCode, int comID)
        {
            string GroupComId = System.Configuration.ConfigurationManager.AppSettings["GroupComId"];
            IQueryable<Customer> qr;
            if (!string.IsNullOrEmpty(GroupComId))
            {
                int[] ArrGroupComId = GroupComId.Split(';').Select(int.Parse).ToArray();
                qr = from c in Query where ArrGroupComId.Contains(c.ComID) && c.Code.Equals(cusCode.Trim()) select c;
            }
            else
                qr = from c in Query where c.ComID == comID select c;
            return qr.FirstOrDefault();
            //return Query.Where(c => c.Code == cusCode && c.ComID == comID).FirstOrDefault();
        }
        /// <summary>
        /// Tạo mới khách hàng
        /// </summary>
        /// <param name="Cus">Đối tượng khách hàng</param>
        /// <param name="Cer">Đối tượng chứng thư</param>
        /// <param name="Comid">Xác định khách hàng của công ty nào</param>
        /// <param name="ErrorMessage">Thông điệp thông báo lỗi</param>
        /// <returns>true(Thành công) or(Thất bại)</returns>
        public bool CreateCus(Customer Cus, Certificate Cer, int Comid, out string ErrorMessage)
        {
            ErrorMessage = "";
            string status;
            Company currentCom = ((EInvoiceContext)FXContext.Current).CurrentCompany;
            string randompass = "";
            randompass = (currentCom.Config.Keys.Contains("SetDefaultCusPass")) ? currentCom.Config["SetDefaultCusPass"] : IdentityManagement.WebProviders.RBACMembershipProvider.CreateRandomPassword(8);

            //string randompass = IdentityManagement.WebProviders.RBACMembershipProvider.CreateRandomPassword(8);
            IdentityManagement.WebProviders.IRBACMembershipProvider _MemberShipProvider = FX.Core.IoC.Resolve<IdentityManagement.WebProviders.IRBACMembershipProvider>();
            BeginTran();
            try
            {
                // kiểm tra tài khoản được sử dụng chưa
                IuserService _userService = IoC.Resolve<IuserService>();
                user us = _userService.Query.Where(u => u.username == Cus.AccountName && u.GroupName== Cus.ComID.ToString()).FirstOrDefault();

                //user us = _MemberShipProvider.GetUser(Cus.AccountName, true);
                if (us != null)
                {
                    ErrorMessage = "Tài khoản có trong hệ thống";
                    return false;
                }
                var cusCodeQr = (from cus in Query where cus.Code == Cus.Code && cus.ComID==Cus.ComID select cus);
                if (cusCodeQr.Count() != 0)
                {
                    ErrorMessage = "Mã khách hàng có trong hệ thống";
                    return false;
                }
                user newuser = new user();
                IuserService _userSVC = IoC.Resolve<IuserService>();
                newuser.username = Cus.AccountName;
                newuser.password = FormsAuthentication.HashPasswordForStoringInConfigFile(randompass, "MD5"); ;
                newuser.email = Cus.Email;
                newuser.IsApproved = true;
                newuser.GroupName = Comid.ToString();
                newuser.PasswordSalt = "MD5";
                newuser.PasswordFormat = 1;
                newuser.ApplicationList = new List<Applications>();
                IApplicationsService _AppSVC = IoC.Resolve<IApplicationsService>();
                Applications _app = _AppSVC.GetByName("EInvoice");  //Chu y fix cung phu hop voi services.config/IRBACMembershipProvider
                newuser.ApplicationList.Add(_app);
                _userSVC.CreateNew(newuser);

                //newuser = _MemberShipProvider.CreateUser(Cus.AccountName, randompass, Cus.Email, "", "", true, null, Comid.ToString(), out status);
                if (newuser != null)
                {
                    // lưu thông tin khách hàng
                    CreateNew(Cus);
                }
                else
                {
                    ErrorMessage = "Không tạo được khách hàng";
                    return false;
                }
                //lưu thông tin về cerificate
                if (Cer.SerialCert != null && Cer.SerialCert.Trim() != string.Empty)
                {
                    // kiểm tra chứng thư có trong hệ thông chưa
                    ICertificateService _cerSVC = FX.Core.IoC.Resolve<ICertificateService>();
                    var cerQr = from c in _cerSVC.Query where c.SerialCert.ToUpper() == Cus.SerialCert.ToUpper() && c.ComID == Comid select c;
                    if (cerQr.Count() == 0)
                    {
                        Cer.ComID = Comid;
                        _cerSVC.CreateNew(Cer);
                    }
                }
                // send Mail--
                try
                {
                    if (!string.IsNullOrEmpty(Cus.Email))
                    {
                        FX.Utils.EmailService.IEmailService emailSrv = FX.Core.IoC.Resolve<FX.Utils.EmailService.IEmailService>();
                        Dictionary<string, string> subjectParams = new Dictionary<string, string>(1);
                        subjectParams.Add("$subject", "");
                        Dictionary<string, string> bodyParams = new Dictionary<string, string>(3);
                        bodyParams.Add("$cusname", Cus.Name);
                        bodyParams.Add("$username", newuser.username);
                        bodyParams.Add("$password", randompass);
                        string labelMail = currentCom.Config.ContainsKey("LabelMail") ? currentCom.Config["LabelMail"] : "hoadondientu@vnpt-invoice.vn";
                        emailSrv.ProcessEmail(labelMail, Cus.Email, "RegisterCustomer", subjectParams, bodyParams);
                    }
                }
                catch(Exception ex)
                { 
                    
                }
                CommitTran();
                return true;
            }
            catch (Exception ex)
            {
                RolbackTran();
                ErrorMessage = ex.Message;
                return false;
            }

        }
        /// <summary>
        /// Sửa đổi thông tin khách hàng
        /// </summary>
        /// <param name="Cus">Đối tượng thông tin khách hàng</param>
        /// <param name="Cer">Đối tượng chứng thư</param>
        /// <param name="Comid">Xác định công ty quản lý khách hàng</param>
        /// <param name="ErrorMessage">Thông điệp lỗi</param>
        /// <returns>true(Thành công) or false (Thất bại)</returns>
        public bool UpdateCus(Customer Cus, Certificate Cer, int Comid, out string ErrorMessage)
        {
            BeginTran();
            try
            {
                ErrorMessage = "";
                IdentityManagement.WebProviders.IRBACMembershipProvider _MemberShipProvider = FX.Core.IoC.Resolve<IdentityManagement.WebProviders.IRBACMembershipProvider>();
                user ouser = _MemberShipProvider.GetUser(Cus.AccountName, true);
                if (ouser.email != Cus.Email)
                {
                    // gán mail mới cho tk cũ
                    ouser.email = Cus.Email;
                    _MemberShipProvider.UpdateUser(ouser);
                }
                // thông tin chứng thư
                if (Cer.SerialCert != null && Cer.SerialCert.Trim() != string.Empty)
                {
                    // kiểm tra chứng thư có trong hệ thông chưa
                    ICertificateService _cerSVC = FX.Core.IoC.Resolve<ICertificateService>();
                    var cerQr = from c in _cerSVC.Query where c.SerialCert == Cus.SerialCert && c.ComID == Comid select c;
                    if (cerQr.Count() == 0)
                    { // chưa có chứng thư này trong hệ thống 
                        Cer.ComID = Comid;
                        _cerSVC.CreateNew(Cer);
                    }
                }
                else
                {
                    Cus.CusType = 0;
                }
                Save(Cus);
                CommitTran();
                return true;
            }
            catch (Exception ex)
            {
                RolbackTran();
                ErrorMessage = ex.Message;
                return false;
            }

        }
        public bool DeleteCus(int id, out string ErrorMessage)
        {
            BeginTran();
            try
            {
                ErrorMessage = "";
                Customer model = Getbykey(id);
               // IdentityManagement.WebProviders.IRBACMembershipProvider _MemberShipProvider = FX.Core.IoC.Resolve<IdentityManagement.WebProviders.IRBACMembershipProvider>();

                IuserService _userService = IoC.Resolve<IuserService>();
                user us = _userService.Query.Where(u => u.username == model.AccountName && u.GroupName == model.ComID.ToString()).FirstOrDefault();
                _userService.Delete(us);
                //if (_MemberShipProvider.DeleteUser(model.AccountName, true))
                //{
                    // ICertificateService _cerSVC = FX.Core.IoC.Resolve<ICertificateService>();
                    // Certificate cerQr = (from c in _cerSVC.Query where c.SerialCert == model.SerialCert && (c.ComID == model.ComID) select c).SingleOrDefault();
                    //_cerSVC.Delete(cerQr);
                    Delete(model);
                    CommitTran();
                    return true;
                //}
                //else
                //{
                //    ErrorMessage = "Xóa khách hàng không thành công!";
                //    return false;
                //}
            }
            catch (Exception ex)
            {
                RolbackTran();
                ErrorMessage = ex.Message;
                return false;
            }
        }

    }

}