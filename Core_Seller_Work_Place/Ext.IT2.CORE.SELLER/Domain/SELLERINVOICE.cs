﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Xml;
using System.Runtime.Serialization;
using System.Xml.Linq;
using FX.Core;
using System.Linq;
using System.Globalization;
using System.Configuration;

namespace EInvoice.Core.Domain
{
    [XmlType("Invoice")]
    [DataContract(Name = "Invoice", Namespace = "")]
    public class SELLERINVOICE : InvoiceBase
    {
        /// <summary>
        /// Company Fax
        /// </summary>
        [XmlElement("ComFax")]
        [DataMember(Name = "ComFax")]
        public virtual string ComFax { get; set; }

        /// <summary>
        /// Tong tien truoc thue
        /// </summary>
        [XmlElement("GrossValue")]
        [DataMember(Name = "GrossValue")]
        public override decimal GrossValue { get; set; }

        [XmlElement("Extra")]
        [DataMember(Name = "Extra")]
        public virtual string Extra { get; set; }

        [XmlIgnore]
        [IgnoreDataMember]
        public virtual int Idrecordsinvcancel { get; set; }

        [XmlElement("VATRate")]
        [DataMember(Name = "VAT_Rate", Order = 22)]
        public virtual float VATRate { get; set; }

        [XmlElement("GrossValue0")]
        [DataMember(Name = "GrossValue0")]
        public override decimal GrossValue0 { get; set; }

        [XmlElement("GrossValue5")]
        [DataMember(Name = "GrossValue5")]
        public override decimal GrossValue5 { get; set; }

        [XmlElement("GrossValue10")]
        [DataMember(Name = "GrossValue10")]
        public override decimal GrossValue10 { get; set; }

        [XmlElement("VATAmount")]
        [DataMember(Name = "VATAmount")]
        public override decimal VATAmount { get; set; }

        [XmlElement("VatAmount0")]
        [DataMember(Name = "VatAmount0")]
        public override decimal VatAmount0 { get; set; }

        [XmlElement("VatAmount5")]
        [DataMember(Name = "VatAmount5")]
        public override decimal VatAmount5 { get; set; }

        [XmlElement("VatAmount10")]
        [DataMember(Name = "VatAmount10")]
        public override decimal VatAmount10 { get; set; }

        [XmlElement("KindOfService"), DataMember(Name = "KindOfService", Order = 5)]
        public virtual string KindOfService { get; set; }

        public override string CurrencyUnit { get; set; }

        public override string Extra1 { get; set; }

        public override string Extra2 { get; set; }

        //START ADDED FOR SELLERINVOICE

        [XmlElement("DeliveryAddress")]
        [DataMember(Name = "DeliveryAddress")]
        public virtual string DeliveryAddress { get; set; }

        [XmlElement("PaymentTerm")]
        [DataMember(Name = "PaymentTerm")]
        public virtual string PaymentTerm { get; set; }

        [XmlElement("SellerName")]
        [DataMember(Name = "SellerName")]
        public virtual string SellerName { get; set; }

        [XmlElement("SellerAddress")]
        [DataMember(Name = "SellerAddress")]
        public virtual string SellerAddress { get; set; }

        [XmlElement("SellerTaxCode")]
        [DataMember(Name = "SellerTaxCode")]
        public virtual string SellerTaxCode { get; set; }

        [XmlElement("SellerBankName")]
        [DataMember(Name = "SellerBankName")]
        public virtual string SellerBankName { get; set; }

        [XmlElement("SellerBankAddress")]
        [DataMember(Name = "SellerBankAddress")]
        public virtual string SellerBankAddress { get; set; }

        [XmlElement("SellerBankNo")]
        [DataMember(Name = "SellerBankNo")]
        public virtual string SellerBankNo { get; set; }

        [XmlElement("SellerBankCode")]
        [DataMember(Name = "SellerBankCode")]
        public virtual string SellerBankCode { get; set; }

        [XmlElement("SellerPhone")]
        [DataMember(Name = "SellerPhone")]
        public virtual string SellerPhone { get; set; }

        [XmlElement("SellerFax")]
        [DataMember(Name = "SellerFax")]
        public virtual string SellerFax { get; set; }

        [XmlElement("ContractNumber")]
        [DataMember(Name = "ContractNumber")]
        public virtual string ContractNumber { get; set; }

        [XmlElement("ContractDate")]
        [DataMember(Name = "ContractDate")]
        public virtual string ContractDate { get; set; }

        [XmlElement("ContNo")]
        [DataMember(Name = "ContNo")]
        public virtual string ContNo { get; set; }

        [XmlElement("ReferenceNo")]
        [DataMember(Name = "ReferenceNo")]
        public virtual string ReferenceNo { get; set; }

        [XmlElement("VehicleNo")]
        [DataMember(Name = "VehicleNo")]
        public virtual string VehicleNo { get; set; }

        [XmlElement("Extra3")]
        [DataMember(Name = "Extra3")]
        public virtual string Extra3 { get; set; }

        [XmlElement("DocNo")]
        [DataMember(Name = "DocNo")]
        public virtual string DocNo { get; set; }

        [XmlElement("DocDate")]
        [DataMember(Name = "DocDate")]
        public virtual string DocDate { get; set; }

        [XmlElement("DocPerson")]
        [DataMember(Name = "DocPerson")]
        public virtual string DocPerson { get; set; }

        [XmlElement("DocWork")]
        [DataMember(Name = "DocWork")]
        public virtual string DocWork { get; set; }

        [XmlElement("DocShipper")]
        [DataMember(Name = "DocShipper")]
        public virtual string DocShipper { get; set; }

        [XmlElement("DocVehicle")]
        [DataMember(Name = "DocVehicle")]
        public virtual string DocVehicle { get; set; }

        [XmlElement("DocExport")]
        [DataMember(Name = "DocExport")]
        public virtual string DocExport { get; set; }

        [XmlElement("DocImport")]
        [DataMember(Name = "DocImport")]
        public virtual string DocImport { get; set; }

        [XmlElement("TotalQuantity")]
        [DataMember(Name = "TotalQuantity")]
        public virtual string TotalQuantity { get; set; }

        //END ADDED FOR SELLERINVOICE

        [XmlElement("CusEmail")]
        [DataMember(Name = "CusEmail")]
        public virtual string CusEmail { get; set; }

        [XmlElement("GrossValueNonTax")]
        [DataMember(Name = "GrossValueNonTax", Order = 37)]
        public override decimal GrossValueNonTax { get; set; }

        [XmlElement("ExchangeRate")]
        [DataMember(Name = "ExchangeRate", Order = 39)]
        public override decimal ExchangeRate { get; set; } // tong tien chuyen doi

        [XmlElement("ConvertedAmount")]
        [DataMember(Name = "ConvertedAmount", Order = 40)]
        public override decimal ConvertedAmount { get; set; } // ti gia chuyen doi

        public override decimal TaxSignaltureDate { get; set; }

        [XmlElement("DiscountAmount")]
        [DataMember(Name = "DiscountAmount")]
        public virtual decimal DiscountAmount { get; set; }

        [XmlElement("DiscountRate")]
        [DataMember(Name = "DiscountRate")]
        public virtual float DiscountRate { get; set; }

        /// <summary>
        /// email cho mỗi hóa đơn
        /// </summary>
        [XmlElement("EmailDeliver")]
        [DataMember(Name = "EmailDeliver", Order = 43)]
        public override string EmailDeliver { get; set; }

        [XmlElement("SMSDeliver")]
        [DataMember(Name = "SMSDeliver", Order = 44)]
        public override string SMSDeliver { get; set; }

        [XmlArray("Products")]
        [DataMember(Name = "Products")]
        public virtual List<ProductInv> ProductList
        {
            get
            {
                return (from pr in Products select pr as ProductInv).ToList();
            }
        }

        public override string SerializeToXML()
        {
            try
            {
                string data = "";
                XmlDocument xmlDoc = new XmlDocument();
                //van de Einvoice
                XmlNode xmlContentInv = null;
                if (this.Type == InvoiceType.ForReplace)
                    xmlContentInv = xmlDoc.CreateElement("ReplaceInv");
                else if (this.Type == InvoiceType.ForAdjustAccrete || this.Type == InvoiceType.ForAdjustInfo || this.Type == InvoiceType.ForAdjustReduce)
                    xmlContentInv = xmlDoc.CreateElement("AdjustInv");
                if (xmlContentInv != null) xmlDoc.AppendChild(xmlContentInv);
                XmlNode xmlkey = xmlDoc.CreateElement("key");
                xmlkey.InnerText = Fkey;
                xmlContentInv.AppendChild(xmlkey);

                //van de ve customer 
                XmlNode xmlCusCode = xmlDoc.CreateElement("CusCode");
                xmlCusCode.InnerText = CusCode;
                xmlContentInv.AppendChild(xmlCusCode);

                XmlNode xmlCusName = xmlDoc.CreateElement("CusName");
                xmlCusName.InnerText = CusName;
                xmlContentInv.AppendChild(xmlCusName);

                XmlNode xmlBuyer = xmlDoc.CreateElement("Buyer");
                xmlBuyer.InnerText = Buyer;
                xmlContentInv.AppendChild(xmlBuyer);

                XmlNode xmlCusAddress = xmlDoc.CreateElement("CusAddress");
                xmlCusAddress.InnerText = CusAddress;
                xmlContentInv.AppendChild(xmlCusAddress);

                XmlNode xmlCusTaxCode = xmlDoc.CreateElement("CusTaxCode");
                xmlCusTaxCode.InnerText = CusTaxCode;
                xmlContentInv.AppendChild(xmlCusTaxCode);

                XmlNode xmlPaymentMethod = xmlDoc.CreateElement("PaymentMethod");
                xmlPaymentMethod.InnerText = PaymentMethod;
                xmlContentInv.AppendChild(xmlPaymentMethod);

                XmlNode xmlKindOfService = xmlDoc.CreateElement("KindOfService");
                xmlKindOfService.InnerText = KindOfService;
                xmlContentInv.AppendChild(xmlKindOfService);

                // START NghiepPV ADDED 20170626 DUOCHAUGIANG
                XmlNode xmlGrossValue0 = xmlDoc.CreateElement("GrossValue0");
                xmlGrossValue0.InnerText = GrossValue0.ToString();
                xmlContentInv.AppendChild(xmlGrossValue0);

                XmlNode xmlGrossValue5 = xmlDoc.CreateElement("GrossValue5");
                xmlGrossValue5.InnerText = GrossValue5.ToString();
                xmlContentInv.AppendChild(xmlGrossValue5);

                XmlNode xmlGrossValue10 = xmlDoc.CreateElement("GrossValue10");
                xmlGrossValue10.InnerText = GrossValue10.ToString();
                xmlContentInv.AppendChild(xmlGrossValue10);

                XmlNode xmlVatAmount0 = xmlDoc.CreateElement("VatAmount0");
                xmlVatAmount0.InnerText = VatAmount0.ToString();
                xmlContentInv.AppendChild(xmlVatAmount0);

                XmlNode xmlVatAmount5 = xmlDoc.CreateElement("VatAmount5");
                xmlVatAmount5.InnerText = VatAmount5.ToString();
                xmlContentInv.AppendChild(xmlVatAmount5);

                XmlNode xmlVatAmount10 = xmlDoc.CreateElement("VatAmount10");
                xmlVatAmount10.InnerText = VatAmount10.ToString();
                xmlContentInv.AppendChild(xmlVatAmount10);
                // END NghiepPV ADDED 20170626 DUOCHAUGIANG

                XmlNode xmlGrossValueNonTax = xmlDoc.CreateElement("GrossValueNonTax");
                xmlGrossValueNonTax.InnerText = GrossValueNonTax.ToString();
                xmlContentInv.AppendChild(xmlGrossValueNonTax);

                //START ADDED FOR SELLERINVOICE

                XmlNode xmlDeliveryAddress = xmlDoc.CreateElement("DeliveryAddress");
                xmlDeliveryAddress.InnerText = DeliveryAddress;
                xmlContentInv.AppendChild(xmlDeliveryAddress);

                XmlNode xmlPaymentTerm = xmlDoc.CreateElement("PaymentTerm");
                xmlPaymentTerm.InnerText = PaymentTerm;
                xmlContentInv.AppendChild(xmlPaymentTerm);

                XmlNode xmlSellerName = xmlDoc.CreateElement("SellerName");
                xmlSellerName.InnerText = SellerName;
                xmlContentInv.AppendChild(xmlSellerName);

                XmlNode xmlSellerAddress = xmlDoc.CreateElement("SellerAddress");
                xmlSellerAddress.InnerText = SellerAddress;
                xmlContentInv.AppendChild(xmlSellerAddress);

                XmlNode xmlSellerTaxCode = xmlDoc.CreateElement("SellerTaxCode");
                xmlSellerTaxCode.InnerText = SellerTaxCode;
                xmlContentInv.AppendChild(xmlSellerTaxCode);

                XmlNode xmlSellerBankName = xmlDoc.CreateElement("SellerBankName");
                xmlSellerBankName.InnerText = SellerBankName;
                xmlContentInv.AppendChild(xmlSellerBankName);

                XmlNode xmlSellerBankAddress = xmlDoc.CreateElement("SellerBankAddress");
                xmlSellerBankAddress.InnerText = SellerBankAddress;
                xmlContentInv.AppendChild(xmlSellerBankAddress);

                XmlNode xmlSellerBankNo = xmlDoc.CreateElement("SellerBankNo");
                xmlSellerBankNo.InnerText = SellerBankNo;
                xmlContentInv.AppendChild(xmlSellerBankNo);

                XmlNode xmlSellerBankCode = xmlDoc.CreateElement("SellerBankCode");
                xmlSellerBankCode.InnerText = SellerBankCode;
                xmlContentInv.AppendChild(xmlSellerBankCode);

                XmlNode xmlSellerPhone = xmlDoc.CreateElement("SellerPhone");
                xmlSellerPhone.InnerText = SellerPhone;
                xmlContentInv.AppendChild(xmlSellerPhone);

                XmlNode xmlSellerFax = xmlDoc.CreateElement("SellerFax");
                xmlSellerFax.InnerText = SellerFax;
                xmlContentInv.AppendChild(xmlSellerFax);

                XmlNode xmlContractNumber = xmlDoc.CreateElement("ContractNumber");
                xmlContractNumber.InnerText = ContractNumber;
                xmlContentInv.AppendChild(xmlContractNumber);

                XmlNode xmlContractDate = xmlDoc.CreateElement("ContractDate");
                xmlContractDate.InnerText = ContractDate;
                xmlContentInv.AppendChild(xmlContractDate);

                XmlNode xmlContNo = xmlDoc.CreateElement("ContNo");
                xmlContNo.InnerText = ContNo;
                xmlContentInv.AppendChild(xmlContNo);

                XmlNode xmlReferenceNo = xmlDoc.CreateElement("ReferenceNo");
                xmlReferenceNo.InnerText = ReferenceNo;
                xmlContentInv.AppendChild(xmlReferenceNo);

                XmlNode xmlVehicleNo = xmlDoc.CreateElement("VehicleNo");
                xmlVehicleNo.InnerText = VehicleNo;
                xmlContentInv.AppendChild(xmlVehicleNo);

                XmlNode xmlDiscountAmount = xmlDoc.CreateElement("DiscountAmount");
                xmlDiscountAmount.InnerText = DiscountAmount.ToString().Replace(",", ".");
                xmlContentInv.AppendChild(xmlDiscountAmount);

                XmlNode xmlDiscountRate = xmlDoc.CreateElement("DiscountRate");
                xmlDiscountRate.InnerText = DiscountRate.ToString().Replace(",", ".");
                xmlContentInv.AppendChild(xmlDiscountRate);

                XmlNode xmlExchangeRate = xmlDoc.CreateElement("ExchangeRate");
                xmlExchangeRate.InnerText = ExchangeRate.ToString().Replace(",", ".");
                xmlContentInv.AppendChild(xmlExchangeRate);

                XmlNode xmlExtra1 = xmlDoc.CreateElement("Extra1");
                xmlExtra1.InnerText = Extra1;
                xmlContentInv.AppendChild(xmlExtra1);

                XmlNode xmlExtra2 = xmlDoc.CreateElement("Extra2");
                xmlExtra2.InnerText = Extra2;
                xmlContentInv.AppendChild(xmlExtra2);

                XmlNode xmlExtra3 = xmlDoc.CreateElement("Extra3");
                xmlExtra3.InnerText = Extra3;
                xmlContentInv.AppendChild(xmlExtra3);

                XmlNode xmlDocNo = xmlDoc.CreateElement("DocNo");
                xmlDocNo.InnerText = DocNo;
                xmlContentInv.AppendChild(xmlDocNo);

                XmlNode xmlDocDate = xmlDoc.CreateElement("DocDate");
                xmlDocDate.InnerText = DocDate;
                xmlContentInv.AppendChild(xmlDocDate);

                XmlNode xmlDocPerson = xmlDoc.CreateElement("DocPerson");
                xmlDocPerson.InnerText = DocPerson;
                xmlContentInv.AppendChild(xmlDocPerson);

                XmlNode xmlDocWork = xmlDoc.CreateElement("DocWork");
                xmlDocWork.InnerText = DocWork;
                xmlContentInv.AppendChild(xmlDocWork);

                XmlNode xmlDocShipper = xmlDoc.CreateElement("DocShipper");
                xmlDocShipper.InnerText = DocShipper;
                xmlContentInv.AppendChild(xmlDocShipper);

                XmlNode xmlDocVehicle = xmlDoc.CreateElement("DocVehicle");
                xmlDocVehicle.InnerText = DocVehicle;
                xmlContentInv.AppendChild(xmlDocVehicle);

                XmlNode xmlDocExport = xmlDoc.CreateElement("DocExport");
                xmlDocExport.InnerText = DocExport;
                xmlContentInv.AppendChild(xmlDocExport);

                XmlNode xmlDocImport = xmlDoc.CreateElement("DocImport");
                xmlDocImport.InnerText = DocImport;
                xmlContentInv.AppendChild(xmlDocImport);

                XmlNode xmlTotalQuantity = xmlDoc.CreateElement("TotalQuantity");
                xmlTotalQuantity.InnerText = TotalQuantity;
                xmlContentInv.AppendChild(xmlTotalQuantity);

                //END ADDED FOR SELLERINVOICE

                XmlNode xmlCusEmail = xmlDoc.CreateElement("CusEmail");
                xmlCusEmail.InnerText = CusEmail;
                xmlContentInv.AppendChild(xmlCusEmail);

                //product
                //van de ve san pham
                XmlNode xmlProducts = xmlDoc.CreateElement("Products");
                xmlContentInv.AppendChild(xmlProducts);
                //khoi tao du lieu da co
                //Insert tung phan tu vao product
                foreach (var el in ProductList)
                {
                    XmlNode xmlProduct = xmlDoc.CreateElement("Product");
                    xmlProducts.AppendChild(xmlProduct);

                    //begin product
                    XmlNode xmlProdCode = xmlDoc.CreateElement("Code");
                    xmlProdCode.InnerText = el.Code;
                    xmlProduct.AppendChild(xmlProdCode);

                    XmlNode xmlProdName = xmlDoc.CreateElement("ProdName");
                    xmlProdName.InnerText = el.Name;
                    xmlProduct.AppendChild(xmlProdName);

                    XmlNode xmlProdUnit = xmlDoc.CreateElement("ProdUnit");
                    xmlProdUnit.InnerText = el.Unit;
                    xmlProduct.AppendChild(xmlProdUnit);

                    XmlNode xmlProdQuantity = xmlDoc.CreateElement("ProdQuantity");
                    xmlProdQuantity.InnerText = el.Quantity.ToString();
                    xmlProduct.AppendChild(xmlProdQuantity);

                    XmlNode xmlProdPrice = xmlDoc.CreateElement("ProdPrice");
                    xmlProdPrice.InnerText = el.Price.ToString();
                    xmlProduct.AppendChild(xmlProdPrice);

                    XmlNode xmlProdRemark = xmlDoc.CreateElement("Remark");
                    xmlProdRemark.InnerText = el.Remark.ToString();
                    xmlProduct.AppendChild(xmlProdRemark);

                    XmlNode xmlProdAmount = xmlDoc.CreateElement("Amount");
                    xmlProdAmount.InnerText = el.Amount.ToString();
                    xmlProduct.AppendChild(xmlProdAmount);

                    XmlNode xmlProdTotal = xmlDoc.CreateElement("Total");
                    xmlProdTotal.InnerText = el.Total.ToString();
                    xmlProduct.AppendChild(xmlProdTotal);
                }
                //end product
                //other
                //tong tien truoc thue
                XmlNode xmlGrossValue = xmlDoc.CreateElement("GrossValue");
                xmlGrossValue.InnerText = GrossValue.ToString();
                xmlContentInv.AppendChild(xmlGrossValue);

                //tong tien dich vu
                XmlNode xmlTotal = xmlDoc.CreateElement("Total");
                xmlTotal.InnerText = Total.ToString();
                xmlContentInv.AppendChild(xmlTotal);

                //thue gia tri gia tang  VAT_Rate
                XmlNode xmlVAT_Rate = xmlDoc.CreateElement("VATRate");
                xmlVAT_Rate.InnerText = VATRate.ToString();
                xmlContentInv.AppendChild(xmlVAT_Rate);

                // tong tien VAT_Amount
                XmlNode xmlVAT_Amount = xmlDoc.CreateElement("VATAmount");
                xmlVAT_Amount.InnerText = VATAmount.ToString();
                xmlContentInv.AppendChild(xmlVAT_Amount);

                //Amount
                XmlNode xmlamount = xmlDoc.CreateElement("Amount");
                xmlamount.InnerText = Amount.ToString();
                xmlContentInv.AppendChild(xmlamount);

                //hien thi so tien bang chu
                XmlNode xmlAmountInWords = xmlDoc.CreateElement("AmountInWords");
                xmlAmountInWords.InnerText = AmountInWords;
                xmlContentInv.AppendChild(xmlAmountInWords);

                //truong extra mở rộng
                XmlNode xmlExtra = xmlDoc.CreateElement("Extra");
                xmlExtra.InnerText = Extra;
                xmlContentInv.AppendChild(xmlExtra);

                ////kieu hoa don (chi dung voi hoa don dieu chinh)
                //if (this.Type == InvoiceType.ForAdjustAccrete || this.Type == InvoiceType.ForAdjustInfo || this.Type == InvoiceType.ForAdjustReduce)
                //{
                //    XmlNode xmlType = xmlDoc.CreateElement("Type");
                //    xmlType.InnerText = Convert.ToInt32(Type).ToString();
                //    xmlContentInv.AppendChild(xmlType);
                //}
                XmlNode xmlType = xmlDoc.CreateElement("Type");
                xmlType.InnerText = Convert.ToInt32(Type).ToString();
                xmlContentInv.AppendChild(xmlType);

                //ngay phat hanh hoa don
                XmlNode xmlArisingDate = xmlDoc.CreateElement("ArisingDate");
                xmlArisingDate.InnerText = ArisingDate.ToString("dd/MM/yyyy");
                xmlContentInv.AppendChild(xmlArisingDate);

                //end other
                //End sanpham
                return xmlDoc.OuterXml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private ProductInv deserializeProduct(XElement el)
        {
            ProductInv p = new ProductInv();
            if (el.Element("Code") != null) p.Code = el.Element("Code").Value;
            if (el.Element("Remark") != null) p.Remark = el.Element("Remark").Value;
            if (el.Element("ProdName") != null) p.Name = el.Element("ProdName").Value;
            if (el.Element("ProdUnit") != null) p.Unit = el.Element("ProdUnit").Value;
            if (el.Element("ProdQuantity") != null && !string.IsNullOrWhiteSpace(el.Element("ProdQuantity").Value)) p.Quantity = Convert.ToDecimal(el.Element("ProdQuantity").Value);
            if (el.Element("ProdPrice") != null && !string.IsNullOrWhiteSpace(el.Element("ProdPrice").Value)) p.Price = Convert.ToDecimal(el.Element("ProdPrice").Value);
            if (el.Element("Total") != null && !string.IsNullOrWhiteSpace(el.Element("Total").Value)) p.Total = Convert.ToDecimal(el.Element("Total").Value);
            if (el.Element("VATRate") != null && !string.IsNullOrWhiteSpace(el.Element("VATRate").Value)) p.VATRate = Convert.ToInt32(el.Element("VATRate").Value);
            if (el.Element("VATAmount") != null && !string.IsNullOrWhiteSpace(el.Element("VATAmount").Value)) p.VATAmount = Convert.ToDecimal(el.Element("VATAmount").Value);
            if (el.Element("Amount") != null && !string.IsNullOrWhiteSpace(el.Element("Amount").Value)) p.Amount = Convert.ToDecimal(el.Element("Amount").Value);
            if (el.Element("Discount") != null && !string.IsNullOrWhiteSpace(el.Element("Discount").Value)) p.Discount = Convert.ToInt32(el.Element("Discount").Value);
            if (el.Element("DiscountAmount") != null && !string.IsNullOrWhiteSpace(el.Element("DiscountAmount").Value)) p.DiscountAmount = Convert.ToDecimal(el.Element("DiscountAmount").Value);
            if (el.Element("Extra1") != null && !string.IsNullOrWhiteSpace(el.Element("Extra1").Value))
            {
                p.Extra1 = el.Element("Extra1").Value;
            }
            if (el.Element("Extra2") != null && !string.IsNullOrWhiteSpace(el.Element("Extra2").Value))
            {
                p.Extra2 = el.Element("Extra2").Value;
            }
            p.ProdType = 1;

            return p;
        }

        public override void DeserializeFromXML(string XML)
        {
            Company _CurrentCom = ((EInvoiceContext)FXContext.Current).CurrentCompany;
            ComID = _CurrentCom.id;
            if (ComName == null)
            {
                ComName = _CurrentCom.Name;
                ComAddress = _CurrentCom.Address;
                ComBankName = _CurrentCom.BankName;
                ComBankNo = _CurrentCom.BankNumber;
                ComTaxCode = _CurrentCom.TaxCode;
                ComPhone = _CurrentCom.Phone;
                ComFax = _CurrentCom.Fax;
            }
            XElement elem = XElement.Parse(XML);
            if (elem.Element("ComName") != null)
                ComName = elem.Element("ComName").Value;
            if (elem.Element("ComAddress") != null)
                ComAddress = elem.Element("ComAddress").Value;
            if (elem.Element("ComPhone") != null)
                ComPhone = elem.Element("ComPhone").Value;
            if (elem.Element("ComBankName") != null)
                ComBankName = elem.Element("ComBankName").Value;
            if (elem.Element("ComBankNo") != null)
                ComBankNo = elem.Element("ComBankNo").Value;
            if (elem.Element("ComTaxCode") != null)
                ComTaxCode = elem.Element("ComTaxCode").Value;
            if (elem.Element("ComFax") != null)
                ComFax = elem.Element("ComFax").Value;
            if (elem.Element("Note") != null)
                Note = elem.Element("Note").Value;
            if (elem.Element("CusName") != null)
                CusName = elem.Element("CusName").Value;
            if (elem.Element("Buyer") != null)
                Buyer = elem.Element("Buyer").Value;
            if (elem.Element("CusAddress") != null)
                CusAddress = elem.Element("CusAddress").Value;
            if (elem.Element("CusCode") != null)
                CusCode = elem.Element("CusCode").Value;
            if (elem.Element("CusTaxCode") != null)
                CusTaxCode = elem.Element("CusTaxCode").Value;
            if (elem.Element("CusBankName") != null)
                CusBankName = elem.Element("CusBankName").Value;
            if (elem.Element("CusBankNo") != null)
                CusBankNo = elem.Element("CusBankNo").Value;
            if (elem.Element("CusPhone") != null)
                CusPhone = elem.Element("CusPhone").Value;

            if (elem.Element("PaymentMethod") != null)
                PaymentMethod = elem.Element("PaymentMethod").Value;
            if (elem.Element("KindOfService") != null)
                KindOfService = elem.Element("KindOfService").Value;
            if (elem.Element("AmountInWords") != null)
                AmountInWords = elem.Element("AmountInWords").Value;
            if (elem.Element("Name") != null)
                Name = elem.Element("Name").Value;
            if (elem.Element("Serial") != null)
                Serial = elem.Element("Serial").Value;
            if (elem.Element("Pattern") != null)
                Pattern = elem.Element("Pattern").Value;
            if (elem.Element("CreateDate") != null)
                CreateDate = DateTime.Parse(elem.Element("CreateDate").Value);
            if (elem.Element("PublishDate") != null)
                PublishDate = DateTime.Parse(elem.Element("PublishDate").Value);
            if (elem.Element("VATRate") != null && !string.IsNullOrWhiteSpace(elem.Element("VATRate").Value))
                VATRate = Convert.ToInt32(elem.Element("VATRate").Value);
            if (elem.Element("VATAmount") != null && !string.IsNullOrWhiteSpace(elem.Element("VATAmount").Value))
                VATAmount = Convert.ToDecimal(elem.Element("VATAmount").Value);
            if (elem.Element("Amount") != null && !string.IsNullOrWhiteSpace(elem.Element("Amount").Value))
                Amount = Convert.ToDecimal(elem.Element("Amount").Value);
            if (elem.Element("Total") != null && !string.IsNullOrWhiteSpace(elem.Element("Total").Value))
                Total = Convert.ToDecimal(elem.Element("Total").Value);
            if (elem.Element("Extra") != null)
                Extra = elem.Element("Extra").Value;
            if (elem.Element("ResourceCode") != null && !string.IsNullOrWhiteSpace(elem.Element("ResourceCode").Value))
                ResourceCode = elem.Element("ResourceCode").Value;
            if (elem.Element("PaymentStatus") != null && !string.IsNullOrWhiteSpace(elem.Element("PaymentStatus").Value))
                PaymentStatus = (Payment)Convert.ToInt32(elem.Element("PaymentStatus").Value);
            if (elem.Element("ArisingDate") != null && !string.IsNullOrWhiteSpace(elem.Element("ArisingDate").Value))
                ArisingDate = DateTime.ParseExact(elem.Element("ArisingDate").Value, "dd/MM/yyyy", new CultureInfo("en-US"));

            if (elem.Element("GrossValue") != null && !string.IsNullOrWhiteSpace(elem.Element("GrossValue").Value))
                GrossValue = Convert.ToDecimal(elem.Element("GrossValue").Value);

            if (elem.Element("EmailDeliver") != null)
                EmailDeliver = elem.Element("EmailDeliver").Value;

            if (elem.Element("key") != null)
                Fkey = elem.Element("key").Value;
            // START NghiepPV ADDED 20170626 DUOCHAUGIANG
            if (elem.Element("GrossValue0") != null && !string.IsNullOrWhiteSpace(elem.Element("GrossValue0").Value))
            {
                GrossValue0 = Convert.ToDecimal(elem.Element("GrossValue0").Value);
            }
            if (elem.Element("GrossValue5") != null && !string.IsNullOrWhiteSpace(elem.Element("GrossValue5").Value))
            {
                GrossValue5 = Convert.ToDecimal(elem.Element("GrossValue5").Value);
            }
            if (elem.Element("GrossValue10") != null && !string.IsNullOrWhiteSpace(elem.Element("GrossValue10").Value))
            {
                GrossValue10 = Convert.ToDecimal(elem.Element("GrossValue10").Value);
            }
            if (elem.Element("VatAmount0") != null && !string.IsNullOrWhiteSpace(elem.Element("VatAmount0").Value))
            {
                VatAmount0 = Convert.ToDecimal(elem.Element("VatAmount0").Value);
            }
            if (elem.Element("VatAmount5") != null && !string.IsNullOrWhiteSpace(elem.Element("VatAmount5").Value))
            {
                VatAmount5 = Convert.ToDecimal(elem.Element("VatAmount5").Value);
            }
            if (elem.Element("VatAmount10") != null && !string.IsNullOrWhiteSpace(elem.Element("VatAmount10").Value))
            {
                VatAmount10 = Convert.ToDecimal(elem.Element("VatAmount10").Value);
            }
            // END NghiepPV ADDED 20170626 DUOCHAUGIANG

            if (elem.Element("GrossValueNonTax") != null && !string.IsNullOrWhiteSpace(elem.Element("GrossValueNonTax").Value))
            {
                GrossValueNonTax = Convert.ToDecimal(elem.Element("GrossValueNonTax").Value);
            }

            // START ADDED FOR SELLERINVOICE
            if (elem.Element("DeliveryAddress") != null)
            {
                DeliveryAddress = elem.Element("DeliveryAddress").Value;
            }

            if (elem.Element("PaymentTerm") != null)
            {
                PaymentTerm = elem.Element("PaymentTerm").Value;
            }

            if (elem.Element("SellerName") != null)
            {
                SellerName = elem.Element("SellerName").Value;
            }

            if (elem.Element("SellerAddress") != null)
            {
                SellerAddress = elem.Element("SellerAddress").Value;
            }

            if (elem.Element("SellerTaxCode") != null)
            {
                SellerTaxCode = elem.Element("SellerTaxCode").Value;
            }

            if (elem.Element("SellerBankName") != null)
            {
                SellerBankName = elem.Element("SellerBankName").Value;
            }

            if (elem.Element("SellerBankAddress") != null)
            {
                SellerBankAddress = elem.Element("SellerBankAddress").Value;
            }

            if (elem.Element("SellerBankNo") != null)
            {
                SellerBankNo = elem.Element("SellerBankNo").Value;
            }

            if (elem.Element("SellerBankCode") != null)
            {
                SellerBankCode = elem.Element("SellerBankCode").Value;
            }

            if (elem.Element("SellerPhone") != null)
            {
                SellerPhone = elem.Element("SellerPhone").Value;
            }

            if (elem.Element("SellerFax") != null)
            {
                SellerFax = elem.Element("SellerFax").Value;
            }

            if (elem.Element("ContractNumber") != null)
            {
                ContractNumber = elem.Element("ContractNumber").Value;
            }

            if (elem.Element("ContractDate") != null)
            {
                ContractDate = elem.Element("ContractDate").Value;
            }

            if (elem.Element("ContNo") != null)
            {
                ContNo = elem.Element("ContNo").Value;
            }

            if (elem.Element("ReferenceNo") != null)
            {
                ReferenceNo = elem.Element("ReferenceNo").Value;
            }

            if (elem.Element("VehicleNo") != null)
            {
                VehicleNo = elem.Element("VehicleNo").Value;
            }

            if (elem.Element("DiscountAmount") != null && !string.IsNullOrWhiteSpace(elem.Element("DiscountAmount").Value))
            {
                DiscountAmount = Convert.ToDecimal(elem.Element("DiscountAmount").Value);
            }

            if (elem.Element("DiscountRate") != null && !string.IsNullOrWhiteSpace(elem.Element("DiscountRate").Value))
            {
                DiscountRate = Convert.ToInt32(elem.Element("DiscountRate").Value);
            }

            if (elem.Element("ExchangeRate") != null)
            {
                ExchangeRate = Convert.ToDecimal(elem.Element("ExchangeRate").Value);
            }

            if (elem.Element("Extra1") != null)
            {
                Extra1 = elem.Element("Extra1").Value;
            }

            if (elem.Element("Extra2") != null)
            {
                Extra2 = elem.Element("Extra2").Value;
            }

            if (elem.Element("Extra3") != null)
            {
                Extra3 = elem.Element("Extra3").Value;
            }

            if (elem.Element("DocNo") != null)
            {
                DocNo = elem.Element("DocNo").Value;
            }

            if (elem.Element("DocDate") != null)
            {
                DocDate = elem.Element("DocDate").Value;
            }

            if (elem.Element("DocPerson") != null)
            {
                DocPerson = elem.Element("DocPerson").Value;
            }

            if (elem.Element("DocWork") != null)
            {
                DocWork = elem.Element("DocWork").Value;
            }

            if (elem.Element("DocShipper") != null)
            {
                DocShipper = elem.Element("DocShipper").Value;
            }

            if (elem.Element("DocVehicle") != null)
            {
                DocVehicle = elem.Element("DocVehicle").Value;
            }

            if (elem.Element("DocExport") != null)
            {
                DocExport = elem.Element("DocExport").Value;
            }

            if (elem.Element("DocImport") != null)
            {
                DocImport = elem.Element("DocImport").Value;
            }

            if (elem.Element("TotalQuantity") != null)
            {
                TotalQuantity = elem.Element("TotalQuantity").Value;
            }
            // END ADDED FOR SELLERINVOICE

            if (elem.Element("CusEmail") != null)
            {
                CusEmail = elem.Element("CusEmail").Value;
            }

            XElement XeleProd = elem.Element("Products");
            IEnumerable<XElement> plist = (from c in XeleProd.Elements("Product") select c).ToList<XElement>();
            IEnumerable<XElement> list = (from c in XeleProd.Elements("Product").Elements("id") select c).ToList<XElement>();
            string n = string.Concat(list.Nodes());
            List<IProductInv> lstp = (from p in Products where n.Contains(p.id.ToString()) select p).ToList();
            List<IProductInv> delstp = (from p in Products where (!n.Contains(p.id.ToString())) select p).ToList();

            if (elem.Element("id") == null)
            {
                foreach (XElement el in plist)
                {
                    ProductInv p = deserializeProduct(el);
                    this.Products.Add(p);
                }
            }
            else
            {
                foreach (XElement el in plist)
                {
                    if (el.Element("id") == null)
                    {
                        //them moi ProductInv
                        ProductInv p = deserializeProduct(el);
                        this.Products.Add(p);
                    }
                    else
                    {
                        foreach (ProductInv p in delstp)
                            Products.Remove(p);
                        //Sua ProductInv
                        ProductInv pNew = deserializeProduct(el);
                        for (int i = 0; i < lstp.Count; i++)
                        {
                            lstp[i] = pNew;
                        }
                    }
                }
            }
        }

        public override string GetXMLData()
        {
            if (String.IsNullOrEmpty(Fkey))
                Fkey = ComID.ToString() + Pattern.Replace("/", "") + Serial.Replace("/", "") + No.ToString();
            string strXML = GetStringXML();
            XmlDocument xdoc = new XmlDocument();
            xdoc.PreserveWhitespace = true;
            xdoc.LoadXml(strXML);
            XmlProcessingInstruction newPI;
            string tempName = InvServiceFactory.GetTempName(Pattern, ComID);
            String PItext = "type='text/xsl' href='";
            Company currentCom = ((EInvoiceContext)FXContext.Current).CurrentCompany;
            if (currentCom.Config.Keys.Contains("PublishDomain"))
                PItext += currentCom.Config["PublishDomain"] + "/InvoiceTemplate/GetXSLTbyTempName/" + tempName + "' ";
            else
                PItext += ConfigurationManager.AppSettings["PublishDomain"] + "/InvoiceTemplate/GetXSLTbyTempName/" + tempName + "' ";

            newPI = xdoc.CreateProcessingInstruction("xml-stylesheet", PItext);
            xdoc.InsertBefore(newPI, xdoc.DocumentElement);

            XDocument xd = Utils.GetXDocument(xdoc);
            Utils.RemoveAttribte(xd);
            Utils.RemoveNamespace(xd);

            xdoc = Utils.GetXmlDocument(xd);
            XmlNodeList xlist = xdoc.DocumentElement.ChildNodes;
            XmlNode newnode = xdoc.DocumentElement.AppendChild(xdoc.CreateElement("Content"));
            XmlAttribute xa1 = xdoc.CreateAttribute("Id");
            xa1.Value = "SigningData";
            newnode.Attributes.Append(xa1);
            for (int i = 0; i < xlist.Count - 1; i++)
            {
                newnode.AppendChild(xlist[i]);
                i--;
            }

            xdoc.DocumentElement.RemoveAll();
            xdoc.DocumentElement.AppendChild(newnode);

            if (this.Type == InvoiceType.ForReplace)
            {
                XmlNode xn = xdoc.GetElementsByTagName("Content")[0].AppendChild(xdoc.CreateElement("isReplace"));
                xn.InnerText = this.ProcessInvNote;
            }

            else if (this.Type == InvoiceType.ForAdjustAccrete || this.Type == InvoiceType.ForAdjustInfo || this.Type == InvoiceType.ForAdjustReduce)
            {
                XmlNode xn = xdoc.GetElementsByTagName("Content")[0].AppendChild(xdoc.CreateElement("isAdjust"));
                xn.InnerText = this.ProcessInvNote;
            }
            return xdoc.OuterXml;
        }

        private string GetStringXML()
        {
            XmlDocument xmlDoc = new XmlDocument();

            try
            {
                string data = "";
                //XmlNode nVersion = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
                //xmlDoc.AppendChild(nVersion);
                //van de Einvoice
                XmlNode xmlContentInv = xmlDoc.CreateElement("Invoice");
                if (xmlContentInv != null)
                {
                    //Ten HD
                    XmlNode xmlInvoiceName = xmlDoc.CreateElement("InvoiceName");
                    xmlInvoiceName.InnerText = Name;
                    xmlContentInv.AppendChild(xmlInvoiceName);

                    //Pattern
                    XmlNode xmlInvoicePattern = xmlDoc.CreateElement("InvoicePattern");
                    xmlInvoicePattern.InnerText = Pattern;
                    xmlContentInv.AppendChild(xmlInvoicePattern);

                    //Serial
                    XmlNode xmlSerialNo = xmlDoc.CreateElement("SerialNo");
                    xmlSerialNo.InnerText = Serial;
                    xmlContentInv.AppendChild(xmlSerialNo);

                    //No
                    XmlNode xmlInvoiceNo = xmlDoc.CreateElement("InvoiceNo");
                    xmlInvoiceNo.InnerText = No.ToString();
                    xmlContentInv.AppendChild(xmlInvoiceNo);

                    //ngay phat hanh hoa don
                    XmlNode xmlArisingDate = xmlDoc.CreateElement("ArisingDate");
                    xmlArisingDate.InnerText = ArisingDate.ToString("dd/MM/yyyy");
                    xmlContentInv.AppendChild(xmlArisingDate);

                    //PaymentMethod                
                    XmlNode xmlKind_of_Payment = xmlDoc.CreateElement("Kind_of_Payment");
                    xmlKind_of_Payment.InnerText = PaymentMethod;
                    xmlContentInv.AppendChild(xmlKind_of_Payment);

                    //ComName
                    XmlNode xmlComName = xmlDoc.CreateElement("ComName");
                    xmlComName.InnerText = ComName;
                    xmlContentInv.AppendChild(xmlComName);

                    //ComTaxCode
                    XmlNode xmlComTaxCode = xmlDoc.CreateElement("ComTaxCode");
                    xmlComTaxCode.InnerText = ComTaxCode;
                    xmlContentInv.AppendChild(xmlComTaxCode);

                    //ComAddress
                    XmlNode xmlComAddress = xmlDoc.CreateElement("ComAddress");
                    xmlComAddress.InnerText = ComAddress;
                    xmlContentInv.AppendChild(xmlComAddress);

                    //ComPhone
                    XmlNode xmlComPhone = xmlDoc.CreateElement("ComPhone");
                    xmlComPhone.InnerText = ComPhone;
                    xmlContentInv.AppendChild(xmlComPhone);

                    //ComBankNo
                    XmlNode xmlComBankNo = xmlDoc.CreateElement("ComBankNo");
                    xmlComBankNo.InnerText = ComBankNo;
                    xmlContentInv.AppendChild(xmlComBankNo);

                    //ComBankName
                    XmlNode xmlComBankName = xmlDoc.CreateElement("ComBankName");
                    xmlComBankName.InnerText = ComBankName;
                    xmlContentInv.AppendChild(xmlComBankName);

                    //CusCode
                    XmlNode xmlCusCode = xmlDoc.CreateElement("CusCode");
                    xmlCusCode.InnerText = CusCode;
                    xmlContentInv.AppendChild(xmlCusCode);

                    //CusName
                    XmlNode xmlCusName = xmlDoc.CreateElement("CusName");
                    xmlCusName.InnerText = CusName;
                    xmlContentInv.AppendChild(xmlCusName);

                    //Buyer
                    XmlNode xmlBuyer = xmlDoc.CreateElement("Buyer");
                    xmlBuyer.InnerText = Buyer;
                    xmlContentInv.AppendChild(xmlBuyer);

                    //CusTaxCode
                    XmlNode xmlCusTaxCode = xmlDoc.CreateElement("CusTaxCode");
                    xmlCusTaxCode.InnerText = CusTaxCode;
                    xmlContentInv.AppendChild(xmlCusTaxCode);

                    //CusPhone
                    XmlNode xmlCusPhone = xmlDoc.CreateElement("CusPhone");
                    xmlCusPhone.InnerText = CusPhone;
                    xmlContentInv.AppendChild(xmlCusPhone);

                    //CusAddress
                    XmlNode xmlCusAddress = xmlDoc.CreateElement("CusAddress");
                    xmlCusAddress.InnerText = CusAddress;
                    xmlContentInv.AppendChild(xmlCusAddress);

                    //CusBankName
                    XmlNode xmlCusBankName = xmlDoc.CreateElement("CusBankName");
                    xmlCusBankName.InnerText = CusBankName;
                    xmlContentInv.AppendChild(xmlCusBankName);

                    //CusBankNo
                    XmlNode xmlCusBankNo = xmlDoc.CreateElement("CusBankNo");
                    xmlCusBankNo.InnerText = CusBankNo;
                    xmlContentInv.AppendChild(xmlCusBankNo);

                    //Total
                    XmlNode xmlTotal = xmlDoc.CreateElement("Total");
                    xmlTotal.InnerText = Total.ToString().Replace(",", ".");
                    xmlContentInv.AppendChild(xmlTotal);

                    //Amount
                    XmlNode xmlAmount = xmlDoc.CreateElement("Amount");
                    xmlAmount.InnerText = Amount.ToString().Replace(",", ".");
                    xmlContentInv.AppendChild(xmlAmount);

                    //Amount_words
                    XmlNode xmlAmountInWords = xmlDoc.CreateElement("Amount_words");
                    xmlAmountInWords.InnerText = AmountInWords;
                    xmlContentInv.AppendChild(xmlAmountInWords);

                    //ComFax
                    XmlNode xmlComFax = xmlDoc.CreateElement("ComFax");
                    xmlComFax.InnerText = ComFax;
                    xmlContentInv.AppendChild(xmlComFax);

                    //KindOfService
                    XmlNode xmlKindOfService = xmlDoc.CreateElement("KindOfService");
                    xmlKindOfService.InnerText = KindOfService;
                    xmlContentInv.AppendChild(xmlKindOfService);

                    //VAT_Amount
                    XmlNode xmlVAT_Amount = xmlDoc.CreateElement("VAT_Amount");
                    xmlVAT_Amount.InnerText = VATAmount.ToString().Replace(",", ".");
                    xmlContentInv.AppendChild(xmlVAT_Amount);

                    //VAT_Rate
                    XmlNode xmlVAT_Rate = xmlDoc.CreateElement("VAT_Rate");
                    xmlVAT_Rate.InnerText = VATRate.ToString();
                    xmlContentInv.AppendChild(xmlVAT_Rate);

                    //GrossValue                
                    XmlNode xmlGrossValue = xmlDoc.CreateElement("GrossValue");
                    xmlGrossValue.InnerText = GrossValue.ToString().Replace(",", ".");
                    xmlContentInv.AppendChild(xmlGrossValue);

                    //Extra
                    XmlNode xmlExtra = xmlDoc.CreateElement("Extra");
                    xmlExtra.InnerText = Extra;
                    xmlContentInv.AppendChild(xmlExtra);

                    // START NghiepPV ADDED 20170626 DUOCHAUGIANG
                    XmlNode xmlGrossValue0 = xmlDoc.CreateElement("GrossValue0");
                    xmlGrossValue0.InnerText = GrossValue0.ToString().Replace(",", ".");
                    xmlContentInv.AppendChild(xmlGrossValue0);

                    XmlNode xmlGrossValue5 = xmlDoc.CreateElement("GrossValue5");
                    xmlGrossValue5.InnerText = GrossValue5.ToString().Replace(",", ".");
                    xmlContentInv.AppendChild(xmlGrossValue5);

                    XmlNode xmlGrossValue10 = xmlDoc.CreateElement("GrossValue10");
                    xmlGrossValue10.InnerText = GrossValue10.ToString().Replace(",", ".");
                    xmlContentInv.AppendChild(xmlGrossValue10);

                    XmlNode xmlVatAmount0 = xmlDoc.CreateElement("VatAmount0");
                    xmlVatAmount0.InnerText = VatAmount0.ToString().Replace(",", ".");
                    xmlContentInv.AppendChild(xmlVatAmount0);

                    XmlNode xmlVatAmount5 = xmlDoc.CreateElement("VatAmount5");
                    xmlVatAmount5.InnerText = VatAmount5.ToString().Replace(",", ".");
                    xmlContentInv.AppendChild(xmlVatAmount5);

                    XmlNode xmlVatAmount10 = xmlDoc.CreateElement("VatAmount10");
                    xmlVatAmount10.InnerText = VatAmount10.ToString().Replace(",", ".");
                    xmlContentInv.AppendChild(xmlVatAmount10);
                    // END NghiepPV ADDED 20170626 DUOCHAUGIANG 

                    XmlNode xmlGrossValueNonTax = xmlDoc.CreateElement("GrossValueNonTax");
                    xmlGrossValueNonTax.InnerText = GrossValueNonTax.ToString().Replace(",", ".");
                    xmlContentInv.AppendChild(xmlGrossValueNonTax);

                    //START ADDED FOR SELLERINVOICE
                    XmlNode xmlDeliveryAddress = xmlDoc.CreateElement("DeliveryAddress");
                    xmlDeliveryAddress.InnerText = DeliveryAddress;
                    xmlContentInv.AppendChild(xmlDeliveryAddress);

                    XmlNode xmlPaymentTerm = xmlDoc.CreateElement("PaymentTerm");
                    xmlPaymentTerm.InnerText = PaymentTerm;
                    xmlContentInv.AppendChild(xmlPaymentTerm);

                    XmlNode xmlSellerName = xmlDoc.CreateElement("SellerName");
                    xmlSellerName.InnerText = SellerName;
                    xmlContentInv.AppendChild(xmlSellerName);

                    XmlNode xmlSellerAddress = xmlDoc.CreateElement("SellerAddress");
                    xmlSellerAddress.InnerText = SellerAddress;
                    xmlContentInv.AppendChild(xmlSellerAddress);

                    XmlNode xmlSellerTaxCode = xmlDoc.CreateElement("SellerTaxCode");
                    xmlSellerTaxCode.InnerText = SellerTaxCode;
                    xmlContentInv.AppendChild(xmlSellerTaxCode);

                    XmlNode xmlSellerBankName = xmlDoc.CreateElement("SellerBankName");
                    xmlSellerBankName.InnerText = SellerBankName;
                    xmlContentInv.AppendChild(xmlSellerBankName);

                    XmlNode xmlSellerBankAddress = xmlDoc.CreateElement("SellerBankAddress");
                    xmlSellerBankAddress.InnerText = SellerBankAddress;
                    xmlContentInv.AppendChild(xmlSellerBankAddress);

                    XmlNode xmlSellerBankNo = xmlDoc.CreateElement("SellerBankNo");
                    xmlSellerBankNo.InnerText = SellerBankNo;
                    xmlContentInv.AppendChild(xmlSellerBankNo);

                    XmlNode xmlSellerBankCode = xmlDoc.CreateElement("SellerBankCode");
                    xmlSellerBankCode.InnerText = SellerBankCode;
                    xmlContentInv.AppendChild(xmlSellerBankCode);

                    XmlNode xmlSellerPhone = xmlDoc.CreateElement("SellerPhone");
                    xmlSellerPhone.InnerText = SellerPhone;
                    xmlContentInv.AppendChild(xmlSellerPhone);

                    XmlNode xmlSellerFax = xmlDoc.CreateElement("SellerFax");
                    xmlSellerFax.InnerText = SellerFax;
                    xmlContentInv.AppendChild(xmlSellerFax);

                    XmlNode xmlContractNumber = xmlDoc.CreateElement("ContractNumber");
                    xmlContractNumber.InnerText = ContractNumber;
                    xmlContentInv.AppendChild(xmlContractNumber);

                    XmlNode xmlContractDate = xmlDoc.CreateElement("ContractDate");
                    xmlContractDate.InnerText = ContractDate;
                    xmlContentInv.AppendChild(xmlContractDate);

                    XmlNode xmlContNo = xmlDoc.CreateElement("ContNo");
                    xmlContNo.InnerText = ContNo;
                    xmlContentInv.AppendChild(xmlContNo);

                    XmlNode xmlReferenceNo = xmlDoc.CreateElement("ReferenceNo");
                    xmlReferenceNo.InnerText = ReferenceNo;
                    xmlContentInv.AppendChild(xmlReferenceNo);

                    XmlNode xmlVehicleNo = xmlDoc.CreateElement("VehicleNo");
                    xmlVehicleNo.InnerText = VehicleNo;
                    xmlContentInv.AppendChild(xmlVehicleNo);

                    XmlNode xmlDiscountAmount = xmlDoc.CreateElement("DiscountAmount");
                    xmlDiscountAmount.InnerText = DiscountAmount.ToString().Replace(",", ".");
                    xmlContentInv.AppendChild(xmlDiscountAmount);

                    XmlNode xmlDiscountRate = xmlDoc.CreateElement("DiscountRate");
                    xmlDiscountRate.InnerText = DiscountRate.ToString().Replace(",", ".");
                    xmlContentInv.AppendChild(xmlDiscountRate);

                    XmlNode xmlExchangeRate = xmlDoc.CreateElement("ExchangeRate");
                    xmlExchangeRate.InnerText = ExchangeRate.ToString().Replace(",", ".");
                    xmlContentInv.AppendChild(xmlExchangeRate);

                    XmlNode xmlExtra1 = xmlDoc.CreateElement("Extra1");
                    xmlExtra1.InnerText = Extra1;
                    xmlContentInv.AppendChild(xmlExtra1);

                    XmlNode xmlExtra2 = xmlDoc.CreateElement("Extra2");
                    xmlExtra2.InnerText = Extra2;
                    xmlContentInv.AppendChild(xmlExtra2);

                    XmlNode xmlExtra3 = xmlDoc.CreateElement("Extra3");
                    xmlExtra3.InnerText = Extra3;
                    xmlContentInv.AppendChild(xmlExtra3);

                    XmlNode xmlDocNo = xmlDoc.CreateElement("DocNo");
                    xmlDocNo.InnerText = DocNo;
                    xmlContentInv.AppendChild(xmlDocNo);

                    XmlNode xmlDocDate = xmlDoc.CreateElement("DocDate");
                    xmlDocDate.InnerText = DocDate;
                    xmlContentInv.AppendChild(xmlDocDate);

                    XmlNode xmlDocPerson = xmlDoc.CreateElement("DocPerson");
                    xmlDocPerson.InnerText = DocPerson;
                    xmlContentInv.AppendChild(xmlDocPerson);

                    XmlNode xmlDocWork = xmlDoc.CreateElement("DocWork");
                    xmlDocWork.InnerText = DocWork;
                    xmlContentInv.AppendChild(xmlDocWork);

                    XmlNode xmlDocShipper = xmlDoc.CreateElement("DocShipper");
                    xmlDocShipper.InnerText = DocShipper;
                    xmlContentInv.AppendChild(xmlDocShipper);

                    XmlNode xmlDocVehicle = xmlDoc.CreateElement("DocVehicle");
                    xmlDocVehicle.InnerText = DocVehicle;
                    xmlContentInv.AppendChild(xmlDocVehicle);

                    XmlNode xmlDocExport = xmlDoc.CreateElement("DocExport");
                    xmlDocExport.InnerText = DocExport;
                    xmlContentInv.AppendChild(xmlDocExport);

                    XmlNode xmlDocImport = xmlDoc.CreateElement("DocImport");
                    xmlDocImport.InnerText = DocImport;
                    xmlContentInv.AppendChild(xmlDocImport);

                    XmlNode xmlTotalQuantity = xmlDoc.CreateElement("TotalQuantity");
                    xmlTotalQuantity.InnerText = TotalQuantity;
                    xmlContentInv.AppendChild(xmlTotalQuantity);
                    //END ADDED FOR SELLERINVOICE

                    XmlNode xmlCusEmail = xmlDoc.CreateElement("CusEmail");
                    xmlCusEmail.InnerText = CusEmail;
                    xmlContentInv.AppendChild(xmlCusEmail);

                    XmlNode xmlFkey = xmlDoc.CreateElement("Fkey");
                    xmlFkey.InnerText = Fkey;
                    xmlContentInv.AppendChild(xmlFkey);

                    XmlNode xmlResourceCode = xmlDoc.CreateElement("ResourceCode");
                    xmlResourceCode.InnerText = ResourceCode;
                    xmlContentInv.AppendChild(xmlResourceCode);

                    //product
                    //van de ve san pham
                    XmlNode xmlProducts = xmlDoc.CreateElement("Products");
                    xmlContentInv.AppendChild(xmlProducts);
                    //khoi tao du lieu da co
                    //Insert tung phan tu vao product
                    foreach (var el in ProductList)
                    {
                        XmlNode xmlProduct = xmlDoc.CreateElement("Product");
                        xmlProducts.AppendChild(xmlProduct);

                        //begin product
                        XmlNode xmlProdCode = xmlDoc.CreateElement("Code");
                        xmlProdCode.InnerText = el.Code;
                        xmlProduct.AppendChild(xmlProdCode);

                        XmlNode xmlProdRemark = xmlDoc.CreateElement("Remark");
                        xmlProdRemark.InnerText = el.Remark;
                        xmlProduct.AppendChild(xmlProdRemark);

                        XmlNode xmlProdName = xmlDoc.CreateElement("ProdName");
                        xmlProdName.InnerText = el.Name;
                        xmlProduct.AppendChild(xmlProdName);

                        XmlNode xmlProdUnit = xmlDoc.CreateElement("ProdUnit");
                        xmlProdUnit.InnerText = el.Unit;
                        xmlProduct.AppendChild(xmlProdUnit);

                        XmlNode xmlProdQuantity = xmlDoc.CreateElement("ProdQuantity");
                        xmlProdQuantity.InnerText = el.Quantity.ToString().Replace(",", ".");
                        xmlProduct.AppendChild(xmlProdQuantity);

                        XmlNode xmlProdPrice = xmlDoc.CreateElement("ProdPrice");
                        xmlProdPrice.InnerText = el.Price.ToString().Replace(",", ".");
                        xmlProduct.AppendChild(xmlProdPrice);

                        XmlNode xmlProdTotal = xmlDoc.CreateElement("Total");
                        xmlProdTotal.InnerText = el.Total.ToString().Replace(",", ".");
                        xmlProduct.AppendChild(xmlProdTotal);

                        XmlNode xmlDiscount = xmlDoc.CreateElement("Discount");
                        xmlDiscount.InnerText = el.Discount.ToString().Replace(",", ".");
                        xmlProduct.AppendChild(xmlDiscount);

                        XmlNode xmlDiscountAmountPro = xmlDoc.CreateElement("DiscountAmount");
                        xmlDiscountAmountPro.InnerText = el.DiscountAmount.ToString().Replace(",", ".");
                        xmlProduct.AppendChild(xmlDiscountAmountPro);

                        XmlNode xmlProdVATRate = xmlDoc.CreateElement("VATRate");
                        xmlProdVATRate.InnerText = el.VATRate.ToString().Replace(",", ".");
                        xmlProduct.AppendChild(xmlProdVATRate);

                        XmlNode xmlProdVATAmount = xmlDoc.CreateElement("VATAmount");
                        xmlProdVATAmount.InnerText = el.VATAmount.ToString().Replace(",", ".");
                        xmlProduct.AppendChild(xmlProdVATAmount);

                        XmlNode xmlProdAmount = xmlDoc.CreateElement("Amount");
                        xmlProdAmount.InnerText = el.Amount.ToString().Replace(",", ".");
                        xmlProduct.AppendChild(xmlProdAmount);

                        XmlNode xmlProdExtra1 = xmlDoc.CreateElement("Extra1");
                        xmlProdExtra1.InnerText = el.Extra1;
                        xmlProduct.AppendChild(xmlProdExtra1);

                        XmlNode xmlProdExtra2 = xmlDoc.CreateElement("Extra2");
                        xmlProdExtra2.InnerText = el.Extra2;
                        xmlProduct.AppendChild(xmlProdExtra2);
                    }
                    //end product
                    //other
                    xmlDoc.AppendChild(xmlContentInv);
                }

                return xmlDoc.OuterXml;

            }
            catch (Exception ex)
            {
                Console.Write("DDS");
                return "SELLERINVOICE|GetStringXML: " + ex.Message;
            }
        }
    }
}
