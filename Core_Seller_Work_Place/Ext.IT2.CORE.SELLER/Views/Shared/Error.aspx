﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>
<html>
<head>
    <title>Error</title>
</head>
<body>
    <div>
        <h2>Not Found.</h2> 
        <%--<p><%= Html.Encode(ViewData["Data"])%></p> --%>    
        <p>The requested document was not found on this server.  </p>
    </div>
</body>
</html>