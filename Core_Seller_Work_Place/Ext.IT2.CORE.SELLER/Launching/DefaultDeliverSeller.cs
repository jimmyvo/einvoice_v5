﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EInvoice.Core.Domain;
using FX.Core;
using EInvoice.Core.IService;
using log4net;
using System.Xml.Linq;
using IdentityManagement.Domain;

namespace EInvoice.Core.Launching
{
    public class DefaultDeliverSeller : IDeliver
    {
        ILog log = LogManager.GetLogger(typeof(DefaultDeliverSeller));
        private string getInvTokenToStore(IInvoice inv)
        {
            StringBuilder rv = new StringBuilder();
            if (inv != null)
            {
                rv.AppendFormat("{0};{1};{2}", inv.Pattern, inv.Serial, inv.No.ToString("0000000"));
            }
            
            return rv.ToString();
        }
        public void PrepareDeliver(IInvoice[] invoices, Company _currentCompany)
        {
            String weblink = (_currentCompany.Config.Keys.Contains("LinkEmail")) ? _currentCompany.Config["LinkEmail"] : System.Configuration.ConfigurationManager.AppSettings["Portal"];
            //FX.Utils.EmailService.IEmailService emailSrv = IoC.Resolve<FX.Utils.EmailService.IEmailService>();
            SendEmailDBSellerService emailSrv = IoC.Resolve<SendEmailDBSellerService>();
            ICustomerService cusSrv = IoC.Resolve<ICustomerService>();
            foreach (IInvoice inv in invoices)
            {
                try
                {
                    Customer cus = cusSrv.SearchCusCode(inv.CusCode, _currentCompany.id);
                    Dictionary<string, string> subjectParams = new Dictionary<string, string>(1);
                    subjectParams.Add("$site", "Thông báo về việc phát hành Hóa đơn điện tử");
                    subjectParams.Add("$invNumber", inv.No.ToString("0000000"));
                    subjectParams.Add("$fkey", inv.Fkey);
                    subjectParams.Add("$CusCode", inv.CusCode);
                    subjectParams.Add("$Amount", inv.Amount.ToString().Replace(",", "."));
                    Dictionary<string, string> bodyParams = new Dictionary<string, string>(5);
                    bodyParams.Add("$company", _currentCompany.Name);

                    string fkey = inv.Fkey;
                    bodyParams.Add("$fkey", fkey);
                    //Added by HienTT
                    string xmlLink = (_currentCompany.Config.Keys.Contains("LinkEmail")) ? _currentCompany.Config["LinkEmail"] : "<LinkEmail><view></view><inv></inv><pdf></pdf></LinkEmail>";
                    if (!string.IsNullOrEmpty(xmlLink))
                    {
                        XElement elem = XElement.Parse(xmlLink);
                        string viewLink = elem.Element("view").Value;
                        string invLink = elem.Element("inv").Value;
                        string pdfLink = elem.Element("pdf").Value;
                        //string stoken = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(inv.Pattern + "_" + inv.Serial + "_" + inv.No.ToString()));
                        string stoken = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(inv.id + "_" + inv.Pattern + "_" + _currentCompany.id.ToString()));
                        string file = viewLink.Replace("@token", stoken).Replace("@fkey", fkey);
                        string pdfFile = pdfLink.Replace("@token", stoken).Replace("@fkey", fkey);
                        if (cus != null)
                        {
                            bodyParams.Add("$NameCustomer", cus.Name);
                            bodyParams.Add("$CusTaxCode", cus.TaxCode);
                            bodyParams.Add("$CusCode", cus.Code);
                            string stokenDown = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(inv.id + ";" + inv.Pattern + ";" + _currentCompany.id.ToString() + ";" + cus.AccountName));
                            string token = invLink.Replace("@token", stokenDown).Replace("@fkey", fkey);
                            bodyParams.Add("$fileUrl", token);
                        }
                        else
                        {
                            bodyParams.Add("$NameCustomer", !string.IsNullOrEmpty(inv.CusName) ? inv.CusName : inv.Buyer);
                            bodyParams.Add("$CusTaxCode", inv.CusTaxCode);
                            bodyParams.Add("$CusCode", inv.CusCode);
                        }
                        bodyParams.Add("$pdfUrl", pdfFile);
                        bodyParams.Add("$invoiceUrl", file);
                        bodyParams.Add("$pdfUrlHsm", pdfFile);


                    }
                   
                    var temp = inv as SELLERINVOICE;
                    if (temp != null)
                    {
                        bodyParams.Add("$InvMonth", temp.KindOfService);
                    }
                    else
                    {
                        bodyParams.Add("$InvMonth", inv.ArisingDate.Month.ToString() + "/" + inv.ArisingDate.Year.ToString());
                    }

                    string linkPortal = getLinkPortal(_currentCompany);
                    if (!string.IsNullOrEmpty(linkPortal))
                    {
                        string pdfUrlToken = linkPortal + "/Share/downloadPDFToMail/" + inv.id + "?pattern=" + inv.Pattern + "&comid=" + inv.ComID + "&fkey=" + inv.Fkey;
                        string linkViewToken = linkPortal + "/Share/viewInv?idInvoice=" + inv.id + "&pattern=" + inv.Pattern + "&comId=" + inv.ComID + "&fkey=" + inv.Fkey;
                        bodyParams.Add("$pdfUrl", pdfUrlToken);
                        bodyParams.Add("$viewUrl", linkViewToken);
                        bodyParams.Add("$Portal", linkPortal);
                    }

                    bodyParams.Add("$pattern", inv.Pattern);
                    bodyParams.Add("$serial", inv.Serial);
                    bodyParams.Add("$invNumber", inv.No.ToString("0000000"));
                    bodyParams.Add("$ArisingDate", inv.ArisingDate.Day.ToString("00") + "/" + inv.ArisingDate.Month.ToString("00") + "/" + inv.ArisingDate.Year.ToString("0000"));
                    string type = "";
                    switch (inv.Type)
                    {
                        case InvoiceType.ForAdjustAccrete:
                            type = "điều chỉnh"; // d/c tang
                            break;
                        case InvoiceType.ForAdjustReduce:
                            type = "điều chỉnh"; // d/c giam
                            break;
                        case InvoiceType.ForAdjustInfo:
                            type = "điều chỉnh"; // d/c thong tin
                            break;
                        case InvoiceType.ForReplace:
                            type = "thay thế"; // thay the
                            break;
                        case InvoiceType.Nomal:
                            type = "phát hành"; // phat hanh
                            break;
                    }
                    bodyParams.Add("$type", type);
                    bodyParams.Add("$Buyer", inv.Buyer);

                    string invToken = getInvTokenToStore(inv);

                    string labelMail = _currentCompany.Config.ContainsKey("LabelMail") ? _currentCompany.Config["LabelMail"] : "hoadondientu@vnpt-invoice.vn";
                    string email = string.Empty;
                    string templateMailConfig = _currentCompany.Config.ContainsKey(getMessageMail(inv.Pattern.Replace("/", "") + inv.Serial.Replace("/", ""))) ? getMessageMail(inv.Pattern.Replace("/", "") + inv.Serial.Replace("/", "")) : "MessagerMail";
                    // thiết lập gửi/không gửi mail(MSB) - create by khanhtq 
                    if (_currentCompany.Config.ContainsKey("TypeLauncher") && _currentCompany.Config["TypeLauncher"].Equals("EInvoice.Core.Launching.LBLauncherSigned"))
                    {
                        if (cus != null && cus.DeliverMethod == 1 && !string.IsNullOrEmpty(cus.Email))
                            email = cus.Email;
                        if (!string.IsNullOrEmpty(email))
                            emailSrv.ProcessEmail(labelMail, email, templateMailConfig, subjectParams, bodyParams, invToken, _currentCompany);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(inv.EmailDeliver))
                            email = inv.EmailDeliver;
                        else if (cus != null && !string.IsNullOrEmpty(cus.Email))
                            email = cus.Email;
                        if (!string.IsNullOrEmpty(email))
                            emailSrv.ProcessEmail(labelMail, email, templateMailConfig, subjectParams, bodyParams, invToken, _currentCompany);
                    }
                }

                catch (Exception ex) { log.Error("PrepareDeliver " + ex); }
            }
        }

        private string getMessageMail(string resourceCode)
        {
            if (!string.IsNullOrEmpty(resourceCode))
            {
                return "MessagerMail_" + resourceCode;
            }
            else
            {
                return "MessagerMail";
            }
        }

        private string getLinkPortal(Company com)
        {
            string portal = "";
            List<string> listDomain = ExtractFromString(com.Domain, "(", ")");
            foreach (var item in listDomain)
            {
                if (item.Contains("portal"))
                {
                    if (item.Contains("443")) portal = "https://" + item.Split(':')[0];
                    else portal = "http://" + item.Split(':')[0];
                }
            }
            return portal;
        }
        public static List<string> ExtractFromString(string text, string startString, string endString)
        {
            List<string> matched = new List<string>();
            int indexStart = 0, indexEnd = 0;
            bool exit = false;
            while (!exit)
            {
                indexStart = text.IndexOf(startString);
                indexEnd = text.IndexOf(endString);
                if (indexStart != -1 && indexEnd != -1)
                {
                    matched.Add(text.Substring(indexStart + startString.Length,
                        indexEnd - indexStart - startString.Length));
                    text = text.Substring(indexEnd + endString.Length);
                }
                else
                    exit = true;
            }
            return matched;
        }
        public void Deliver(IInvoice[] invoices, Company _currentCompany)
        {
            PrepareDeliver(invoices, _currentCompany);
        }
		
		public void ProcessingSendmail(IInvoice oInv, IInvoice newInv, int processingType, string pattern)
        {
            try
            {
                string CusCode = (oInv.CusCode != null) ? oInv.CusCode : "";
                Customer cus = IoC.Resolve<ICustomerService>().SearchCusCode(CusCode, oInv.ComID);
                Company currentCom = ((EInvoiceContext)FXContext.Current).CurrentCompany;
                if (((currentCom.Config.Keys.Contains("MessagerAdjust") && processingType == 0) || (currentCom.Config.Keys.Contains("MessagerReplace") && processingType == 1)) || (currentCom.Config.Keys.Contains("MessagerCancel") && processingType == 2))
                {
                    SendEmailDBSellerService emailSrv = IoC.Resolve<SendEmailDBSellerService>(); //FX.Core.IoC.Resolve<FX.Utils.EmailService.IEmailService>();
                    Dictionary<string, string> subjectParams = new Dictionary<string, string>(1);
                    subjectParams.Add("subject", "");
                    subjectParams.Add("$invNumber", oInv.No.ToString("0000000"));
                    subjectParams.Add("$fkey", oInv.Fkey);
                    subjectParams.Add("$CusCode", oInv.CusCode);
                    subjectParams.Add("$Amount", oInv.Amount.ToString().Replace(",", "."));
                    Dictionary<string, string> bodyParams = new Dictionary<string, string>(9);
                    bodyParams.Add("$CompanyName", currentCom.Name);
                    bodyParams.Add("$pattern", oInv.Pattern);
                    bodyParams.Add("$serial", oInv.Serial);
                    bodyParams.Add("$CusCode", oInv.CusCode);
                    bodyParams.Add("$invNumber", oInv.No.ToString("0000000"));
                    user _currentUser = FXContext.Current.CurrentUser;
                    string weblink = (currentCom.Config.Keys.Contains("LinkEmail")) ? currentCom.Config["LinkEmail"] : System.Configuration.ConfigurationManager.AppSettings["Portal"];
                    if (newInv != null)
                    {
                        subjectParams.Add("$Newfkey", newInv.Fkey);
                        bodyParams.Add("$NewCusCode", newInv.CusCode);
                        bodyParams.Add("$Newpattern", newInv.Pattern);
                        bodyParams.Add("$NewSerial", newInv.Serial);
                        bodyParams.Add("$NewInvMumber", newInv.No.ToString("0000000"));
                        bodyParams.Add("$AdjustType", Utils.GetNameInvoiceType((InvoiceType)newInv.Type));
                        string token = Utils.recordsDownloadLink(weblink, oInv.id.ToString(), newInv.id.ToString(), pattern, currentCom.id.ToString(), _currentUser.username);
                        bodyParams.Add("$RecordUrl", token);
                    }
                    // String weblink = System.Configuration.ConfigurationManager.AppSettings["Portal"];

                    string email = string.Empty;
                    if (!string.IsNullOrEmpty(oInv.EmailDeliver))
                        email = oInv.EmailDeliver;
                    else if (cus != null && !string.IsNullOrEmpty(cus.Email))
                        email = cus.Email;
                    
                    if (!string.IsNullOrEmpty(email)) {
                        string invToken = string.Empty;

                        string labelMail = currentCom.Config.ContainsKey("LabelMail") ? currentCom.Config["LabelMail"] : "hoadondientu@vnpt-invoice.vn";
                        if (processingType == 0)
                        {
                            invToken = getInvTokenToStore(newInv);
                            emailSrv.ProcessEmail(labelMail, email, "MessagerAdjust", subjectParams, bodyParams, invToken, currentCom);
                        }
                        else if (processingType == 1)
                        {
                            invToken = getInvTokenToStore(newInv);
                            emailSrv.ProcessEmail(labelMail, email, "MessagerReplace", subjectParams, bodyParams, invToken, currentCom);
                        }
                        else if (processingType == 2)
                        {
                            invToken = getInvTokenToStore(oInv);
                            emailSrv.ProcessEmail(labelMail, email, "MessagerCancel", subjectParams, bodyParams, invToken, currentCom);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
            }
        }
		
        public bool IsAllowDeliver(IInvoice inv, Company _currentCompany)
        {
            return true;
        }
    }
}
