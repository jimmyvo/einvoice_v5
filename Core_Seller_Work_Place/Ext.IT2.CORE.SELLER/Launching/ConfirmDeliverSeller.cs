﻿using System;
using log4net;
using EInvoice.Core.Domain;
using EInvoice.Core.IService;
using FX.Core;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using IdentityManagement.Domain;

namespace EInvoice.Core.Launching
{
    public class ConfirmDeliverSeller : IDeliver
    {
        ILog log = LogManager.GetLogger(typeof(ConfirmDeliverSeller));
        private string getInvTokenToStore(IInvoice inv)
        {
            StringBuilder rv = new StringBuilder();
            if (inv != null)
            {
                rv.AppendFormat("{0};{1};{2}", inv.Pattern, inv.Serial, inv.No.ToString("0000000"));
            }
            return rv.ToString();
        }
        private string getInvToken(IInvoice inv, Company _currentCompany)
        {
            StringBuilder rv = new StringBuilder();
            rv.AppendFormat("{0};{1};{2};{3}", inv.Pattern, inv.Serial, inv.No, _currentCompany.id);
            return rv.ToString();
        }
        public void PrepareDeliver(IInvoice[] invoices, Company _currentCompany)
        {
            Parallel.ForEach(invoices, inv =>
            {
                IInvDeliverService _deliverSrv = IoC.Resolve<IInvDeliverService>();
                try
                {
                    InvDeliver deliver = new InvDeliver();
                    deliver.InvToken = getInvToken(inv, _currentCompany);
                    deliver.isDelivery = 0;
                    _deliverSrv.CreateNew(deliver);
                    _deliverSrv.CommitChanges();
                }

                catch (Exception ex) { log.Error("PrepareDeliver: " + ex); }

            });
        }
        public void Deliver(IInvoice[] invoices, Company _currentCompany)
        {
            //log.Error(invoices[0].No + "   " + _currentCompany.id);
            IInvDeliverService _deliverSrv = IoC.Resolve<IInvDeliverService>();
            //FX.Utils.EmailService.IEmailService emailSrv = IoC.Resolve<FX.Utils.EmailService.IEmailService>();
            SendEmailDBSellerService emailSrv = IoC.Resolve<SendEmailDBSellerService>();
            ICustomerService cusSrv = IoC.Resolve<ICustomerService>();
            foreach (IInvoice inv in invoices)
            {
                try
                {
                    string invToken = getInvToken(inv, _currentCompany);
                    InvDeliver deliver = _deliverSrv.searchByTokenString(invToken);
                    if (null != deliver)
                    {
                        //FX.Utils.EmailService.IEmailService emailSrv = IoC.Resolve<FX.Utils.EmailService.IEmailService>();
                        Dictionary<string, string> subjectParams = new Dictionary<string, string>(1);
                        Customer cus = cusSrv.SearchCusCode(inv.CusCode, _currentCompany.id);
                        if (cus == null || string.IsNullOrEmpty(cus.Email) || string.IsNullOrWhiteSpace(cus.Email) || cus.Email.Equals("NULL"))
                        {
                            deliver.isDelivery = 1;
                            _deliverSrv.Save(deliver);
                            _deliverSrv.CommitChanges();
                            continue;
                        }
                        //subjectParams.Add("$site", "Phát hành hóa đơn điện tử");
                        subjectParams.Add("$site", "Thông báo về việc phát hành Hóa đơn điện tử");
                        subjectParams.Add("$invNumber", inv.No.ToString("0000000"));
                        subjectParams.Add("$fkey", inv.Fkey);
                        subjectParams.Add("$CusCode", inv.CusCode);
                        subjectParams.Add("$Amount", inv.Amount.ToString().Replace(",", "."));
                        Dictionary<string, string> bodyParams = new Dictionary<string, string>(12);
                        bodyParams.Add("$company", _currentCompany.Name);
                        /*
                         * cấu trúc xml link email:
                         * <view;inv;pdf> @token, @fkey
                          <LinkEmail>
                             <view>http://ebill.hcmtelecom.vn/HDDT_View.aspx?token=@token</view>
                             <inv>http://ebill.hcmtelecom.vn/HDDT_Download.aspx?token=@token<inv>
                             <pdf>http://ebill.hcmtelecom.vn/HDDT_PDF.aspx?token=@token</pdf>
                          </LinkEmail>
                         */
                        //String weblink = System.Configuration.ConfigurationManager.AppSettings["Portal"];
                        //string file = Utils.createInvoiceDownloadLink(System.Configuration.ConfigurationManager.AppSettings["viewLink"], inv.id.ToString(), inv.Pattern, _currentCompany.id.ToString(), cus.AccountName);
                        //string xmlLink = "<view>http://ebill.hcmtelecom.vn/HDDT_View.aspx?token=@token</view><inv>http://ebill.hcmtelecom.vn/HDDT_Download.aspx?token=@token<inv><pdf>http://ebill.hcmtelecom.vn/HDDT_PDF.aspx?token=@token</pdf>";
                        string xmlLink = (_currentCompany.Config.Keys.Contains("LinkEmail")) ? _currentCompany.Config["LinkEmail"] : "<LinkEmail><view></view><inv></inv><pdf></pdf></LinkEmail>";
                        XElement elem = XElement.Parse(xmlLink);
                        string viewLink = elem.Element("view").Value;
                        string invLink = elem.Element("inv").Value;
                        string pdfLink = elem.Element("pdf").Value;
                        string stoken = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(inv.id + "_" + inv.Pattern + "_" + _currentCompany.id.ToString()));
                        string stokenDown = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(inv.id + ";" + inv.Pattern + ";" + _currentCompany.id.ToString() + ";" + cus.AccountName));
                        string fkey = inv.Fkey;
                        string file = viewLink.Replace("@token", stoken).Replace("@fkey", fkey);
                        string token = invLink.Replace("@token", stokenDown).Replace("@fkey", fkey);
                        string pdfFile = pdfLink.Replace("@token", stoken).Replace("@fkey", fkey);


                        //string token = System.Configuration.ConfigurationManager.AppSettings["downloadLink"] + stoken;
                        //string file = System.Configuration.ConfigurationManager.AppSettings["viewLink"] + stoken;
                        //string pdfFile = System.Configuration.ConfigurationManager.AppSettings["pdfLink"] + stoken;
                        bodyParams.Add("$fileUrl", token);
                        bodyParams.Add("$pdfUrl", pdfFile);
                        bodyParams.Add("$invoiceUrl", file);
                        bodyParams.Add("$pattern", inv.Pattern);
                        bodyParams.Add("$serial", inv.Serial);
                        bodyParams.Add("$invNumber", inv.No.ToString("0000000"));
                        if (inv.GetType().Equals(typeof(SELLERINVOICE)))
                            bodyParams.Add("$InvMonth", ((SELLERINVOICE)inv).KindOfService ?? "");
                        else
                            bodyParams.Add("$InvMonth", inv.ArisingDate.Month.ToString() + "/" + inv.ArisingDate.Year.ToString());
                        bodyParams.Add("$NameCustomer", inv.CusName);
                        bodyParams.Add("$CusTaxCode", inv.CusTaxCode ?? "");
                        bodyParams.Add("$CusCode", inv.CusCode);
                        bodyParams.Add("$ArisingDate", inv.ArisingDate.Day.ToString("00") + "/" + inv.ArisingDate.Month.ToString("00") + "/" + inv.ArisingDate.Year.ToString("0000"));

                        string invTokenToStore = getInvTokenToStore(inv);

                        // fkey hóa đơn
                        bodyParams.Add("$fkey", fkey);
                        string email = string.Empty;
                        string templateMailConfig = _currentCompany.Config.ContainsKey(getMessageMail(inv.Pattern.Replace("/", "") + inv.Serial.Replace("/", ""))) ? getMessageMail(inv.Pattern.Replace("/", "") + inv.Serial.Replace("/", "")) : "MessagerMail";
                        if (!string.IsNullOrEmpty(inv.EmailDeliver))
                            email = inv.EmailDeliver;
                        else if (cus != null && !string.IsNullOrEmpty(cus.Email))
                            email = cus.Email;
                        if (!string.IsNullOrEmpty(email))
                            emailSrv.ProcessEmail("hoadondientu@vnpt-invoice.vn", email, templateMailConfig, subjectParams, bodyParams, invTokenToStore, _currentCompany);
                        //save lai vao bang InvDeliver
                        deliver.isDelivery = 1;
                        _deliverSrv.Save(deliver);
                        _deliverSrv.CommitChanges();
                    }
                }

                catch (Exception ex) { log.Error("Deliver: " + ex.Message); }
            }
        }
		
		public void ProcessingSendmail(IInvoice oInv, IInvoice newInv, int processingType, string pattern)
        {
            try
            {
                string CusCode = (oInv.CusCode != null) ? oInv.CusCode : "";
                Customer cus = IoC.Resolve<ICustomerService>().SearchCusCode(CusCode, oInv.ComID);
                Company currentCom = ((EInvoiceContext)FXContext.Current).CurrentCompany;
                if (((currentCom.Config.Keys.Contains("MessagerAdjust") && processingType == 0) || (currentCom.Config.Keys.Contains("MessagerReplace") && processingType == 1)) || (currentCom.Config.Keys.Contains("MessagerCancel") && processingType == 2))
                {
                    //FX.Utils.EmailService.IEmailService emailSrv = FX.Core.IoC.Resolve<FX.Utils.EmailService.IEmailService>();
                    SendEmailDBSellerService emailSrv = IoC.Resolve<SendEmailDBSellerService>();
                    Dictionary<string, string> subjectParams = new Dictionary<string, string>(1);
                    subjectParams.Add("subject", "");
                    subjectParams.Add("$invNumber", oInv.No.ToString("0000000"));
                    subjectParams.Add("$fkey", oInv.Fkey);
                    subjectParams.Add("$CusCode", oInv.CusCode);
                    subjectParams.Add("$Amount", oInv.Amount.ToString().Replace(",", "."));
                    Dictionary<string, string> bodyParams = new Dictionary<string, string>(9);
                    bodyParams.Add("$CompanyName", currentCom.Name);
                    bodyParams.Add("$pattern", oInv.Pattern);
                    bodyParams.Add("$serial", oInv.Serial);
                    bodyParams.Add("$CusCode", oInv.CusCode);
                    bodyParams.Add("$invNumber", oInv.No.ToString("0000000"));
                    user _currentUser = FXContext.Current.CurrentUser;
                    string weblink = (currentCom.Config.Keys.Contains("LinkEmail")) ? currentCom.Config["LinkEmail"] : System.Configuration.ConfigurationManager.AppSettings["Portal"];
                    if (newInv != null)
                    {
                        subjectParams.Add("$Newfkey", newInv.Fkey);
                        bodyParams.Add("$NewCusCode", newInv.CusCode);
                        bodyParams.Add("$Newpattern", newInv.Pattern);
                        bodyParams.Add("$NewSerial", newInv.Serial);
                        bodyParams.Add("$NewInvMumber", newInv.No.ToString("0000000"));
                        bodyParams.Add("$AdjustType", Utils.GetNameInvoiceType((InvoiceType)newInv.Type));
                        string token = Utils.recordsDownloadLink(weblink, oInv.id.ToString(), newInv.id.ToString(), pattern, currentCom.id.ToString(), _currentUser.username);
                        bodyParams.Add("$RecordUrl", token);
                    }
                    // String weblink = System.Configuration.ConfigurationManager.AppSettings["Portal"];

                    string email = string.Empty;
                    if (!string.IsNullOrEmpty(oInv.EmailDeliver))
                        email = oInv.EmailDeliver;
                    else if (cus != null && !string.IsNullOrEmpty(cus.Email))
                        email = cus.Email;

                    if (!string.IsNullOrEmpty(email))
                    {
                        string invTokenToStore = string.Empty;

                        string labelMail = currentCom.Config.ContainsKey("LabelMail") ? currentCom.Config["LabelMail"] : "hoadondientu@vnpt-invoice.vn";
                        if (processingType == 0)
                        {
                            invTokenToStore = getInvTokenToStore(newInv);
                            emailSrv.ProcessEmail(labelMail, email, "MessagerAdjust", subjectParams, bodyParams, invTokenToStore, currentCom);
                        }
                        else if (processingType == 1)
                        {
                            invTokenToStore = getInvTokenToStore(newInv);
                            emailSrv.ProcessEmail(labelMail, email, "MessagerReplace", subjectParams, bodyParams, invTokenToStore, currentCom);
                        }
                        else if (processingType == 2)
                        {
                            invTokenToStore = getInvTokenToStore(oInv);
                            emailSrv.ProcessEmail(labelMail, email, "MessagerCancel", subjectParams, bodyParams, invTokenToStore, currentCom);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
            }
        }
		
        public bool IsAllowDeliver(IInvoice inv, Company _currentCompany)
        {
            IInvDeliverService _deliverSrv = IoC.Resolve<IInvDeliverService>();
            try
            {
                string invToken = getInvToken(inv, _currentCompany);
                InvDeliver deliver = _deliverSrv.searchByTokenString(invToken);
                if (null == deliver || deliver.isDelivery != 0)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                log.Error("IsAllowDeliver: " + ex);
            }
            return false;
        }
        
        private string getMessageMail(string resourceCode)
        {
            if (!string.IsNullOrEmpty(resourceCode))
            {
                return "MessagerMail_" + resourceCode;
            }
            else
            {
                return "MessagerMail";
            }
        }
    }
}
