﻿using System;
using log4net;
using EInvoice.Core.Domain;
using EInvoice.Core.IService;
using FX.Core;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;
using IdentityManagement.Domain;

namespace EInvoice.Core.Launching
{
    public class ConfirmDeliverForAIRPORT : IDeliver
    {
        ILog log = LogManager.GetLogger(typeof(ConfirmDeliverForAIRPORT));
        private string getInvToken(IInvoice inv, Company _currentCompany)
        {
            StringBuilder rv = new StringBuilder();
            rv.AppendFormat("{0};{1};{2};{3}", inv.Pattern, inv.Serial, inv.No, _currentCompany.id);
            return rv.ToString();
        }
        public void PrepareDeliver(IInvoice[] invoices, Company _currentCompany)
        {
            String weblink = (_currentCompany.Config.Keys.Contains("LinkEmail")) ? _currentCompany.Config["LinkEmail"] : System.Configuration.ConfigurationManager.AppSettings["Portal"];
            FX.Utils.EmailService.IEmailService emailSrv = IoC.Resolve<FX.Utils.EmailService.IEmailService>();
            ICustomerService cusSrv = IoC.Resolve<ICustomerService>();
            string labelMail = _currentCompany.Config.ContainsKey("LabelMail") ? _currentCompany.Config["LabelMail"] : "hoadondientu@vnpt-invoice.vn";
            foreach (IInvoice inv in invoices)
            {
                try
                {
                    Dictionary<string, string> subjectParams = new Dictionary<string, string>();
                    Customer cus = cusSrv.SearchCusCode(inv.CusCode, _currentCompany.id);
                    string emailToSend = ((AIRPORTINVOICE)inv).CusEmail;

                    if (string.IsNullOrEmpty(emailToSend))
                    {
                        if (cus != null && !string.IsNullOrEmpty(cus.Email))
                        {
                            emailToSend = cus.Email;
                        }
                    }

                    if (emailToSend != null && !string.IsNullOrEmpty(emailToSend))
                    {
                        subjectParams.Add("$site", "Thông báo về việc phát hành Hóa đơn điện tử");
                        subjectParams.Add("$invNumber", inv.No.ToString("0000000"));
                        subjectParams.Add("$pattern", inv.Pattern);
                        subjectParams.Add("$serial", inv.Serial);
                        subjectParams.Add("$CusCode", inv.CusCode);
                        subjectParams.Add("$Fkey", inv.Fkey);
                        subjectParams.Add("$Buyer", inv.Buyer);
                        subjectParams.Add("$ArisingDate", inv.ArisingDate.ToString("dd/MM/yyyy"));
                        Dictionary<string, string> bodyParams = new Dictionary<string, string>();
                        bodyParams.Add("$company", _currentCompany.Name);
                        
                        //Added by HienTT
                        string xmlLink = (_currentCompany.Config.Keys.Contains("LinkEmail")) ? _currentCompany.Config["LinkEmail"] : "<LinkEmail><view></view><inv></inv><pdf></pdf></LinkEmail>";
                        XElement elem = XElement.Parse(xmlLink);
                        string viewLink = elem.Element("view").Value;
                        string invLink = elem.Element("inv").Value;
                        string pdfLink = elem.Element("pdf").Value;
                        //string stoken = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(inv.Pattern + "_" + inv.Serial + "_" + inv.No.ToString()));
                        string stoken = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(inv.id + "_" + inv.Pattern + "_" + _currentCompany.id.ToString()));
                        string stokenDown = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(inv.id + ";" + inv.Pattern + ";" + _currentCompany.id.ToString() + ";" + inv.CusCode));
                        string fkey = inv.Fkey;
                        string file = viewLink.Replace("@token", stoken).Replace("@fkey", fkey);
                        string token = invLink.Replace("@token", stokenDown).Replace("@fkey", fkey);
                        string pdfFile = pdfLink.Replace("@token", stoken).Replace("@fkey", fkey);
                        bodyParams.Add("$fileUrl", token);
                        bodyParams.Add("$pdfUrl", pdfFile);
                        bodyParams.Add("$invoiceUrl", file);

                        var temp = inv as InvoiceVAT;
                        if (temp != null)
                        {
                            bodyParams.Add("$InvMonth", temp.KindOfService);
                        }
                        else
                        {
                            bodyParams.Add("$InvMonth", inv.ArisingDate.Month.ToString("00") + "/" + inv.ArisingDate.Year.ToString());
                        }
                        bodyParams.Add("$NameCustomer", string.IsNullOrEmpty(inv.CusName) ? inv.Buyer : inv.CusName);
                        bodyParams.Add("$CusTaxCode", inv.CusTaxCode);
                        bodyParams.Add("$CusCode", inv.CusCode);
                        bodyParams.Add("$pattern", inv.Pattern);
                        bodyParams.Add("$serial", inv.Serial);
                        bodyParams.Add("$invNumber", inv.No.ToString("0000000"));
                        bodyParams.Add("$comPhone", _currentCompany.Phone);
                        bodyParams.Add("$Fkey", inv.Fkey);
                        bodyParams.Add("$Buyer", inv.Buyer);
                        bodyParams.Add("$ArisingDate", inv.ArisingDate.ToString("dd/MM/yyyy"));
                        emailSrv.ProcessEmail(labelMail, emailToSend, "MessagerMail", subjectParams, bodyParams);
                    }
                }

                catch (Exception ex) { log.Warn("PrepareDeliver|ConfirmDeliverForAIRPORT " + ex); }
            }

        }
        public void Deliver(IInvoice[] invoices, Company _currentCompany)
        {
            //log.Error(invoices[0].No + "   " + _currentCompany.id);
            IInvDeliverService _deliverSrv = IoC.Resolve<IInvDeliverService>();
            FX.Utils.EmailService.IEmailService emailSrv = IoC.Resolve<FX.Utils.EmailService.IEmailService>();
            ICustomerService cusSrv = IoC.Resolve<ICustomerService>();
            string labelMail = _currentCompany.Config.ContainsKey("LabelMail") ? _currentCompany.Config["LabelMail"] : "hoadondientu@vnpt-invoice.vn";
            foreach (IInvoice inv in invoices)
            {
                try
                {
                    string invToken = getInvToken(inv, _currentCompany);
                    InvDeliver deliver = _deliverSrv.searchByTokenString(invToken);
                    if (null != deliver)
                    {
                        //FX.Utils.EmailService.IEmailService emailSrv = IoC.Resolve<FX.Utils.EmailService.IEmailService>();
                        Dictionary<string, string> subjectParams = new Dictionary<string, string>();
                        Customer cus = cusSrv.SearchCusCode(inv.CusCode, _currentCompany.id);
                        string emailToSend = ((AIRPORTINVOICE)inv).CusEmail;
                        if (string.IsNullOrEmpty(emailToSend))
                        {
                            if (cus != null && string.IsNullOrEmpty(cus.Email))
                            {
                                emailToSend = cus.Email;
                            }
                        }
                        if (string.IsNullOrEmpty(emailToSend) || string.IsNullOrWhiteSpace(emailToSend) || emailToSend.Equals("NULL"))
                        {
                            deliver.isDelivery = 1;
                            _deliverSrv.Save(deliver);
                            _deliverSrv.CommitChanges();
                            continue;
                        }
                        //subjectParams.Add("$site", "Phát hành hóa đơn điện tử");
                        subjectParams.Add("$site", "Thông báo về việc phát hành Hóa đơn điện tử");
                        subjectParams.Add("$invNumber", inv.No.ToString("0000000"));
                        subjectParams.Add("$pattern", inv.Pattern);
                        subjectParams.Add("$serial", inv.Serial);
                        subjectParams.Add("$CusCode", inv.CusCode);
                        subjectParams.Add("$Fkey", inv.Fkey);
                        subjectParams.Add("$Buyer", inv.Buyer);
                        subjectParams.Add("$ArisingDate", inv.ArisingDate.ToString("dd/MM/yyyy"));
                        Dictionary<string, string> bodyParams = new Dictionary<string, string>(12);
                        bodyParams.Add("$company", _currentCompany.Name);

                        /*
                         * cấu trúc xml link email:
                         * <view;inv;pdf> @token, @fkey
                          <LinkEmail>
                             <view>http://ebill.hcmtelecom.vn/HDDT_View.aspx?token=@token</view>
                             <inv>http://ebill.hcmtelecom.vn/HDDT_Download.aspx?token=@token<inv>
                             <pdf>http://ebill.hcmtelecom.vn/HDDT_PDF.aspx?token=@token</pdf>
                          </LinkEmail>
                         */
                        //String weblink = System.Configuration.ConfigurationManager.AppSettings["Portal"];
                        //string file = Utils.createInvoiceDownloadLink(System.Configuration.ConfigurationManager.AppSettings["viewLink"], inv.id.ToString(), inv.Pattern, _currentCompany.id.ToString(), cus.AccountName);
                        //string xmlLink = "<view>http://ebill.hcmtelecom.vn/HDDT_View.aspx?token=@token</view><inv>http://ebill.hcmtelecom.vn/HDDT_Download.aspx?token=@token<inv><pdf>http://ebill.hcmtelecom.vn/HDDT_PDF.aspx?token=@token</pdf>";
                        string xmlLink = (_currentCompany.Config.Keys.Contains("LinkEmail")) ? _currentCompany.Config["LinkEmail"] : "<LinkEmail><view></view><inv></inv><pdf></pdf></LinkEmail>";
                        XElement elem = XElement.Parse(xmlLink);
                        string viewLink = elem.Element("view").Value;
                        string invLink = elem.Element("inv").Value;
                        string pdfLink = elem.Element("pdf").Value;
                        string stoken = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(inv.id + "_" + inv.Pattern + "_" + _currentCompany.id.ToString()));
                        string stokenDown = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(inv.id + ";" + inv.Pattern + ";" + _currentCompany.id.ToString()+ ";" + cus.AccountName));
                        string fkey = inv.Fkey;
                        string file = viewLink.Replace("@token", stoken).Replace("@fkey", fkey);
                        string token = invLink.Replace("@token", stokenDown).Replace("@fkey", fkey);
                        string pdfFile = pdfLink.Replace("@token", stoken).Replace("@fkey", fkey);


                        //string token = System.Configuration.ConfigurationManager.AppSettings["downloadLink"] + stoken;
                        //string file = System.Configuration.ConfigurationManager.AppSettings["viewLink"] + stoken;
                        //string pdfFile = System.Configuration.ConfigurationManager.AppSettings["pdfLink"] + stoken;
                        bodyParams.Add("$fileUrl", token);
                        bodyParams.Add("$pdfUrl", pdfFile);
                        bodyParams.Add("$invoiceUrl", file);
                        bodyParams.Add("$pattern", inv.Pattern);
                        bodyParams.Add("$serial", inv.Serial);
                        bodyParams.Add("$invNumber", inv.No.ToString("0000000"));
                        var temp = inv as InvoiceVAT;
                        if (temp != null)
                        {
                            bodyParams.Add("$InvMonth", temp.KindOfService);
                        }
                        else
                        {
                            bodyParams.Add("$InvMonth", inv.ArisingDate.Month.ToString("00") + "/" + inv.ArisingDate.Year.ToString());
                        }
                        bodyParams.Add("$NameCustomer", inv.CusName);
                        bodyParams.Add("$CusTaxCode", inv.CusTaxCode ?? "");
                        bodyParams.Add("$CusCode", inv.CusCode);
                        bodyParams.Add("$Fkey", inv.Fkey);
                        bodyParams.Add("$Buyer", inv.Buyer);
                        bodyParams.Add("$ArisingDate", inv.ArisingDate.ToString("dd/MM/yyyy"));
                        emailSrv.ProcessEmail(labelMail, emailToSend, "MessagerMail", subjectParams, bodyParams);
                        //save lai vao bang InvDeliver
                        deliver.isDelivery = 1;
                        _deliverSrv.Save(deliver);
                        _deliverSrv.CommitChanges();
                    }
                }

                catch (Exception ex) {  log.Warn("Deliver: " + ex);  }
               
            }
        }
        public void ProcessingSendmail(IInvoice oInv, IInvoice newInv, int processingType, string pattern)
        {
            try
            {
                string CusCode = (oInv.CusCode != null) ? oInv.CusCode : "";
                Customer cus = IoC.Resolve<ICustomerService>().SearchCusCode(CusCode, oInv.ComID);
                Company currentCom = ((EInvoiceContext)FXContext.Current).CurrentCompany;
                if (((currentCom.Config.Keys.Contains("MessagerAdjust") && processingType == 0) || (currentCom.Config.Keys.Contains("MessagerReplace") && processingType == 1)) || (currentCom.Config.Keys.Contains("MessagerCancel") && processingType == 2))
                {
                    FX.Utils.EmailService.IEmailService emailSrv = FX.Core.IoC.Resolve<FX.Utils.EmailService.IEmailService>();
                    Dictionary<string, string> subjectParams = new Dictionary<string, string>(1);
                    subjectParams.Add("subject", "");
                    Dictionary<string, string> bodyParams = new Dictionary<string, string>(9);
                    bodyParams.Add("$CompanyName", currentCom.Name);
                    bodyParams.Add("$pattern", oInv.Pattern);
                    bodyParams.Add("$serial", oInv.Serial);
                    bodyParams.Add("$invNumber", oInv.No.ToString("0000000"));
                    user _currentUser = FXContext.Current.CurrentUser;
                    string weblink = (currentCom.Config.Keys.Contains("LinkEmail")) ? currentCom.Config["LinkEmail"] : System.Configuration.ConfigurationManager.AppSettings["Portal"];
                    if (newInv != null)
                    {
                        bodyParams.Add("$Newpattern", newInv.Pattern);
                        bodyParams.Add("$NewSerial", newInv.Serial);
                        bodyParams.Add("$NewInvMumber", newInv.No.ToString("0000000"));
                        bodyParams.Add("$AdjustType", Utils.GetNameInvoiceType((InvoiceType)newInv.Type));
                        string token = Utils.recordsDownloadLink(weblink, oInv.id.ToString(), newInv.id.ToString(), pattern, currentCom.id.ToString(), _currentUser.username);
                        bodyParams.Add("$RecordUrl", token);
                    }
                    // String weblink = System.Configuration.ConfigurationManager.AppSettings["Portal"];

                    string email = string.Empty;
                    if (!string.IsNullOrEmpty(oInv.EmailDeliver))
                        email = oInv.EmailDeliver;
                    else if (cus != null && !string.IsNullOrEmpty(cus.Email))
                        email = cus.Email;

                    if (!string.IsNullOrEmpty(email))
                    {
                        string labelMail = currentCom.Config.ContainsKey("LabelMail") ? currentCom.Config["LabelMail"] : "hoadondientu@vnpt-invoice.vn";
                        if (processingType == 0) emailSrv.ProcessEmail(labelMail, email, "MessagerAdjust", subjectParams, bodyParams);
                        else if
                            (processingType == 1) emailSrv.ProcessEmail(labelMail, email, "MessagerReplace", subjectParams, bodyParams);
                        else if
                            (processingType == 2) emailSrv.ProcessEmail(labelMail, email, "MessagerCancel", subjectParams, bodyParams);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
            }
        }

        public bool IsAllowDeliver(IInvoice inv, Company _currentCompany)
        {
             IInvDeliverService _deliverSrv = IoC.Resolve<IInvDeliverService>();
             try
             {
                 string invToken = getInvToken(inv, _currentCompany);
                 InvDeliver deliver = _deliverSrv.searchByTokenString(invToken);
                 if (null == deliver || deliver.isDelivery!=0)
                 {
                     return false;
                 }
                 return true;
             }
             catch (Exception ex)
             {
                 log.Error("IsAllowDeliver: " + ex);
             }
            return false;
        }
    }
}
